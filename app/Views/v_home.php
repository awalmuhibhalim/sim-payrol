<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <img src="<?php base_url() ?> template/assets/img/dashboard2.png" class="img-fluid"
                                style="width: 100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <h3>Data Kehadiran</h3>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Jam Datang</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <h3>Data Tidak Hadir / Terlambat / Leave / Izin / Sakit</h3>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tbl_userlist2">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Jam Datang</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <h3>Prosentase Kehadiran</h3>
                        <canvas id="donutChart"
                            style="min-height: 250px; height: 350px; max-height: 350px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    let table2 = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "AbsensiController/presentToday", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "data": "nama_karyawan"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "jam_datang"
                },
                {
                    "data": "status"
                }
            ],
        });

        table2 = $('#tbl_userlist2').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "AbsensiController/unPresentToday", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "data": "nama_karyawan"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "jam_datang"
                },
                {
                    "data": "status"
                }
            ],
        });

        drawChart()
    }, 1000)
});

function drawChart() {
    $.ajax({
        method: "GET",
        url: "AbsensiController/persentasiKehadiran",
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
        var donutData = {
            labels: [
                'Hadir',
                'Tidak Hadir',
                'Terlambat',
                'Leave',
                'Izin',
                'Sakit',
            ],
            datasets: [{
                data: [data.hadir, data.tidak_hadir, data.terlambat, data.leave,
                    data.izin, data.sakit
                ],
                // data: [700, 500, 400, 600, 300, 100],
                backgroundColor: ['#00a65a', '#f56954', '#f39c12', '#00c0ef',
                    '#3c8dbc',
                    '#d2d6de'
                ],
            }]
        }
        var donutOptions = {
            maintainAspectRatio: false,
            responsive: true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })
    })
}
</script>