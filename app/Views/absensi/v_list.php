<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-1.9">
                                        <div type="button" class="btn btn-warning hak_akses absensi_btn_izin"
                                            onclick="openform2()">Izin
                                            / Sakit
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div type="button" class="btn btn-primary" onclick="openform()">Absen</div>
                                    </div>
                                    <div class="col-md-1.2">
                                        <div type="button" class="btn btn-primary" onclick="leaveOffice()">Check Out
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Tanggal</th>
                                                <th>Jam Datang</th>
                                                <th>Jam Pulang</th>
                                                <th>Status</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="header-title">Hai <?= $_SESSION['nama_karyawan']; ?>, ini waktunya absen
                </h4>
            </div>
            <!-- <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1"><?= $_SESSION['nama_karyawan']; ?></label>
                        </div>
                    </div>
                </form>
            </div> -->
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="save" class="btn btn-primary" onclick="save()">Absen</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-xl-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Izin / Sakit</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Karyawan</label>

                            <select class="selectpicker form-control" data-container="body" data-live-search="true"
                                title="Select a number" data-hide-disabled="true" id="karyawan_id">
                                <option value="">Iwan</option>
                                <option value="">Rudy</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Alasan</label>
                            <select class="form-control" id="status">
                                <option value="Hadir">Hadir</option>
                                <option value="Tidak Hadir">Tidak Hadir</option>
                                <option value="Sakit">Sakit</option>
                                <option value="Izin">Izin</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="reset" class="btn btn-warning" data-dismiss="modal">Reset</div>
                    <div id="save2" class="btn btn-primary absensi_btn_save" onclick="openform2()">Save</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_daftar_absensi') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                },
                {
                    "data": "nama_karyawan"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "tanggal"
                },
                {
                    "data": "jam_datang"
                },
                {
                    title: "Jam Pulang",
                    "data": "jam_pulang",
                    render(data) {
                        return (data == null ? "-" : data)
                    }
                },
                {
                    "data": "status"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary hak_akses absensi_btn_view">View</button>';
                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="deleteData(\'' +
                            row
                            .id +
                            '\')" class="btn btn-danger hak_akses absensi_btn_delete">Delete</button>';
                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();

        $('#karyawan_id').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        getKaryawanList()
    }, 1000)
});


function openform() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()');
    let session = "<?= $_SESSION['nama_karyawan']; ?>";
    $('#header-title').text("Hai " + session + ", ini waktunya absen");
    $('#save').text('ABSEN');
}

function openform2() {
    $('#modal-xl-2').modal({
        'backdrop': 'static'
    });
    $('#save2').attr('onclick', 'save2()');
}

function reset() {
    // $('#form')[0].reset();
}

function save() {
    // if (!validation()) {
    //     return;
    // }

    $('#save').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let status = "";
    if (date.getHours() <= 9 && date.getMinutes() < 15) {
        status = "Hadir"
    } else {
        status = "Terlambat"
    }
    let karyawan_id = "<?= $_SESSION['id']; ?>";
    if (karyawan_id == null || karyawan_id == "") {
        toastr.error("Invalid Session");
    }
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_datang": datetime,
        "jam_pulang": null,
        "tanggal": year + "-" + month + "-" + day,
        "status": status
    };

    $.ajax({
        method: "POST",
        url: "insertAbsensi",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function save2() {
    if (!validation()) {
        return;
    }

    $('#save2').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let karyawan_id = $('#karyawan_id').val();
    let status = $('#status').val();
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_datang": null,
        "jam_pulang": null,
        "tanggal": year + "-" + month + "-" + day,
        "status": status
    };

    $.ajax({
        method: "POST",
        url: "insertAbsensi",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save2').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "AbsensiFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#karyawan_id').val(data.karyawan_id);
        $('#status').val(data.status);
        $('#modal-xl-2').modal({
            'backdrop': 'static'
        });
        $('#save2').attr('onclick', 'update2(' + data.id + ')')
        $('#karyawan_id').selectpicker('refresh');
    })
}

function leaveOffice() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'update()')
    let session = "<?= $_SESSION['nama_karyawan']; ?>";
    $('#header-title').text('Hai ' + session + ', apakah kamu yakin akan meninggalkan waktu kerjamu?');
    $('#save').text('LEAVE');
}

function update() {

    $('#save').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let status = "";
    if (date.getHours() < 17 && date.getMinutes() < 15) {
        status = "Leave"
    } else {
        status = "Hadir"
    }
    let karyawan_id = "<?= $_SESSION['id']; ?>";
    if (karyawan_id == null || karyawan_id == "") {
        toastr.error("Invalid Session");
    }
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_datang": "",
        "jam_pulang": datetime,
        "tanggal": year + "-" + month + "-" + day,
        "status": status
    };

    $.ajax({
        method: "POST",
        url: "updateAbsensi",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function update2(id) {
    if (!validation()) {
        return;
    }

    $('#save2').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let karyawan_id = $('#karyawan_id').val();
    let status = $('#status').val();
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_datang": null,
        "jam_pulang": null,
        "tanggal": year + "-" + month + "-" + day,
        "status": status
    };

    $.ajax({
        method: "POST",
        url: "updateAbsensi2/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save2').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function validation() {
    if ($('#karyawan_id').val() == null) {
        toastr.error("karyawan tidak boleh kosong");
        return false;
    }

    if ($('#status').val() == null) {
        toastr.error("Alasan tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete gaji',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteAbsensi/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Absensi berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });
}

function getKaryawanList() {
    $.ajax({
        method: 'GET',
        url: 'KaryawanController/getKaryawanList',
        contentType: 'application/json'
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data)
        var options = [],
            _options;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].id;
            let text = data[i].nama_karyawan;

            let option = '<option value="' + value + '">' + text + '</option>';
            options.push(option);

        }
        _options = options.join('');
        $('#karyawan_id')[0].innerHTML = _options;
        $('#karyawan_id').selectpicker('refresh');
    })
}
</script>