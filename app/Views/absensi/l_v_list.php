<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <!-- <a type="button" class="btn btn-primary">Tambah Data</a> -->
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
                            Tambah Data Gaji
                        </button><br>
                    </div>
                    <div class="col-md-4">
                        <form action="" method="get">
                            <div class="input-group">
                                <input id="btn-input" type="text" class="form-control input-sm" name="keyword"
                                    placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm" id="btn-chat" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Karywan</th>
                                <th>Gaji Pokok</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1 + (5 * ($currentPage - 1));
                            foreach ($gaji as $key => $value) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['nama_karyawan']; ?></td>
                                <td><?= $value['gaji_pokok']; ?></td>
                                <td><a class="btn btn-info">View</a>&nbsp;&nbsp;&nbsp;
                                    <a href="<?= base_url('edituser/' . $value['id']); ?>"
                                        class="btn btn-primary">Edit</a>&nbsp;&nbsp;&nbsp;
                                    <a href="<?= base_url('deleteuser/' . $value['id']); ?>" class="btn btn-danger"
                                        onclick="return confirm('apakah anda yakin ingin menghapus data  <?= $value['nama_karyawan']; ?> ?')">Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?= $pager->links(); ?>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Gaji</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="<?= base_url('adduser'); ?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Karyawan</label>
                                <select class="form-control" id="karyawan_id">
                                    <option value="">Iwan</option>
                                    <option value="">Rudy</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Gaji Pokok</label>
                                <input type="number" name="password" class="form-control" placeholder="Enter Password">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>