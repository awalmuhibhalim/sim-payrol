<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row detail_gaji_btn_save">
                                    <div class="col-md-2">
                                        <select class="selectpicker form-control" data-container="body"
                                            data-live-search="true" title="Bulan" data-hide-disabled="true" id="bulan">
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select class="selectpicker form-control" data-container="body"
                                            data-live-search="true" title="Tahun" data-hide-disabled="true" id="tahun">
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="btn btn-warning" onclick="printGaji()">Print</div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Total Gaji</th>
                                                <th>Bulan</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Gaji</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Karyawan</label>

                            <select class="selectpicker form-control" data-container="body" data-live-search="true"
                                title="Pilih" data-hide-disabled="true" id="karyawan_id">
                                <option value="">Iwan</option>
                                <option value="">Rudy</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Gaji Pokok</label>
                            <input type="number" id="gaji_pokok" class="form-control" placeholder="gaji pokok">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="reset" class="btn btn-warning" data-dismiss="modal">Reset</div>
                    <div id="save" class="btn btn-primary" onclick="save()">Save</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-generate">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Laporan Gaji</h4>
            </div>
            <div class="modal-body">
                <div id="detail_gaji">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Karyawan</th>
                                <th>Jabatan</th>
                                <th>Total Gaji</th>
                                <th>Keterangan</th>
                                <th>Bulan</th>
                            </tr>
                        </thead>
                        <tbody id="gaji_list">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div class="btn btn-default" onclick="cetak()">Cetak</div>
                    <!-- <div class="btn btn-primary" onclick="generateSemuaGaji()">Hitung</div>
                    <div class="btn btn-primary" onclick="simpanGaji()">Simpan Gaji</div> -->
                </div>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_daftar_detail_gaji') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                },
                {
                    "data": "nama_karyawan"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "total_gaji"
                },
                {
                    "data": "tanggal"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary detail_gaji_btn_view">View</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' +
                            row
                            .id +
                            '\')" class="btn btn-danger detail_gaji_btn_delete">Delete</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();

        $('#karyawan_id').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        $('#bulan').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        $('#tahun').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });
        monthList();
        getKaryawanList()

        $(".datepicker").datetimepicker({
            format: "YYYY-MM-DD",
            useCurrent: false
        })

    }, 1000)
});


function openform() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "GajiFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#karyawan_id').val(data.karyawan_id);
        $('#gaji_pokok').val(data.gaji_pokok);
        $('#karyawan_id').selectpicker('refresh');
    })
}

function update(id) {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "karyawan_id": parseInt($('#karyawan_id').val()),
        "gaji_pokok": parseInt($('#gaji_pokok').val())
    };

    $.ajax({
        method: "POST",
        url: "updateGaji/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function validation() {
    if ($('#karyawan_id').val() == null) {
        toastr.error("karyawan tidak boleh kosong");
        return false;
    }

    if ($('#gaji_pokok').val() == null || $('#gaji_pokok').val() < 1) {
        toastr.error("Gaji Pokok tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete gaji',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteGaji/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Gaji berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });
}

function getKaryawanList() {
    $.ajax({
        method: 'GET',
        url: 'KaryawanController/getKaryawanList',
        contentType: 'application/json'
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data)
        var options = [],
            _options;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].id;
            let text = data[i].nama_karyawan;

            let option = '<option value="' + value + '">' + text + '</option>';
            options.push(option);

        }
        _options = options.join('');
        $('#karyawan_id')[0].innerHTML = _options;
        $('#karyawan_id').selectpicker('refresh');
    })
}

function monthList() {
    var options = [],
        _options;
    let date = new Date();
    let month = date.getMonth();
    let year = date.getFullYear();
    options.push('<option value="' + year + '">' + year + '</option>');
    for (let i = 1; i < 10; i++) {
        let value = year - i;
        let option = '<option value="' + value + '">' + value + '</option>';
        options.push(option);
    }
    _options = options.join('');
    $('#tahun')[0].innerHTML = _options;
    $('#tahun').selectpicker('refresh');
}

function openFormGenerate() {
    $('#detail_gaji').empty();
    $('#modal-generate').modal({
        'backdrop': 'static'
    });
}

function printGaji() {
    if ($('#bulan').val().trim() == "" || $('#bulan').val() == null) {
        toastr.warning("Bulan belum dipilih");
        return;
    }

    if ($('#tahun').val().trim() == "" || $('#tahun').val() == null) {
        toastr.warning("Tahun belum dipilih");
        return;
    }
    let value = {
        'month': $('#bulan').val() + "-" + $('#tahun').val()
    }
    $.ajax({
        method: 'POST',
        url: 'DetailGajiController/findByMonth',
        contentType: 'application/json',
        data: JSON.stringify(value),
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        $('#gaji_list').empty();
        $('#modal-generate').modal({
            'backdrop': 'static'
        });
        for (let i = 0; i < data.length; i++) {
            let no = i + 1;
            let keterangan = data[i].keterangan.split("=");
            let detailKeterangan = '<div><div>' + keterangan[0] + '</div>\n\
            <div>' + keterangan[1] + '</div>\n\
            <div>' + keterangan[2] + '</div></div>';
            console.log(detailKeterangan)
            let tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].nama_karyawan + '</td>\n\
                            <td>' + data[i].jabatan + '</td>\n\
                            <td>' + data[i].total_gaji + '</td>\n\
                            <td>' + detailKeterangan + '</td>\n\
                            <td>' + data[i].tanggal + '</td>\n\
                        </tr>';

            $('#gaji_list').append(tr)
        }

    })
}

function cetak(i) {
    $('#detail_gaji').printElement({});
}
</script>