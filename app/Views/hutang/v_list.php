<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-1.5">
                                        <div type="button" class="btn btn-primary hutang_btn_add" onclick="openform()">
                                            Tambah
                                            Data</div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Tgl Pinjaman</th>
                                                <th>Total Pinjaman</th>
                                                <th>Jangka Waktu</th>
                                                <th>Cicilan</th>
                                                <th>Sisa</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Hutang</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Karyawan</label>

                            <select class="selectpicker form-control" data-container="body" data-live-search="true"
                                title="Select a number" data-hide-disabled="true" id="karyawan_id">
                                <option value="">Iwan</option>
                                <option value="">Rudy</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Pinjaman</label>
                            <input type="date" id="tanggal_pinjaman" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Total Pinjaman</label>
                            <input type="number" id="total_pinjaman" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jangka Waktu</label>
                            <input type="number" id="jangka_waktu" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Cicilan Perbulan</label>
                            <input type="number" id="cicilan_perbulan" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Sisa Cicilan</label>
                            <input type="number" id="sisa_cicilan" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="reset" class="btn btn-warning" data-dismiss="modal">Reset</div>
                    <div id="save" class="btn btn-primary hutang_btn_save" onclick="save()">Save</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-generate">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hitung Gaji</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Start</label>
                            <input type="text" id="start" class="form-control start datepicker datetimepicker-input"
                                data-toggle="datetimepicker" data-target=".start" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">End</label>
                            <input type="text" id="end" class="form-control end datepicker datetimepicker-input"
                                data-toggle="datetimepicker" data-target=".end" />
                        </div>
                    </div>
                </form>
                <div id="detail_gaji"> </div>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <!-- <div class="btn btn-default" onclick="cetak()">Cetak</div> -->
                    <div class="btn btn-primary" onclick="generateSemuaGaji()">Hitung</div>
                    <div class="btn btn-primary" onclick="simpanGaji()">Simpan Gaji</div>
                </div>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_daftar_hutang') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                },
                {
                    "data": "nama_karyawan"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "tanggal_pinjaman"
                },
                {
                    "data": "total_pinjaman"
                },
                {
                    "data": "jangka_waktu"
                },
                {
                    "data": "cicilan_perbulan"
                },
                {
                    "data": "sisa_cicilan"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary hutang_btn_view">View</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' +
                            row
                            .id +
                            '\')" class="btn btn-danger hutang_btn_delete">Delete</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();

        $('#karyawan_id').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        getKaryawanList()

        $(".datepicker").datetimepicker({
            format: "YYYY-MM-DD",
            useCurrent: false
        })

    }, 1000)
});


function openform() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "karyawan_id": parseInt($('#karyawan_id').val()),
        "tanggal_pinjaman":$('#tanggal_pinjaman').val(),
        "total_pinjaman": parseInt($('#total_pinjaman').val()),
        "jangka_waktu": parseInt($('#jangka_waktu').val()),
        "cicilan_perbulan": parseInt($('#cicilan_perbulan').val()),
        "sisa_cicilan": parseInt($('#sisa_cicilan').val())
    };

    $.ajax({
        method: "POST",
        url: "insertHutang",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "HutangFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#karyawan_id').val(data.karyawan_id);
        $('#tanggal_pinjaman').val(data.tanggal_pinjaman);
        $('#total_pinjaman').val(data.total_pinjaman);
        $('#jangka_waktu').val(data.jangka_waktu);
        $('#cicilan_perbulan').val(data.cicilan_perbulan);
        $('#karyawan_id').selectpicker('refresh');
    })
}

function update(id) {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "karyawan_id": parseInt($('#karyawan_id').val()),
        "tanggal_pinjaman":$('#tanggal_pinjaman').val(),
        "total_pinjaman": parseInt($('#total_pinjaman').val()),
        "jangka_waktu": parseInt($('#jangka_waktu').val()),
        "cicilan_perbulan": parseInt($('#cicilan_perbulan').val()),
        "sisa_cicilan": parseInt($('#sisa_cicilan').val())
    };

    $.ajax({
        method: "POST",
        url: "updateHutang/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function validation() {
    if ($('#karyawan_id').val() == null) {
        toastr.error("karyawan tidak boleh kosong");
        return false;
    }

    if ($('#tanggal_pinjaman').val() == null || $('#tanggal_pinjaman').val() == "") {
        toastr.error("Tanggal Pinjaman tidak boleh kosong");
        return false;
    }

    if ($('#total_pinjaman').val() == null || $('#total_pinjaman').val() < 1) {
        toastr.error("total pinjaman tidak boleh kosong");
        return false;
    }

    if ($('#jangka_waktu').val() == null || $('#jangka_waktu').val() < 1) {
        toastr.error("jangka waktu tidak boleh kosong");
        return false;
    }

    if ($('#cicilan_perbulan').val() == null || $('#cicilan_perbulan').val() < 1) {
        toastr.error("cicilan perbulan tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete gaji',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteHutang/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Hutang berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });
}

function getKaryawanList() {
    $.ajax({
        method: 'GET',
        url: 'KaryawanController/getKaryawanList',
        contentType: 'application/json'
    }).done(function(response) {
        console.log("===========================")
        let data = JSON.parse(response);
        console.log(data)
        var options = [],
            _options;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].id;
            let text = data[i].nama_karyawan;

            let option = '<option value="' + value + '">' + text + '</option>';
            options.push(option);

        }
        _options = options.join('');
        $('#karyawan_id')[0].innerHTML = _options;
        $('#karyawan_id').selectpicker('refresh');
    })
}
</script>