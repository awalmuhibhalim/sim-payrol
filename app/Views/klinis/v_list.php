<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <!--                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
                    Add User
                </button><br>-->
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div type="button" class="btn btn-primary" onclick="openform()">Tambah
                            Data</div>
                    </div>
                </div>
                <br>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Rekam Medis</th>
                                <th>Nama Pasien</th>
                                <th>Nomor Telpn Pasien</th>
                                <th>Tanggal</th>
                                <th>Petugas</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>

    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Klinis</h4>
                </div>
                <div class="modal-body custom_scroll">
                    <form id="form" role="form" action="<?= base_url('add_klinis') ?>" method="post">
                        <div class="card-body" id="form_area">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Adminstrasi</label>
                                <input type="text" class="form-control" id="search"
                                    placeholder="Cari data administrasi (nama pasien, no. rekam medis)"
                                    onkeyup="getAdminstrasi()" />
                                <input type="hidden" class="form-control" id="administrasi_id" />
                                <table id="table_administrasi" class="table table-striped table-bordered table-hover"
                                    style="display:none">
                                    <tbody id="administrasi_list"></tbody>
                                </table>
                                <div class="btn btn-default" id="nama_pasien"></div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Cari data obat</label>
                                <input type="text" class="form-control" id="search_farmasetis" placeholder="search..."
                                    onkeyup="getFarmasetis()" />
                                <table id="table_farmasetis" class="table table-striped table-bordered table-hover">
                                    <tbody id="farmasetis_id"></tbody>
                                </table>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        </tr>
                                    </thead>
                                    <tbody id="farmasetis_selected"></tbody>
                                </table>
                            </div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                    </tr>
                                </thead>
                                <tbody id="comparation">
                                </tbody>
                            </table>
                        </div>
                </div>
                <!-- /.card-body -->
                <div class="container" id="print_area" style="display: none">
                    <div class="card">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col-sm-2">
                                    <div>
                                        <strong>Tanggal Resep</strong>
                                    </div><br>
                                    <div>
                                        Nama Pasien
                                    </div>
                                    <div>
                                        Nama Dokter
                                    </div>
                                    <div>
                                        Jaminan
                                    </div><br>
                                </div>
                                <div class="col-sm-1">
                                    <div>
                                        <strong>:</strong>
                                    </div><br>
                                    <div>
                                        :
                                    </div>
                                    <div>
                                        :
                                    </div>
                                    <div>
                                        :
                                    </div><br>
                                </div>
                                <div class="col-sm-4">
                                    <div>
                                        <strong id="inv_tanggal_resep">#</strong>
                                    </div><br>
                                    <div id="inv_pasien">-</div>
                                    <div id="inv_dokter">-</div>
                                    <div id="inv_jaminan">-</div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-sm-9">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="center">#</th>
                                                    <th>Nama Obat</th>
                                                    <th>Bentuk Sediaan</th>
                                                    <th class="right">Jumlah Obat</th>
                                                    <th class="right">Aturan Pakai</th>
                                                </tr>
                                            </thead>
                                            <tbody id="inv_obat">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-default" id="preview" onclick="openPreview()">Preview</div>
                    <div type="button" class="btn btn-warning" id="reset" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary" id="save" onclick="save()">Save</div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-detail-obat">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body custom_scroll">
                    <form id="form" role="form" action="<?= base_url('add_farmasetis') ?>" method="post">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Farmasetis</h3>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Obat</label>
                                        <input type="text" id="nama_obat" class="form-control" placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Bentuk Sediaan</label>
                                        <select class="form-control" id="bentuk_sediaan" readonly>
                                            <option value="Pulvis (serbuk)">Pulvis (serbuk)</option>
                                            <option value="Pulveres">Pulveres</option>
                                            <option value="Tablet (compressi)">Tablet (compressi)</option>
                                            <option value="Pilulae (PIL)">Pilulae (PIL)</option>
                                            <option value="Kapsul (capsule)">Kapsul (capsule)</option>
                                            <option value="Solutiones (Larutan)">Solutiones (Larutan)</option>
                                            <option value="Suspensi">Suspensi</option>
                                            <option value="Emulsi">Emulsi (elmusiones)</option>
                                            <option value="Infusa">Infusa</option>
                                            <option value="Imunoserum">Imunoserum (immunosera)</option>
                                            <option value="Salep">Salep (unguenta)</option>
                                            <option value="Suppositoria">Suppositoria</option>
                                            <option value="Obat tetes">Obat tetes (guttae)</option>
                                            <option value="Injeksi">Injeksi (injectiones)</option>
                                            <option value="Aerosol">Aerosol</option>
                                            <option value="Gel">Gel</option>
                                            <option value="Ovula">Ovula</option>
                                            <option value="Implan">Implan</option>
                                            <option value="Obat Kumur (Gargle)">Obat Kumur (Gargle)</option>
                                            <option value="Sirup">Sirup</option>
                                            <option value="Elixir">Elixir</option>
                                            <option value="potio">potio</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kekuatan Sediaan</label>
                                        <input type="text" id="kekuatan_sediaan" class="form-control" placeholder=""
                                            readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Satuan Sediaan</label>
                                        <input type="text" id="satuan_sediaan" class="form-control" placeholder=""
                                            readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jumlah Obat</label>
                                        <input type="number" id="jumlah_obat" class="form-control" placeholder=""
                                            readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Aturan Pakai</label>
                                        <textarea id="aturan_pakai" class="form-control" placeholder=""
                                            readonly></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Cara Pakai</label>
                                        <textarea id="cara_pakai" class="form-control" placeholder=""
                                            readonly></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Stabilitas Obat</label>
                                        <input type="text" id="stabilitas_obat" class="form-control" placeholder=""
                                            readonly>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h3>Klinis</h3>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Indikasi Obat</label>
                                        <input type="text" name="indikasi_obat" id="indikasi_obat" class="form-control"
                                            placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Dosis Obat</label>
                                        <input type="text" name="dosis_obat" id="dosis_obat" class="form-control"
                                            placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Rute Pemberian Obat</label>
                                        <input type="text" name="rute_pemberian_obat" id="rute_pemberian_obat"
                                            class="form-control" placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tepat Waktu</label>
                                        <input type="text" name="tepat_waktu" id="tepat_waktu" class="form-control"
                                            placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Duplikasi</label>
                                        <input type="text" name="duplikasi" id="duplikasi" class="form-control"
                                            placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Alergi</label>
                                        <textarea id="alergi" class="form-control" placeholder="" readonly></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Interaksi Obat</label>
                                        <textarea name="interaksi_obat" id="interaksi_obat" class="form-control"
                                            placeholder="" readonly></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kontraindikasi Obat</label>
                                        <input type="text" name="kontraindikasi_obat" id="kontraindikasi_obat"
                                            class="form-control" placeholder="" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Efek Samping</label>
                                        <textarea id="efek_samping" class="form-control" placeholder=""
                                            readonly></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



</div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('klinis_datatable') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ],
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "no_rekam_medis"
                }, {
                    "data": "nama_pasien"
                },
                {
                    "data": "no_telepon_pasien"
                },
                {
                    "data": "created_at"
                },
                {
                    "data": "creator"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary">View</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' + row
                            .id +
                            '\')" class="btn btn-danger">Delete</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});

let drags = [];
let timeout;
let administrasiTemp = [];

function openform() {
    // $('#modal-xl').modal('show');
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    enableButton()
    $('#preview').attr('disabled', true);
    reset()
    closePreview()
    drags = [];
    // drawFarmasetis()
    $('#farmasetis_selected').empty();
    $('#inv_obat').empty();
    $('#comparation').empty();
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    if (!validation()) {
        return;
    }
    disableButton()
    let date = new Date();
    let current = date.getHours();
    let shift = "";

    if (current > 7 && current <= 14) {
        shift = "shift pagi";
    } else if (current > 14 && current <= 21) {
        shift = "shift siang";
    } else {
        shift = "shift malam";
    }

    let resep = [];
    $('#inv_obat').empty();
    for (let i = 0; i < drags.length; i++) {
        let no = i + 1;
        let farmasetis_id = $('#farmasetis_id_' + drags[i].id).val();
        let nama_obat = drags[i].nama_obat;
        let bentuk_sediaan = $('#bentuk_sediaan_' + drags[i].id).val();
        let jumlah_obat = $('#jumlah_obat_' + drags[i].id).val();
        let aturan_pakai = $('#aturan_pakai_' + drags[i].id).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        resep.push(obj);

        let tr_obat = '<tr>\n\
                            <td class="center">' + no + '</td>\n\
                            <td class="left strong">' + nama_obat + '</td>\n\
                            <td class="left">' + bentuk_sediaan + '</td>\n\
                            <td class="right">' + jumlah_obat + '</td>\n\
                            <td class="right">' + aturan_pakai + '</td>\n\
                        </tr>';
        $('#inv_obat').append(tr_obat);
    }
    let value = {
        "administrasi_id": parseInt($('#administrasi_id').val()),
        "shift": shift,
        "resep_obat": resep
    }

    $.ajax({
        method: "POST",
        url: "insertKlinis",
        contentType: "json",
        data: JSON.stringify(value)
    }).done(function(response) {
        toastr.info("Data berhasil ditambahkan, silahkan klik button close");
        for (let i = 0; i < administrasiTemp.length; i++) {
            if (administrasiTemp[i].id == $('#administrasi_id').val()) {
                $('#inv_tanggal_resep').text(administrasiTemp[i].tanggal_resep);
                $('#inv_pasien').text(administrasiTemp[i].nama_pasien);
                $('#inv_dokter').text(administrasiTemp[i].nama_dokter);
                $('#inv_jaminan').text(administrasiTemp[i].jenis_penjamin);
                break;
            }
        }
        openPreview()

    })
}

function showData(id) {
    drags = [];
    $.ajax({
        method: "GET",
        url: "edit_klinis/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        // $('#modal-xl').modal('show');
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        enableButton()
        closePreview()
        $('#preview').attr('disabled', false);
        $('#farmasetis_selected').empty();
        $('#inv_obat').empty();
        $('#save').attr('onclick', 'update(' + data.klinis.id + ')')
        $('#administrasi_id').val(data.klinis.administrasi_id);
        $('#nama_pasien').text("Nama Pasien : " + data.klinis.nama_pasien);


        $('#inv_tanggal_resep').text(data.klinis.created_at);
        $('#inv_pasien').text(data.klinis.nama_pasien);
        $('#inv_dokter').text(data.klinis.nama_dokter);
        $('#inv_jaminan').text(data.klinis.jenis_penjamin);

        let resep = [];
        resep = data.resep;
        for (let i = 0; i < resep.length; i++) {
            selectFarmasetis(resep[i].farmasetis_id, resep[i].nama_obat, resep[i].bentuk_sediaan, resep[i]
                .jumlah_obat, resep[i].aturan_pakai)
        }
    })
}

function showDataFarmasetisDetail(id) {
    $.ajax({
        method: "GET",
        url: "edit_farmasetis/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-detail-obat').modal('show');
        $('#nama_obat').val(data.nama_obat);
        $('#bentuk_sediaan').val(data.bentuk_sediaan);
        $('#kekuatan_sediaan').val(data.kekuatan_sediaan);
        $('#satuan_sediaan').val(data.satuan_sediaan);
        $('#jumlah_obat').val(data.jumlah_obat);
        $('#aturan_pakai').val(data.aturan_pakai);
        $('#cara_pakai').val(data.cara_pakai);
        $('#stabilitas_obat').val(data.stabilitas_obat);
        $('#indikasi_obat').val(data.indikasi_obat)
        $('#dosis_obat').val(data.dosis_obat);
        $('#rute_pemberian_obat').val(data.rute_pemberian_obat);
        $('#tepat_waktu').val(data.tepat_waktu);
        $('#duplikasi').val(data.duplikasi);
        $('#alergi').val(data.alergi);
        $('#interaksi_obat').val(data.interaksi_obat);
        $('#kontraindikasi_obat').val(data.kontraindikasi_obat);
        $('#efek_samping').val(data.efek_samping);
    })
}

function update(id) {
    if (!validation()) {
        return;
    }
    disableButton()
    let resep = [];
    $('#inv_obat').empty();
    for (let i = 0; i < drags.length; i++) {
        let no = i + 1;
        let farmasetis_id = $('#farmasetis_id_' + drags[i].id).val();
        let nama_obat = drags[i].nama_obat;
        let bentuk_sediaan = $('#bentuk_sediaan_' + drags[i].id).val();
        let jumlah_obat = $('#jumlah_obat_' + drags[i].id).val();
        let aturan_pakai = $('#aturan_pakai_' + drags[i].id).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        resep.push(obj);

        let tr_obat = '<tr>\n\
                            <td class="center">' + no + '</td>\n\
                            <td class="left strong">' + nama_obat + '</td>\n\
                            <td class="left">' + bentuk_sediaan + '</td>\n\
                            <td class="right">' + jumlah_obat + '</td>\n\
                            <td class="right">' + aturan_pakai + '</td>\n\
                        </tr>';
        $('#inv_obat').append(tr_obat);
    }

    let value = {
        "administrasi_id": parseInt($('#administrasi_id').val()),
        "resep_obat": resep
    }

    $.ajax({
        method: "POST",
        url: "updateKlinis/" + id,
        contentType: "json",
        data: JSON.stringify(value)
    }).done(function(response) {
        toastr.info("Berhasil diubah");
        openPreview()
    })
}

function disableButton() {
    $('#save').attr('disabled', true);
    $('#reset').attr('disabled', true);
    // $('#preview').attr('disabled', false);

}

function enableButton() {
    $('#save').attr('disabled', false);
    $('#reset').attr('disabled', false);
    // $('#preview').attr('disabled', true);
}



function validation() {
    if ($('#administrasi_id').val() == null || $('#administrasi_id').val().trim() == "") {
        toastr.error("data administrasi belum dipilih");
        return false;
    }

    if (drags.length < 1) {
        toastr.error("resep obat tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete data',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "delete_klinis/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.success(data.message);
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });

}

function getAdminstrasi() {
    let keyword = $('#search').val().trim();
    if (keyword.length >= 1) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "AdministrasiController/showAdministrasionList",
                contentType: "application/json",
                data: {
                    'keyword': keyword
                },
            }).done(function(response) {
                $('#search').val("");
                let data = JSON.parse(response);
                if (data == null) {
                    return
                }
                $('#table_administrasi').show();
                $('#administrasi_list').empty();
                let tr = '<tr>\n\
                        <th>No</th>\n\
                        <th>No Rekam Medis</th>\n\
                        <th>Nama Pasien</th>\n\
                        <th>Nama Dokter</th>\n\
                        <th></th>\n\
                    </tr>';
                $('#administrasi_list').append(tr);
                administrasiTemp = data;
                for (let i = 0; i < data.length; i++) {
                    let no = i + 1;
                    tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].no_rekam_medis + '</td>\n\
                            <td>' + data[i].nama_pasien + '</td>\n\
                            <td>' + data[i].nama_dokter + '</td>\n\
                            <td><buttom class="btn btn-info" onclick="pilihPasien(\'' + data[i].id + '\',\'' + data[i]
                        .nama_pasien + '\')">Pilih</buttom>\n\
                            </td>\n\
                        </tr>';
                    $('#administrasi_list').append(tr);
                }
            })
        }, 1000)
    } else if (keyword.length < 3) {
        $('#table_administrasi').hide();
    }
}

function getFarmasetis() {
    let keyword = $('#search_farmasetis').val();
    if (keyword.length >= 1) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "FarmasetisController/serachFarmasetis",
                contentType: "application/json",
                data: {
                    'keyword': keyword
                },
            }).done(function(response) {
                let data = JSON.parse(response);
                if (data == null) {
                    return;
                }
                $('#table_farmasetis').show();
                $('#farmasetis_id').empty();
                $('#search_farmasetis').val("");
                for (let i = 0; i < data.length; i++) {
                    let no = i + 1;
                    tr = '<tr>\n\
                            <td>' + data[i].nama_obat + '</td>\n\
                            <td>' + data[i].bentuk_sediaan + '</td>\n\
                            <td><buttom class="btn btn-default" onclick="selectFarmasetis(\'' + data[i].id + '\',\'' +
                        data[i].nama_obat + '\',\'' +
                        data[i].bentuk_sediaan + '\',\'' +
                        data[i].jumlah_obat + '\',\'' +
                        data[i].aturan_pakai + '\')\n\
                ">Pilih</buttom>\n\
                            &nbsp;&nbsp;&nbsp<div class="btn btn-default" onclick="showDataFarmasetisDetail(\'' + data[
                            i].id + '\')">Detail</div>\n\
                            </td>\n\
                        </tr>';
                    $('#farmasetis_id').append(tr);
                }
            })
        }, 1000)
    } else if (keyword.length < 3) {
        $('#table_farmasetis').hide();
    }
}

function pilihPasien(id, nama_pasien) {
    let flag = false;
    let obj = {
        'id': id,
        'nama_pasien': nama_pasien
    }
    $('#nama_pasien').text("Nama Pasien : " + nama_pasien);
    $('#table_administrasi').hide();
    $('#administrasi_id').val(id);
}

function selectFarmasetis(id, nama_obat, bentuk_sediaan, jumlah_obat, aturan_pakai) {
    setTimeout(function() {
        $('#farmasetis_id').empty();
    }, 2000);
    let flag = false;
    let obj = {
        'id': id,
        'nama_obat': nama_obat,
        'bentuk_sediaan': bentuk_sediaan,
        'jumlah_obat': jumlah_obat,
        'aturan_pakai': aturan_pakai

    }
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            toastr.warning(nama_obat + " sudah tersedia");
            flag = true;
            break;
        }
    }

    if (!flag) {
        drags.push(obj);
        buildResep(id, nama_obat, bentuk_sediaan, jumlah_obat, aturan_pakai)
        // drawFarmasetis()
        if (drags.length > 1) {
            comparation()
        }
    }
}

function buildResep(id, nama_obat, bentuk_sediaan, jumlah_obat, aturan_pakai) {
    if (drags.length == 1) {
        $('#farmasetis_selected').empty();
        let th = '<tr>\n\
                        <th colspan="3"><label for="exampleInputEmail1">Resep Obat</label></th>\n\
                    </tr>\n\
                    <tr>\n\
                        <th>Obat</th>\n\
                        <th>Bentuk Sediaan</th>\n\
                        <th>Jumlah Obat</th>\n\
                        <th>Aturan Pakai</th>\n\
                        <th colspan="2"></th>\n\
                    </tr>';
        $('#farmasetis_selected').append(th);
    }

    let tr = '<tr id="farmasetis_' + id + '">\n\
                            <td>' + nama_obat + ' <input type="hidden" id="farmasetis_id_' + id + '" value="' + id + '" ></td>\n\
                            <td>\n\
                                <select class="form-control" id="bentuk_sediaan_' + id + '">\n\
                                <option value="Pulvis (serbuk)">Pulvis (serbuk)</option>\n\
                                    <option value="Pulveres">Pulveres</option>\n\
                                    <option value="Tablet (compressi)">Tablet (compressi)</option>\n\
                                    <option value="Pilulae (PIL)">Pilulae (PIL)</option>\n\
                                    <option value="Kapsul (capsule)">Kapsul (capsule)</option>\n\
                                    <option value="Solutiones (Larutan)">Solutiones (Larutan)</option>\n\
                                    <option value="Suspensi">Suspensi</option>\n\
                                    <option value="Emulsi">Emulsi (elmusiones)</option>\n\
                                    <option value="Infusa">Infusa</option>\n\
                                    <option value="Imunoserum">Imunoserum (immunosera)</option>\n\
                                    <option value="Salep">Salep (unguenta)</option>\n\
                                    <option value="Suppositoria">Suppositoria</option>\n\
                                    <option value="Obat tetes">Obat tetes (guttae)</option>\n\
                                    <option value="Injeksi">Injeksi (injectiones)</option>\n\
                                    <option value="Aerosol">Aerosol</option>\n\
                                    <option value="Gel">Gel</option>\n\
                                    <option value="Ovula">Ovula</option>\n\
                                    <option value="Implan">Implan</option>\n\
                                    <option value="Obat Kumur (Gargle)">Obat Kumur (Gargle)</option>\n\
                                    <option value="Sirup">Sirup</option>\n\
                                    <option value="Elixir">Elixir</option>\n\
                                    <option value="potio">potio</option>\n\
                                </select>\n\
                            </td>\n\
                            <td><input type="text" class="form-control" id="jumlah_obat_' + id +
        '" placeholder="jumlah obat" value="' + jumlah_obat + '"></td>\n\
                            <td><input type="text" class="form-control" id="aturan_pakai_' + id +
        '" placeholder="aturan pakai" value="' + aturan_pakai + '"></td>\n\
                            <td><div class="btn btn-danger" onclick="deleteFarmasetis(\'' + id + '\',\'' +
        nama_obat + '\')\n\
                ">Hapus</div></td>\n\
                <td><div class="btn btn-default" onclick="showDataFarmasetisDetail(\'' + id + '\')">Detail Master Obat</div>\n\
                            </td>\n\
                        </tr>';

    let tr_obat = '<tr>\n\
                            <td class="center"></td>\n\
                            <td class="left strong">' + nama_obat + '</td>\n\
                            <td class="left">' + bentuk_sediaan + '</td>\n\
                            <td class="right">' + jumlah_obat + '</td>\n\
                            <td class="right">' + aturan_pakai + '</td>\n\
                        </tr>';
    $('#farmasetis_selected').append(tr);
    $('#inv_obat').append(tr_obat);
    $('#bentuk_sediaan_' + id).val(bentuk_sediaan);
}

function drawFarmasetis() {
    $('#farmasetis_selected').empty();
    if (drags.length != 0) {
        let th = '<tr>\n\
                        <th colspan="3"><label for="exampleInputEmail1">Resep Obat</label></th>\n\
                    </tr>\n\
                    <tr>\n\
                        <th>No</th>\n\
                        <th>Obat</th>\n\
                        <th>Bentuk Sediaan</th>\n\
                        <th>Jumlah Obat</th>\n\
                        <th>Aturan Pakai</th>\n\
                        <th colspan="2"></th>\n\
                    </tr>';
        $('#farmasetis_selected').append(th);
    }

    $('#inv_obat').empty();
    for (let i = 0; i < drags.length; i++) {
        let no = i + 1;
        let optionValue = drags[i].bentuk_sediaan;
        let tr = '<tr id="farmasetis_' + drags[i].id + '">\n\
                            <td>' + no + ' <input type="hidden" id="farmasetis_id_' + i + '" value="' + drags[i].id + '" ></td>\n\
                            <td>' + drags[i].nama_obat + '</td>\n\
                            <td>\n\
                                <select class="form-control" id="bentuk_sediaan_' + i + '">\n\
                                <option value="Pulvis (serbuk)">Pulvis (serbuk)</option>\n\
                                    <option value="Pulveres">Pulveres</option>\n\
                                    <option value="Tablet (compressi)">Tablet (compressi)</option>\n\
                                    <option value="Pilulae (PIL)">Pilulae (PIL)</option>\n\
                                    <option value="Kapsul (capsule)">Kapsul (capsule)</option>\n\
                                    <option value="Solutiones (Larutan)">Solutiones (Larutan)</option>\n\
                                    <option value="Suspensi">Suspensi</option>\n\
                                    <option value="Emulsi">Emulsi (elmusiones)</option>\n\
                                    <option value="Infusa">Infusa</option>\n\
                                    <option value="Imunoserum">Imunoserum (immunosera)</option>\n\
                                    <option value="Salep">Salep (unguenta)</option>\n\
                                    <option value="Suppositoria">Suppositoria</option>\n\
                                    <option value="Obat tetes">Obat tetes (guttae)</option>\n\
                                    <option value="Injeksi">Injeksi (injectiones)</option>\n\
                                    <option value="Aerosol">Aerosol</option>\n\
                                    <option value="Gel">Gel</option>\n\
                                    <option value="Ovula">Ovula</option>\n\
                                    <option value="Implan">Implan</option>\n\
                                    <option value="Obat Kumur (Gargle)">Obat Kumur (Gargle)</option>\n\
                                    <option value="Sirup">Sirup</option>\n\
                                    <option value="Elixir">Elixir</option>\n\
                                    <option value="potio">potio</option>\n\
                                </select>\n\
                            </td>\n\
                            <td><input type="text" class="form-control" id="jumlah_obat_' + i +
            '" placeholder="jumlah obat" value="' + drags[i].jumlah_obat + '"></td>\n\
                            <td><input type="text" class="form-control" id="aturan_pakai_' + i +
            '" placeholder="aturan pakai" value="' + drags[i].aturan_pakai + '"></td>\n\
                            <td><div class="btn btn-danger" onclick="deleteFarmasetis(\'' + drags[i].id + '\',\'' +
            drags[i].nama_obat + '\')\n\
                ">Hapus</div></td>\n\
                <td><div class="btn btn-default" onclick="showDataFarmasetisDetail(\'' + drags[i].id + '\')">Detail Master Obat</div>\n\
                            </td>\n\
                        </tr>';

        let tr_obat = '<tr>\n\
                            <td class="center">' + no + '</td>\n\
                            <td class="left strong">' + drags[i].nama_obat + '</td>\n\
                            <td class="left">' + optionValue + '</td>\n\
                            <td class="right">' + drags[i].jumlah_obat + '</td>\n\
                            <td class="right">' + drags[i].aturan_pakai + '</td>\n\
                        </tr>';
        $('#farmasetis_selected').append(tr);
        $('#inv_obat').append(tr_obat);
        $('#bentuk_sediaan_' + i).val(optionValue);
    }
}

function deleteFarmasetis(id, nama_obat) {
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            drags.splice(i, 1);
            $('#farmasetis_' + id).remove();
            if (drags.length == 0) {
                $('#farmasetis_selected').empty();
            }
            // drawFarmasetis()
            comparation()
            break;
        }
    }
}

function comparation() {
    $('#comparation').empty();
    let dragsTemp = [];
    dragsTemp = drags;
    let obatTemp = [];
    for (let i = 0; i < dragsTemp.length; i++) {
        for (let j = 0; j < drags.length; j++) {
            if (dragsTemp[i].id != drags[j].id) {

                let obj = {
                    'o1': dragsTemp[i].id,
                    'o2': drags[j].id
                }
                obatTemp.push(obj);
                for (let k = 0; k < obatTemp.length; k++) {
                    if (obatTemp[k].o1 == drags[j].id && obatTemp[k].o2 == dragsTemp[i].id) {
                        console.log(dragsTemp[i].nama_obat + " <  > " + drags[j].nama_obat)
                        interaksiObat(dragsTemp[i].id, drags[j].id);
                    }
                }
            }
        }
    }
}

function interaksiObat(obat_a, obat_b) {
    $.ajax({
        method: "GET",
        url: "KombinasiController/dragCompare",
        contentType: "application/json",
        data: {
            'obat_a': obat_a,
            'obat_b': obat_b
        }
    }).done(function(response) {
        let data = JSON.parse(response);
        if (data == null) {
            return;
        }
        console.log(data)
        let obat_a;
        let obat_b;
        let button = "";
        for (let i = 0; i < drags.length; i++) {
            if (drags[i].id == data.farmasetis_x) {
                obat_a = drags[i].nama_obat;
            }

            if (drags[i].id == data.farmasetis_y) {
                obat_b = drags[i].nama_obat;
            }
        }

        if (data.interaksi_obat == 'Major') {
            toastr.error(obat_a + ' < >' + obat_b + ' = ' + data.interaksi_obat);
            button = "<button class='btn' style='background-color:#ff0000; color:white'>Major</button>";
        } else if (data.interaksi_obat == 'Moderat') {
            toastr.warning(obat_a + ' < >' + obat_b + ' = ' + data.interaksi_obat);
            button = "<button class='btn' style='background-color:orange; color:white'>Moderat</button>";
        } else if (data.interaksi_obat == 'Minor') {
            toastr.info(obat_a + ' < >' + obat_b + ' = ' + data.interaksi_obat);
            button = "<button class='btn' style='background-color:#2f96b4; color:white'>Minor</button>";
        } else if (data.interaksi_obat == 'Indikasi') {
            toastr.success(obat_a + ' < >' + obat_b + ', Peringatan! Memiliki Indikasi Yang Sama');
            button =
                "<button class='btn' style='background-color:yellow; color:black'>Indikasi</button><br><p style='font-size: 14px;'>Peringatan! Memiliki Indikasi Yang Sama</p>";
        }

        let tag = '<tr>\n\
                    <th>' + obat_a + '</th>\n\
                    <th>&lt; &gt;</th>\n\
                    <th>' + obat_b + '</th>\n\
                    <th>' + button + '</th>\n\
                   </tr>'
        $('#comparation').append(tag)
    })
}

function openPreview() {
    $('#print_area').show();
    $('#form_area').hide();
    // printJS('print_area', 'html');
    // https: //printjs.crabbly.com/
    // printJS({
    //     printable: myData,
    //     type: 'json',
    //     properties: ['prop1', 'prop2', 'prop3']
    // });
}

function closePreview() {
    $('#print_area').hide();
    $('#form_area').show();
}

function print() {

}

function submitKlinis() {
    let value = [];
    for (let i = 0; i < drags.length; i++) {
        let farmasetis_id = $('#farmasetis_id_' + i).val();
        let bentuk_sediaan = $('#bentuk_sediaan_' + i).val();
        let jumlah_obat = $('#jumlah_obat_' + i).val();
        let aturan_pakai = $('#aturan_pakai_' + i).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        value.push(obj);
    }

    $.ajax({
        method: "POST",
        url: "insertKlinis",
        contentType: "json",
        data: JSON.stringify({
            "administrasi_id": parseInt($('#administrasi_id').val()),
            "indikasi_obat": $('#indikasi_obat').val(),
            "dosis_obat": $('#dosis_obat').val(),
            "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
            "tepat_waktu": $('#tepat_waktu').val(),
            "duplikasi": $('#duplikasi').val(),
            "alergi": $('#alergi').val(),
            "interaksi_obat": $('#interaksi_obat').val(),
            "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
            "efek_samping": $('#efek_samping').val(),
            "resep_obat": value
        })
    }).done(function(response) {
        console.log(response)
    })
}
</script>