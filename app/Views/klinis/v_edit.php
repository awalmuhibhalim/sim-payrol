<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
            <form role="form" action="<?= base_url('update_klinis/'.$klinis['id'])?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Indikasi Obat</label>
                                <input type="text" name="indikasi_obat" class="form-control" placeholder="" value="<?= $klinis['indikasi_obat'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Dosis Obat</label>
                                <input type="text"  name="dosis_obat" class="form-control" placeholder="" value="<?= $klinis['dosis_obat'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Rute Pemberian Obat</label>
                                <input type="text"  name="rute_pemberian_obat" class="form-control" placeholder="" value="<?= $klinis['rute_pemberian_obat'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tepat Waktu</label>
                                <input type="text"  name="tepat_waktu" class="form-control" placeholder="" value="<?= $klinis['tepat_waktu'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Duplikasi</label>
                                <input type="text"  name="duplikasi" class="form-control" placeholder="" value="<?= $klinis['duplikasi'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Alergi</label>
                                <input type="text"  name="alergi" class="form-control" placeholder="" value="<?= $klinis['alergi'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Interaksi Obat</label>
                                <textarea name="interaksi_obat" class="form-control" placeholder=""><?= $klinis['interaksi_obat'];?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Kontraindikasi Obat</label>
                                <input type="text"  name="kontraindikasi_obat" class="form-control" placeholder="" value="<?= $klinis['kontraindikasi_obat'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Efek Samping</label>
                                <input type="text"  name="efek_samping" class="form-control" placeholder="" value="<?= $klinis['efek_samping'];?>">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </form>
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

