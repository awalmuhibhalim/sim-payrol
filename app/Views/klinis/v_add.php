<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form role="form" action="<?= base_url('add_klinis') ?>" method="post">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pasien</label>
                                    <input type="text" class="form-control" id="search" placeholder="search..."
                                        onkeyup="getAdminstrasi()" />
                                    <input type="hidden" class="form-control" id="administrasi_id" />
                                    <table id="table_administrasi"
                                        class="table table-striped table-bordered table-hover" style="display:none">
                                        <tbody id="administrasi_list"></tbody>
                                    </table>
                                    <p id="nama_pasien"></p>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Obat</label>
                                    <input type="text" class="form-control" id="search_farmasetis"
                                        placeholder="search..." onkeyup="getFarmasetis()" />
                                    <table id="table_farmasetis" class="table table-striped table-bordered table-hover">
                                        <tbody id="farmasetis_id"></tbody>
                                    </table>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                            </tr>
                                        </thead>
                                        <tbody id="farmasetis_selected"></tbody>
                                    </table>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                            </tr>
                                        </thead>
                                        <tbody id="comparation">
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Indikasi Obat</label>
                                    <input type="text" name="indikasi_obat" id="indikasi_obat" class="form-control"
                                        placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Dosis Obat</label>
                                    <input type="text" name="dosis_obat" id="dosis_obat" class="form-control"
                                        placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Rute Pemberian Obat</label>
                                    <input type="text" name="rute_pemberian_obat" id="rute_pemberian_obat"
                                        class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tepat Waktu</label>
                                    <input type="text" name="tepat_waktu" id="tepat_waktu" class="form-control"
                                        placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Duplikasi</label>
                                    <input type="text" name="duplikasi" id="duplikasi" class="form-control"
                                        placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Alergi</label>
                                    <input type="text" name="alergi" id="alergi" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Interaksi Obat</label>
                                    <textarea name="interaksi_obat" id="interaksi_obat" class="form-control"
                                        placeholder=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Kontraindikasi Obat</label>
                                    <input type="text" name="kontraindikasi_obat" id="kontraindikasi_obat"
                                        class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Efek Samping</label>
                                    <input type="text" name="efek_samping" id="efek_samping" class="form-control"
                                        placeholder="">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <div class="btn btn-primary" onclick="submitKlinis()">Submit</button>
                                </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {

})
let drags = [];
let timeout;

function getAdminstrasi() {
    let keyword = $('#search').val().trim();
    if (keyword.length >= 2) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "AdministrasiController/showAdministrasionList",
                contentType: "application/json",
                data: {
                    'keyword': keyword
                },
            }).done(function(response) {
                let data = JSON.parse(response);
                if (data == null) {
                    return
                }
                $('#table_administrasi').show();
                $('#administrasi_list').empty();
                let tr = '<tr>\n\
                        <th>No</th>\n\
                        <th>No Rekam Medis</th>\n\
                        <th>Nama Pasien</th>\n\
                        <th>Nama Dokter</th>\n\
                        <th></th>\n\
                    </tr>';
                $('#administrasi_list').append(tr);
                for (let i = 0; i < data.length; i++) {
                    let no = i + 1;
                    tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].no_rekam_medis + '</td>\n\
                            <td>' + data[i].nama_pasien + '</td>\n\
                            <td>' + data[i].nama_dokter + '</td>\n\
                            <td><buttom class="btn btn-info" onclick="pilihPasien(\'' + data[i].id + '\',\'' + data[i]
                        .nama_pasien + '\')">Select</buttom>\n\
                            </td>\n\
                        </tr>';
                    $('#administrasi_list').append(tr);
                }
            })
        }, 1000)
    } else if (keyword.length < 3) {
        $('#table_administrasi').hide();
    }
}

function getFarmasetis() {
    let keyword = $('#search_farmasetis').val();
    if (keyword.length >= 2) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "FarmasetisController/serachFarmasetis",
                contentType: "application/json",
                data: {
                    'keyword': keyword
                },
            }).done(function(response) {
                let data = JSON.parse(response);
                if (data == null) {
                    return;
                }
                $('#table_farmasetis').show();
                $('#farmasetis_id').empty();
                let tr = '<tr>\n\
                        <th>No</th>\n\
                        <th>Nama Obat</th>\n\
                        <th>Satuan</th>\n\
                        <th>Aturan Pakai</th>\n\
                        <th></th>\n\
                    </tr>';
                $('#farmasetis_id').append(tr);
                for (let i = 0; i < data.length; i++) {
                    let no = i + 1;
                    tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].nama_obat + '</td>\n\
                            <td>' + data[i].bentuk_sediaan + '</td>\n\
                            <td>' + data[i].aturan_pakai + '</td>\n\
                            <td><buttom class="btn btn-info" onclick="selectFarmasetis(\'' + data[i].id + '\',\'' +
                        data[i].nama_obat + '\')\n\
                ">Select</buttom>\n\
                            </td>\n\
                        </tr>';
                    $('#farmasetis_id').append(tr);
                }
            })
        }, 1000)
    } else if (keyword.length < 3) {
        $('#table_farmasetis').hide();
    }
}

function pilihPasien(id, nama_pasien) {
    let flag = false;
    let obj = {
        'id': id,
        'nama_pasien': nama_pasien
    }
    $('#nama_pasien').text("Nama Pasien : " + nama_pasien);
    $('#table_administrasi').hide();
    $('#administrasi_id').val(id);
}

function selectFarmasetis(id, nama_obat) {
    let flag = false;
    let obj = {
        'id': id,
        'nama_obat': nama_obat
    }
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            alert(nama_obat + " already exist");
            flag = true;
            break;
        }
    }

    if (!flag) {
        drags.push(obj);
        drawFarmasetis()
        if (drags.length > 1) {
            comparation()
        }
    }
}

function drawFarmasetis() {
    $('#farmasetis_selected').empty();
    for (let i = 0; i < drags.length; i++) {
        let no = i + 1;
        let tr = '<tr id="farmasetis_' + drags[i].id + '">\n\
                            <td>' + no + '</td>\n\
                            <td><input type="hidden" id="farmasetis_id_' + i + '" value="' + drags[i].id + '" ></td>\n\
                            <td>' + drags[i].nama_obat + '</td>\n\
                            <td>\n\
                                <select class="form-control" id="bentuk_sediaan_' + i + '">\n\
                                    <option>Bentuk Sediaan</option>\n\
                                    <option value="tablet">Tablet</option>\n\
                                    <option value="cair">Cair</option>\n\
                                    <option value="bubuk">Bubuk</option>\n\
                                </select>\n\
                            </td>\n\
                            <td><input type="text" class="form-control" id="jumlah_obat_' + i + '" placeholder="jumlah obat"></td>\n\
                            <td><input type="text" class="form-control" id="aturan_pakai_' + i + '" placeholder="aturan pakai"></td>\n\
                            <td><buttom class="btn btn-danger" onclick="deleteFarmasetis(\'' + drags[i].id + '\',\'' +
            drags[i].nama_obat + '\')\n\
                ">Remove</buttom>\n\
                            </td>\n\
                        </tr>';
        $('#farmasetis_selected').append(tr);
    }
}

function deleteFarmasetis(id, nama_obat) {
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            drags.splice(i, 1);
            $('#farmasetis_' + id).remove();
            drawFarmasetis()
            comparation()
            break;
        }
    }
}

function comparation() {
    $('#comparation').empty();
    let dragsTemp = [];
    dragsTemp = drags;
    let obatTemp = [];
    for (let i = 0; i < dragsTemp.length; i++) {
        for (let j = 0; j < drags.length; j++) {
            if (dragsTemp[i].id != drags[j].id) {

                let obj = {
                    'o1': dragsTemp[i].id,
                    'o2': drags[j].id
                }
                obatTemp.push(obj);
                for (let k = 0; k < obatTemp.length; k++) {
                    if (obatTemp[k].o1 == drags[j].id && obatTemp[k].o2 == dragsTemp[i].id) {
                        console.log(dragsTemp[i].nama_obat + " <  > " + drags[j].nama_obat)
                        interaksiObat(dragsTemp[i].id, drags[j].id);
                    }
                }
            }
        }
    }
}

function interaksiObat(obat_a, obat_b) {
    $.ajax({
        method: "GET",
        url: "KombinasiController/dragCompare",
        contentType: "application/json",
        data: {
            'obat_a': obat_a,
            'obat_b': obat_b
        }
    }).done(function(response) {
        let data = JSON.parse(response);
        if (data == null) {
            return;
        }
        console.log(data)
        let obat_a;
        let obat_b;
        let button = "";
        for (let i = 0; i < drags.length; i++) {
            if (drags[i].id == data.farmasetis_x) {
                obat_a = drags[i].nama_obat;
            }

            if (drags[i].id == data.farmasetis_y) {
                obat_b = drags[i].nama_obat;
            }
        }

        if (data.interaksi_obat == 'Major') {
            button = "<button class='btn' style='background-color:#ff0000; color:white'>Major</button>";
        } else if (data.interaksi_obat == 'Moderat') {
            button = "<button class='btn' style='background-color:#ff9999; color:white'>Moderate</button>";
        } else if (data.interaksi_obat == 'Minor') {
            button = "<button class='btn' style='background-color:#ffe6e6; color:black'>Minor</button>";
        }

        let tag = '<tr>\n\
                    <th>' + obat_a + '</th>\n\
                    <th>&lt; &gt;</th>\n\
                    <th>' + obat_b + '</th>\n\
                    <th>' + button + '</th>\n\
                   </tr>'
        $('#comparation').append(tag)
    })
}

function submitKlinis() {
    let value = [];
    for (let i = 0; i < drags.length; i++) {
        let farmasetis_id = $('#farmasetis_id_' + i).val();
        let bentuk_sediaan = $('#bentuk_sediaan_' + i).val();
        let jumlah_obat = $('#jumlah_obat_' + i).val();
        let aturan_pakai = $('#aturan_pakai_' + i).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        value.push(obj);
    }

    $.ajax({
        method: "POST",
        url: "insertKlinis",
        contentType: "json",
        data: JSON.stringify({
            "administrasi_id": parseInt($('#administrasi_id').val()),
            "indikasi_obat": $('#indikasi_obat').val(),
            "dosis_obat": $('#dosis_obat').val(),
            "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
            "tepat_waktu": $('#tepat_waktu').val(),
            "duplikasi": $('#duplikasi').val(),
            "alergi": $('#alergi').val(),
            "interaksi_obat": $('#interaksi_obat').val(),
            "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
            "efek_samping": $('#efek_samping').val(),
            "resep_obat": value
        })
    }).done(function(response) {
        console.log(response)
    })
}
</script>