<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div type="button" class="btn btn-primary golongan_btn_add"
                                            onclick="openform()">Tambah
                                            Data</div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kategori</th>
                                                <th>Ranges</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Golongan</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('addgolongan'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kategori</label>
                            <input type="text" id="kategori" class="form-control" placeholder="Kategori">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Ranges</label>
                            <input type="text" id="ranges" class="form-control" placeholder="Ranges">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="reset" class="btn btn-warning" data-dismiss="modal">Reset</div>
                    <div id="save" class="btn btn-primary" onclick="save()">Save</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_golongan') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                },
                {
                    "data": "kategori"
                },
                {
                    "data": "ranges"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary golongan_btn_view">View</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' +
                            row
                            .id +
                            '\')" class="btn btn-danger golongan_btn_delete">Delete</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});


function openform() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "GolonganFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        console.log(data.kategori)
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#kategori').val(data.kategori);
        $('#ranges').val(data.ranges);
    })
}

function save() {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "kategori": $('#kategori').val(),
        "ranges": $('#ranges').val()
    };

    $.ajax({
        method: "POST",
        url: "insertGolongan",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function update(id) {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "kategori": $('#kategori').val(),
        "ranges": $('#ranges').val()
    };

    $.ajax({
        method: "POST",
        url: "updateGolongan/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function validation() {
    if ($('#kategori').val() == null) {
        toastr.error("kategori tidak boleh kosong");
        return false;
    }

    if ($('#ranges').val() == null) {
        toastr.error("ranges tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete golongan',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteGolongan/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Golongan berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });
}

function openFormGenerate() {
    $('#detail_gaji').empty();
    $('#modal-generate').modal({
        'backdrop': 'static'
    });
}

function printGaji() {
    if ($('#bulan').val().trim() == "" || $('#bulan').val() == null) {
        toastr.warning("Bulan belum dipilih");
        return;
    }

    if ($('#tahun').val().trim() == "" || $('#tahun').val() == null) {
        toastr.warning("Tahun belum dipilih");
        return;
    }
    let value = {
        'month': $('#bulan').val() + "-" + $('#tahun').val()
    }
    $.ajax({
        method: 'POST',
        url: 'DetailGajiController/findByMonth',
        contentType: 'application/json',
        data: JSON.stringify(value),
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        $('#gaji_list').empty();
        $('#modal-generate').modal({
            'backdrop': 'static'
        });
        for (let i = 0; i < data.length; i++) {
            let no = i + 1;
            let keterangan = data[i].keterangan.split("=");
            let detailKeterangan = '<div><div>' + keterangan[0] + '</div>\n\
            <div>' + keterangan[1] + '</div>\n\
            <div>' + keterangan[2] + '</div></div>';
            console.log(detailKeterangan)
            let tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].nama_karyawan + '</td>\n\
                            <td>' + data[i].jabatan + '</td>\n\
                            <td>' + data[i].total_gaji + '</td>\n\
                            <td>' + detailKeterangan + '</td>\n\
                            <td>' + data[i].tanggal + '</td>\n\
                        </tr>';

            $('#gaji_list').append(tr)
        }

    })
}

function cetak(i) {
    $('#detail_gaji').printElement({});
}
</script>