<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIM-PAYROL | <?= $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/fontawesome-free/css/all.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
        href="<?php base_url() ?> template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php base_url() ?> template/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/summernote/summernote-bs4.css">

    <link rel="stylesheet" href="<?php base_url() ?> template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="<?php base_url() ?> template/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link href="<?php base_url() ?> template/assets/css/jquery-confirm.min.css?v=5" rel="stylesheet">

    <link href="<?php base_url() ?> template/assets/css/font-awesome.css?v=5" rel="stylesheet" />
    <link href="<?php base_url() ?> template/assets/css/bootstrap-select.css?v=5" rel="stylesheet">
    <link href="<?php base_url() ?> template/assets/css/print-min.css?v=5" rel="stylesheet">
    <link href="<?php base_url() ?> template/assets/toastr/toastr.min.css?v=5" rel="stylesheet">
    <link href="<?php base_url() ?> template/assets/css/bootstrap-clockpicker.min.css?v=5" rel="stylesheet" />
    <link href="<?php base_url() ?> template/assets/css/github.min.css?v=5" rel="stylesheet" />
</head>