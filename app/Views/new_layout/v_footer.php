<footer class="main-footer">
    <strong>Copyright &copy; 2014-2019
        <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.0.5
    </div>
</footer>
<aside class="control-sidebar control-sidebar-dark">
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php base_url() ?> template/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php base_url() ?> template/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge("uibutton", $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="<?php base_url() ?> template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php base_url() ?> template/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<!-- <script src="<?php base_url() ?> template/plugins/sparklines/sparkline.js"></script> -->
<!-- JQVMap -->
<script src="<?php base_url() ?> template/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php base_url() ?> template/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php base_url() ?> template/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php base_url() ?> template/plugins/moment/moment.min.js"></script>
<script src="<?php base_url() ?> template/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php base_url() ?> template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js">
</script>
<!-- Summernote -->
<script src="<?php base_url() ?> template/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php base_url() ?> template/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php base_url() ?> template/dist/js/adminlte.js"></script>

<script src="<?php base_url() ?> template/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php base_url() ?> template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php base_url() ?> template/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php base_url() ?> template/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="<?php base_url() ?>template/assets/js/bootstrap-select.js?v=5"></script>
<script src="<?php base_url() ?>template/assets/js/jquery-confirm.min.js?v=5"></script>

<script src="<?php base_url() ?> template/dist/js/demo.js"></script>
<script src="<?php base_url() ?>template/assets/toastr/toastr.min.js?v=5"></script>
<!-- <script src="<?php base_url() ?>template/assets/js/custom.js?v=5"></script> -->
<script src="<?php base_url() ?>template/assets/js/jquery-ui.js?v=5"></script>
<script src="<?php base_url() ?>template/assets/js/global.js?v=5"></script>
<script src="<?php base_url() ?>template/assets/js/bootstrap-clockpicker.min.js?v=5"></script>
<script src="<?php base_url() ?>template/assets/js/highlight.min.js?v=5"></script>
<script src="<?php base_url() ?>template/assets/js/divjs/divjs.js?v=5"></script>
</body>

</html>