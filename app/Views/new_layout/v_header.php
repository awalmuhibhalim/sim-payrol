<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fa fa-power-off"></i>
                        <span class="badge badge-warning navbar-badge"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header"></span>
                        <div class="dropdown-divider"></div>
                        <a href="<?= base_url('logout'); ?>" class="dropdown-item">
                            <i class="fa fa-sign-out"></i> Keluar
                            <!-- <span class="float-right text-muted text-sm">3 mins</span> -->
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="<?= base_url() ?>" class="brand-link">
                <img src="<?php base_url() ?> template/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: 0.8" />
                    <span class="brand-text font-weight-light">BPRS</span><br>
                <span class="brand-text font-weight-light">RIF'ATUL UMMAH</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="<?php base_url() ?> template/dist/img/user2-160x160.jpg"
                            class="img-circle elevation-2" alt="User Image" />
                    </div>
                    <div class="info">
                        <a href="#" class="d-block"><?= $_SESSION['nama_karyawan'] ?></a>
                        <a href="#" class="d-block">-<?= $_SESSION['jabatan'] ?>-</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Master
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item menu_karyawan">
                                    <a href="<?= base_url('menu_karyawan') ?>" class="nav-link active">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Karyawan</p>
                                    </a>
                                </li>
                                <li class="nav-item menu_gaji">
                                    <a href="<?= base_url('menu_gaji') ?>"" class=" nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Gaji</p>
                                    </a>
                                </li>
                                <li class="nav-item menu_tunjangan">
                                    <a href="<?= base_url('menu_tunjangan') ?>" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Tunjangan</p>
                                    </a>
                                </li>
                                <li class="nav-item menu_golongan">
                                    <a href="<?= base_url('menu_golongan') ?>" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Golongan</p>
                                    </a>
                                </li>
                                <li class="nav-item menu_hutang">
                                    <a href="<?= base_url('menu_hutang') ?>" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Hutang</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item menu_absensi">
                            <a href="<?= base_url('menu_absensi') ?>" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    Absensi
                                </p>
                            </a>
                        </li>
                        <li class="nav-item menu_lembur">
                            <a href="<?= base_url('menu_lembur') ?>" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Lembur
                                </p>
                            </a>
                        </li>
                        <li class="nav-item menu_detail_gaji">
                            <a href="<?= base_url('menu_detail_gaji') ?>" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Laporan Gaji
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>