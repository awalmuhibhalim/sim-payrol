<?php
if (!isset($_SESSION['username'])) {
    echo view('login/login');
} else {
    if ($_SESSION['jabatan'] == "Staff") {
        echo view('new_layout/v_head');
        echo view('new_layout/v_header');
        echo view('new_layout/v_content');
        echo view('new_layout/v_footer');
    } else {
        echo view('new_layout/v_head');
        echo view('new_layout/v_header');
        echo view('new_layout/v_content');
        echo view('new_layout/v_footer');
    }
}