<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SIMRS : <?= $title; ?></title>
    <!-- BOOTSTRAP STYLES-->
    <link href="<?php base_url() ?> template/assets/css/bootstrap.css?v=5" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php base_url() ?> template/assets/css/font-awesome.css?v=5" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="<?php base_url() ?> template/assets/css/custom.css?v=5" rel="stylesheet" />
    <link href="<?php base_url() ?> template/assets/css/jquery-confirm.min.css?v=5" rel="stylesheet">
    <!-- GOOGLE FONTS-->
    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> -->
    <!-- TABLE STYLES-->
    <link href="<?php base_url() ?> template/assets/js/dataTables/dataTables.bootstrap.css?v=5" rel="stylesheet" />
    <link href="<?php base_url() ?> template/assets/css/bootstrap-select.css?v=5" rel="stylesheet">
    <link href="<?php base_url() ?> template/assets/css/print-min.css?v=5" rel="stylesheet">
    <link href="<?php base_url() ?> template/assets/toastr/toastr.min.css?v=5" rel="stylesheet">
    <link href="<?php base_url() ?> template/assets/css/jquery-ui.css?v=5" rel="stylesheet" />

</head>