<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="<?php base_url() ?> template/assets/img/find_user.png" class="user-image img-responsive" />
            </li>


            <li>
                <a href="<?= base_url('index') ?>"><i class="fa fa-dashboard fa-2x"></i> Home</a>
            </li>
            <li>
                <a href="<?= base_url('menu_karyawan') ?>"><i class="fa fa-desktop fa-2x"></i> Karyawan</a>
            </li>
            <li>
                <a href="<?= base_url('administrasilist') ?>"><i class="fa fa-laptop fa-2x"></i> Absensi</a>
            </li>
            <li>
                <a href="<?= base_url('gaji_list') ?>"><i class="fa fa-bar-chart-o fa-2x"></i> Gaji</a>
            </li>
            <li>
                <a href="<?= base_url('klinis_list') ?>"><i class="fa fa-qrcode fa-2x"></i> Tunjangan</a>
            </li>
            <li>
                <a href="<?= base_url('kombinasi_list') ?>"><i class="fa fa-table fa-2x"></i> Lembur</a>
            </li>

            <li>
                <a href="#"><i class="fa fa-sitemap fa-2x"></i> Multi-Level Dropdown<span class="fa arrow "></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Link</a>
                    </li>
                    <li>
                        <a href="#">Second Level Link</a>
                    </li>
                    <li>
                        <a href="#">Second Level Link<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>
                            <li>
                                <a href="#">Third Level Link</a>
                            </li>

                        </ul>

                    </li>
                </ul>
            </li>
            <li>
                <a class="active-menu" href="blank.html"><i class="fa fa-square-o fa-2x"></i> Blank Page</a>
            </li>
            <!-- <li  >
                <a  href="form.html"><i class="fa fa-edit fa-2x"></i> Drugs Combinations </a>
            </li>				
            <li  >
                <a   href="login.html"><i class="fa fa-bolt fa-2x"></i> Login</a>
            </li>	 -->
        </ul>

    </div>

</nav>
<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2><?= $title ?></h2>


            </div>
        </div>
        <!-- /. ROW  -->
        <hr />