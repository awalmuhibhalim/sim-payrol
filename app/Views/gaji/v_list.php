<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-1.5">
                                        <div type="button" class="btn btn-primary gaji_btn_add" onclick="openform()">
                                            Tambah
                                            Data</div>
                                    </div>
                                    <div class="col-md-2">
                                        <div type="button" class="btn btn-warning gaji_btn_hitung"
                                            onclick="openFormGenerate()">Hitung
                                            Gaji
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Gaji Pokok</th>
                                                <th>Tanggal Gabung</th>
                                                <th>Golongan</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Gaji</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Karyawan</label>

                            <select class="selectpicker form-control" data-container="body" data-live-search="true"
                                title="Select a number" data-hide-disabled="true" id="karyawan_id">
                                <option value="">Iwan</option>
                                <option value="">Rudy</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Golongan</label>

                            <select class="selectpicker form-control" data-container="body" data-live-search="true"
                                title="Select a number" data-hide-disabled="true" id="golongan_id">
                                <option value=""></option>
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Gaji Pokok</label>
                            <input type="number" id="gaji_pokok" class="form-control" placeholder="Enter Password">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="reset" class="btn btn-warning" data-dismiss="modal">Reset</div>
                    <div id="save" class="btn btn-primary gaji_btn_save" onclick="save()">Save</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-generate">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hitung Gaji</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Start</label>
                            <input type="text" id="start" class="form-control start datepicker datetimepicker-input"
                                data-toggle="datetimepicker" data-target=".start" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">End</label>
                            <input type="text" id="end" class="form-control end datepicker datetimepicker-input"
                                data-toggle="datetimepicker" data-target=".end" />
                        </div>
                    </div>
                </form>
                <div id="detail_gaji"> </div>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <!-- <div class="btn btn-default" onclick="cetak()">Cetak</div> -->
                    <div class="btn btn-primary" onclick="generateSemuaGaji()">Hitung</div>
                    <div class="btn btn-primary" onclick="simpanGaji()">Simpan Gaji</div>
                </div>
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_daftar_gaji') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                },
                {
                    "data": "nama_karyawan"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "gaji_pokok"
                },
                {
                    "data": "tanggal_gabung"
                },
                {
                    "data": "kategori"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary gaji_btn_view">View</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' +
                            row
                            .id +
                            '\')" class="btn btn-danger gaji_btn_delete">Delete</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();

        $('#karyawan_id').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        $('#golongan_id').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        getKaryawanList();
        getGolonganList();

        $(".datepicker").datetimepicker({
            format: "YYYY-MM-DD",
            useCurrent: false
        })

    }, 1000)
});


function openform() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "karyawan_id": parseInt($('#karyawan_id').val()),
        "gaji_pokok": parseInt($('#gaji_pokok').val()),
        "golongan_id": parseInt($('#golongan_id').val())
    };

    $.ajax({
        method: "POST",
        url: "insertGaji",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "GajiFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#karyawan_id').val(data.karyawan_id);
        $('#golongan_id').val(data.id_golongan);
        $('#gaji_pokok').val(data.gaji_pokok);
        $('#karyawan_id').selectpicker('refresh');
        $('#golongan_id').selectpicker('refresh');
    })
}

function update(id) {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "karyawan_id": parseInt($('#karyawan_id').val()),
        "gaji_pokok": parseInt($('#gaji_pokok').val()),
        "golongan_id": parseInt($('#golongan_id').val())
    };

    $.ajax({
        method: "POST",
        url: "updateGaji/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function validation() {
    if ($('#karyawan_id').val() == null) {
        toastr.error("karyawan tidak boleh kosong");
        return false;
    }

    if ($('#golongan_id').val() == null) {
        toastr.error("golongan tidak boleh kosong");
        return false;
    }

    if ($('#gaji_pokok').val() == null || $('#gaji_pokok').val() < 1) {
        toastr.error("Gaji Pokok tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete gaji',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteGaji/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Gaji berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });
}

function getKaryawanList() {
    $.ajax({
        method: 'GET',
        url: 'KaryawanController/getKaryawanList',
        contentType: 'application/json'
    }).done(function(response) {
        console.log("===========================")
        let data = JSON.parse(response);
        console.log(data)
        var options = [],
            _options;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].id;
            let text = data[i].nama_karyawan;

            let option = '<option value="' + value + '">' + text + '</option>';
            options.push(option);

        }
        _options = options.join('');
        $('#karyawan_id')[0].innerHTML = _options;
        $('#karyawan_id').selectpicker('refresh');
    })
}

function getGolonganList() {
    $.ajax({
        method: 'GET',
        url: 'GolonganController/getGolonganList',
        contentType: 'application/json'
    }).done(function(response) {
        console.log("===========================")
        let data = JSON.parse(response);
        console.log(data)
        var options = [],
            _options;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].id;
            let text = data[i].kategori+" ("+data[i].ranges+")";

            let option = '<option value="' + value + '">' + text + '</option>';
            options.push(option);

        }
        _options = options.join('');
        $('#golongan_id')[0].innerHTML = _options;
        $('#golongan_id').selectpicker('refresh');
    })
}

function openFormGenerate() {
    $('#detail_gaji').empty();
    $('#modal-generate').modal({
        'backdrop': 'static'
    });
}

function generateGaji(karyawan_id) {
    if ($('#start').val() == null || $('#start').val().trim() == "") {
        toastr.error("tanggal mulai belum dipilih");
        return;
    }

    if ($('#end').val() == null || $('#end').val().trim() == "") {
        toastr.error("tanggal akhir belum dipilih");
        return;
    }
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "start": $('#start').val(),
        "end": $('#end').val()
    }
    $.ajax({
        method: 'POST',
        url: 'GajiController/generateGaji',
        contentType: 'application/json',
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log('=======================');
        console.log(data);
    })
}

function generateSemuaGaji() {
    if ($('#start').val() == null || $('#start').val().trim() == "") {
        toastr.error("tanggal mulai belum dipilih");
        return;
    }

    if ($('#end').val() == null || $('#end').val().trim() == "") {
        toastr.error("tanggal akhir belum dipilih");
        return;
    }
    let value = {
        // "karyawan_id": parseInt(karyawan_id),
        "start": $('#start').val(),
        "end": $('#end').val()
    }
    $.ajax({
        method: 'POST',
        url: 'GajiController/generateGaji',
        contentType: 'application/json',
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        let bendahara = "<?= $_SESSION['nama_karyawan'] ?>";

        for (let i = 0; i < data.length; i++) {
            let detail = '<div class="invoice p-3 mb-3" id="print_area_' + i + '">\n\
            <div class="row">\n\
                <div class="col-6">\n\
                    <h4>\n\
                    <i class="nav-icon fas fa-tachometer-alt"></i> Laporan Slip Gaji\n\
                    </h4>\n\
                </div>\n\
                <div class="col-6">\n\
                    <h2>\n\
                        <i class="fas fa-globe"></i> BPRS Rifatul Ummah </h2>\n\
                </div>\n\
            </div><br>\n\
            <div class="row invoice-info">\n\
                <div class="col-sm-6 invoice-col">\n\
                    <address>\n\
                        <h5>\n\
                            <small class="float-left">Nama Karyawan : ' + data[i].nama_karyawan + '</small><br>\n\
                            <small class="float-left">Jabatan : ' + data[i].jabatan + '</small><br>\n\
                            <small class="float-left">Periode : ' + data[i].tanggal + '</small><br>\n\
                        </h5>\n\
                    </address>\n\
                </div>\n\
            </div>\n\
            <div class="row">\n\
                <div class="col-5">\n\
                    <p class="lead">Detail Gaji</p>\n\
                    <div class="table-responsive">\n\
                        <table class="table">\n\
                            <tbody>\n\
                                <tr>\n\
                                    <th style="width:50%">Gaji Pokok</th>\n\
                                    <td>' + data[i].gaji_pokok + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <th>Total Tunjangan</th>\n\
                                    <td>' + data[i].total_tunjangan + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <th>Lemburan</th>\n\
                                    <td>' + data[i].lemburan + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <th>Gaji Kotor</th>\n\
                                    <td>' + data[i].gaji_kotor + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <th>Total Gaji</th>\n\
                                    <td>' + data[i].total_gaji + '</td>\n\
                                </tr>\n\
                            </tbody>\n\
                        </table>\n\
                    </div>\n\
                </div>\n\
                <div class="col-7">\n\
                    <p class="lead">Potongan</p>\n\
                    <div class="table-responsive">\n\
                        <table class="table">\n\
                            <tbody>\n\
                                <tr>\n\
                                    <th>PPH 10%</th>\n\
                                    <td>' + data[i].pph + '</td>\n\
                                </tr>\n\
                                <tr style="border:none">\n\
                                    <th style="border:none"></th>\n\
                                    <td style="border:none"></td>\n\
                                </tr>\n\
                                <tr style="border:none">\n\
                                    <th style="border:none"></th>\n\
                                    <td style="border:none"></td>\n\
                                </tr>\n\
                                <tr style="border:none">\n\
                                    <th style="border:none"></th>\n\
                                    <td style="border:none"></td>\n\
                                </tr>\n\
                                <tr style="border:none">\n\
                                    <th style="border:none"></th>\n\
                                    <td style="border:none"></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <th>Total Pengeluaran</th>\n\
                                    <td>' + data[i].total_pengeluaran + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td> *catatan : hutang anda Rp. ' + (data[i].sisa_hutang != null ? data[i].sisa_hutang : 0) + '</td>\n\
                                </tr>\n\
                            </tbody>\n\
                        </table>\n\
                    </div>\n\
                </div>\n\
                <div class="col-6">\n\
                </div>\n\
                <div class="col-6">\n\
                <div class="table-responsive">\n\
                        <table class="table">\n\
                            <tbody>\n\
                                <tr>\n\
                                    <th style="width:50%">Diserahkan Oleh</th>\n\
                                    <th>Diterima Oleh</th>\n\
                                </tr>\n\
                                <tr>\n\
                                </tr>\n\
                                <tr style="height: 100px;">\n\
                                    <td style="vertical-align:bottom;">' + bendahara + '</td>\n\
                                    <td style="vertical-align:bottom;">' + data[i].nama_karyawan + '</td>\n\
                                </tr>\n\
                            </tbody>\n\
                        </table>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            <div class="row no-print">\n\
                <div class="col-12">\n\
                  <div onclick="cetak(' + i + ')" class="btn btn-default"><i class="fas fa-print"></i> Print</a>\n\
                </div>\n\
              </div>\n\
            <hr>\n\
        </div>';
            $('#detail_gaji').append(detail);
        }
    })
}

function simpanGaji() {
    if ($('#start').val() == null || $('#start').val().trim() == "") {
        toastr.error("tanggal mulai belum dipilih");
        return;
    }

    if ($('#end').val() == null || $('#end').val().trim() == "") {
        toastr.error("tanggal akhir belum dipilih");
        return;
    }
    let value = {
        // "karyawan_id": parseInt(karyawan_id),
        "start": $('#start').val(),
        "end": $('#end').val()
    }
    $.ajax({
        method: 'POST',
        url: 'GajiController/simpanGaji',
        contentType: 'application/json',
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        location.reload();
    })
}

function cetak(i) {
    $('#print_area_' + i).printElement({});
}
</script>