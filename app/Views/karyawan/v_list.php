<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div type="button" class="btn btn-primary karyawan_btn_add"
                                            onclick="openform()">Tambah
                                            Data</div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Username</th>
                                                <th>Nama Karyawan</th>
                                                <th>Tgl Lahir</th>
                                                <th>Jenis Kelamin</th>
                                                <th>NIK</th>
                                                <th>NPWP</th>
                                                <th>Jabatan</th>
                                                <th>Tgl Masuk</th>
                                                <th>Tgl Keluar</th>
                                                <th>Alasan Keluar</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form User</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body custom_scroll">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" id="password" class="form-control" placeholder="Enter Password">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Karyawan</label>
                            <input type="text" id="nama_karyawan" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tanggal Lahir</label>
                            <input type="date" id="tanggal_lahir" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tempat Lahir</label>
                            <input type="text" id="tempat_lahir" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">NIK</label>
                            <input type="text" id="nik" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">NPWP</label>
                            <input type="text" id="nama_karyawan" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" id="status">
                                <option value="Menikah">Menikah</option>
                                <option value="Belum Menikah">Belum Menikah</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Jenis Kelamin</label>
                            <select class="form-control" id="jk">
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jabatan</label>
                            <select id="jabatan" class="form-control">
                                <option value="Karyawan">Karyawan</option>
                                <option value="HRD">HRD</option>
                                <option value="Keuangan">Keuangan</option>
                                <option value="Menejer">Menejer</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Alamat</label>
                            <textarea type="text" id="alamat" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tanggal Gabung</label>
                            <input type="date" id="tanggal_gabung" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tanggal Keluar</label>
                            <input type="date" id="tgl_keluar" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Alasan Keluar</label>
                            <select class="form-control" id="alasan_keluar">
                                <option value="Dipecat">Dipecat</option>
                                <option value="Resign">Resign</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div type="button" class="btn btn-warning" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary karyawan_btn_save" id="save" onclick="save()">Save</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_daftar_karyawan') ?>", // URL file untuk proses select datanya
                "type": "POST",
                // "success": function() {
                //     // alert("Done!");
                // }
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "username"
                },
                {
                    "data": "nama_karyawan"
                },
                {
                    "data": "tanggal_lahir"
                },
                {
                    "data": "jk"
                },
                {
                    "data": "nik"
                },
                {
                    "data": "npwp"
                },
                {
                    "data": "jabatan"
                },
                {
                    "data": "tanggal_gabung"
                },
                {
                    "data": "tgl_keluar"
                },
                {
                    "data": "alasan_keluar"
                },
                {
                    "data": "status"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary karyawan_btn_view">View or Edit</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' + row
                            .id +
                            '\')" class="btn btn-danger karyawan_btn_delete">Delete</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});


function openform() {
    // $('#modal-xl').modal('show');
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "username": $('#username').val(),
        "password": $('#password').val(),
        "nama_karyawan": $('#nama_karyawan').val(),
        "tanggal_lahir": $('#tanggal_lahir').val(),
        "tempat_lahir": $('#tempat_lahir').val(),
        "status": $('#status').val(),
        "jk": $('#jk').val(),
        "jabatan": $('#jabatan').val(),
        "alamat": $('#alamat').val(),
        "tanggal_gabung": $('#tanggal_gabung').val(),
        "nik": $('#nik').val(),
        "npwp": $('#npwp').val(),
        "tgl_keluar": $('#tgl_keluar').val(),
        "alasan_keluar": $('#alasan_keluar').val()
    };

    $.ajax({
        method: "POST",
        url: "insertKaryawan",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "karyawanFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        // $('#modal-xl').modal('show');
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#username').val(data.username);
        $('#password').val(data.password);
        $('#nama_karyawan').val(data.nama_karyawan);
        $('#tanggal_lahir').val(data.tanggal_lahir);
        $('#tempat_lahir').val(data.tempat_lahir);
        $('#status').val(data.status);
        $('#jk').val(data.jk);
        $('#jabatan').val(data.jabatan);
        $('#alamat').val(data.alamat);
        $('#tanggal_gabung').val(data.tanggal_gabung);
        $('#nik').val(data.nik);
        $('#npwp').val(data.npwp);
        $('#tgl_keluar').val(data.tgl_keluar);
        $('#alasan_keluar').val(data.alasan_keluar);
    })
}

function update(id) {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "username": $('#username').val(),
        "password": $('#password').val(),
        "nama_karyawan": $('#nama_karyawan').val(),
        "tanggal_lahir": $('#tanggal_lahir').val(),
        "tempat_lahir": $('#tempat_lahir').val(),
        "status": $('#status').val(),
        "jk": $('#jk').val(),
        "jabatan": $('#jabatan').val(),
        "alamat": $('#alamat').val(),
        "tanggal_gabung": $('#tanggal_gabung').val(),
        "nik": $('#nik').val(),
        "npwp": $('#npwp').val(),
        "tgl_keluar": $('#tgl_keluar').val(),
        "alasan_keluar": $('#alasan_keluar').val()
    };

    $.ajax({
        method: "POST",
        url: "updateKaryawan/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function validation() {
    if ($('#username').val() == null || $('#username').val().trim() == "") {
        toastr.error("username tidak boleh kosong");
        return false;
    }

    if ($('#password').val() == null || $('#password').val().trim() == "") {
        toastr.error("password tidak boleh kosong");
        return false;
    }

    if ($('#nama_karyawan').val() == null || $('#nama_karyawan').val().trim() == "") {
        toastr.error("nama tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete user',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteKaryawan/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("User berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });

}
</script>