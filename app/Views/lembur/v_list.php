<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-chart-pie mr-1"></i>
                        <?= $title ?>
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-1.9">
                                        <div type="button" class="btn btn-warning lembur_btn_add" onclick="openform2()">
                                            Lembur
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div type="button" class="btn btn-primary" onclick="openform()">Ajukan Lembur
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                                        <thead>
                                            <!-- <tr>
                                                <th>No</th>
                                                <th>Nama Karyawan</th>
                                                <th>Jabatan</th>
                                                <th>Tanggal</th>
                                                <th>Jam Datang</th>
                                                <th>Jam Pulang</th>
                                                <th>Status</th>
                                                <th></th>
                                                <th></th>
                                            </tr> -->
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="header-title">FORM LEMBUR
                </h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jam Awal</label>
                            <div class="input-group clockpicker">
                                <input type="text" class="form-control" id="jam_awal_1" value="09:30">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jam Akhir</label>
                            <div class="input-group clockpicker">
                                <input type="text" class="form-control" id="jam_akhir_1" value="09:30">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <select class="form-control" id="status_1">
                                <option value="Menunggu">Menunggu</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Keterangan</label>
                            <textarea class="form-control" id="notes_1"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="save" class="btn btn-primary" onclick="save()">Lembur</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-xl-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form Lembur</h4>
            </div>
            <div class="modal-body">
                <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Karyawan</label>
                            <select class="selectpicker form-control" data-container="body" data-live-search="true"
                                title="Select a number" data-hide-disabled="true" id="karyawan_id">
                                <option value="">Iwan</option>
                                <option value="">Rudy</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jam Awal</label>
                            <div class="input-group clockpicker">
                                <input type="text" class="form-control" id="jam_awal" value="09:30">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jam Akhir</label>
                            <div class="input-group clockpicker">
                                <input type="text" class="form-control" id="jam_akhir" value="09:30">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <select class="form-control" id="status">
                                <option value="Menunggu">Menunggu</option>
                                <option value="Disetujui">Disetujui</option>
                                <option value="Ditolak">Ditolak</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Keterangan</label>
                            <textarea class="form-control" id="notes"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <div class="btn btn-default" data-dismiss="modal">Close</div>
                <div>
                    <div id="reset" class="btn btn-warning" data-dismiss="modal">Reset</div>
                    <div id="save2" class="btn btn-primary" onclick="openform2()">Save</div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script src="<?php base_url() ?>js/access.js"></script>
<script>
$(document).ready(function() {
    let $jabatan = "<?= $_SESSION['jabatan']; ?>";
    accessManagement($jabatan)
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('ls_daftar_lembur') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    title: "No",
                    "render": function(data, type, row) {
                        return "";
                    }
                },
                {
                    title: "Nama",
                    "data": "nama_karyawan"
                },
                {
                    title: "Jabatan",
                    "data": "jabatan"
                },
                {
                    title: "Tanggal",
                    "data": "tanggal"
                },
                {
                    title: "Jam Awal",
                    "data": "jam_awal"
                },
                {
                    title: "Jam Akhir",
                    "data": "jam_akhir",
                    render(data) {
                        return (data == null ? "-" : data)
                    }
                },
                {
                    title: "Total",
                    "data": "total_jam",
                    render(data) {
                        return data + " jam";
                    }
                },
                {
                    title: "Status",
                    "data": "status"
                },
                {
                    title: "",
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary lembur_btn_view">View</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
                {
                    title: "",
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="deleteData(\'' +
                            row
                            .id +
                            '\')" class="btn btn-danger lembur_btn_delete">Delete</button>';

                        setTimeout(function() {
                            accessManagement($jabatan)
                        }, 10)
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();

        $('.clockpicker').clockpicker({
            placement: 'bottom',
            align: 'left',
            donetext: 'Done'
        });

        $('#karyawan_id').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });

        getKaryawanList()
    }, 1000)
});


function openform() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()');
    // let session = "<?= $_SESSION['nama_karyawan']; ?>";
    // $('#header-title').text("Hai " + session + ", ini waktunya absen");
    // $('#save').text('ABSEN');
}

function openform2() {
    $('#modal-xl-2').modal({
        'backdrop': 'static'
    });
    $('#save2').attr('onclick', 'save2()');
}

function reset() {
    // $('#form')[0].reset();
}

function save() {
    // if (!validation()) {
    //     return;
    // }

    $('#save').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());
    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;

    let karyawan_id = "<?= $_SESSION['id']; ?>";
    if (karyawan_id == null || karyawan_id == "") {
        toastr.error("Invalid Session");
    }
    let jam_awal = $('#jam_awal_1').val();
    let jam_akhir = $('#jam_akhir_1').val();
    let durasi = hitungJam(jam_awal, jam_akhir);
    let status = $('#status_1').val();
    let notes = $('#notes_1').val();
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_awal": jam_awal,
        "jam_akhir": jam_akhir,
        "total_jam": durasi,
        "tanggal": year + "-" + month + "-" + day,
        "status": status,
        "notes": notes
    };

    $.ajax({
        method: "POST",
        url: "insertLembur",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })

    $.ajax({
        method: "POST",
        url: "insertAbsensi",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function save2() {
    if (!validation()) {
        return;
    }

    $('#save2').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let karyawan_id = $('#karyawan_id').val();
    let jam_awal = $('#jam_awal').val();
    let jam_akhir = $('#jam_akhir').val();
    let status = $('#status').val();
    let notes = $('#notes').val();
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_awal": jam_awal,
        "jam_akhir": jam_akhir,
        "total_jam": 2,
        "tanggal": year + "-" + month + "-" + day,
        "status": status,
        "notes": notes
    };

    $.ajax({
        method: "POST",
        url: "insertLembur",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save2').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "lemburFindById/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#karyawan_id').val(data.karyawan_id);
        $('#karyawan_id').attr('disabled', true);
        $('#jam_awal').val(data.jam_awal);
        $('#jam_awal').attr('disabled', true);
        $('#jam_akhir').val(data.jam_akhir);
        $('#jam_akhir').attr('disabled', true);
        $('#status').val(data.status);
        $('#notes').val(data.notes);
        $('#modal-xl-2').modal({
            'backdrop': 'static'
        });
        $('#save2').attr('onclick', 'update2(' + data.id + ')')
        $('#karyawan_id').selectpicker('refresh');
    })
}

function leaveOffice() {
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'update()')
    let session = "<?= $_SESSION['nama_karyawan']; ?>";
    $('#header-title').text('Hai ' + session + ', apakah kamu yakin akan meninggalkan waktu kerjamu?');
    $('#save').text('LEAVE');
}

function update() {

    $('#save').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let status = "";
    if (date.getHours() < 17 && date.getMinutes() < 15) {
        status = "Leave"
    } else {
        status = "Hadir"
    }
    let karyawan_id = "<?= $_SESSION['id']; ?>";
    if (karyawan_id == null || karyawan_id == "") {
        toastr.error("Invalid Session");
    }
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_datang": "",
        "jam_pulang": datetime,
        "tanggal": year + "-" + month + "-" + day,
        "status": status
    };

    $.ajax({
        method: "POST",
        url: "updateAbsensi",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function update2(id) {
    if (!validation2()) {
        return;
    }

    $('#save2').attr('disabled', true);
    let date = new Date();
    let year = date.getFullYear();
    let monthTemp = date.getMonth() + 1;
    let month = (monthTemp < 10 ? '0' + monthTemp : monthTemp);
    let day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
    let hour = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
    let minute = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    let secound = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let milisecound = (date.getMilliseconds() < 10 ? '0' + date.getMilliseconds() : date.getMilliseconds());

    let datetime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound;
    let karyawan_id = $('#karyawan_id').val();
    let jam_awal = $('#jam_awal').val();
    let jam_akhir = $('#jam_akhir').val();
    let status = $('#status').val();
    let notes = $('#notes').val();
    let value = {
        "karyawan_id": parseInt(karyawan_id),
        "jam_awal": jam_awal,
        "jam_akhir": jam_akhir,
        "total_jam": null,
        "status": status,
        "notes": notes
    };

    $.ajax({
        method: "POST",
        url: "updateLembur/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save2').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function validation() {

    if ($('#jam_awal_1').val() == null || $('#jam_awal_1').val().trim() == "") {
        toastr.error("jam awal tidak boleh kosong");
        return false;
    }

    if ($('#jam_akhir_1').val() == null || $('#jam_akhir_1').val().trim() == "") {
        toastr.error("jam akhir tidak boleh kosong");
        return false;
    }

    if ($('#status_1').val() == null || $('#status_1').val().trim() == "") {
        toastr.error("status tidak boleh kosong");
        return false;
    }

    if ($('#notes_1').val() == null || $('#notes_1').val().trim() == "") {
        toastr.error("Alasan tidak boleh kosong");
        return false;
    }

    return true;
}

function validation2() {

    if ($('#jam_awal').val() == null || $('#jam_awal').val().trim() == "") {
        toastr.error("jam awal tidak boleh kosong");
        return false;
    }

    if ($('#jam_akhir').val() == null || $('#jam_akhir').val().trim() == "") {
        toastr.error("jam akhir tidak boleh kosong");
        return false;
    }

    if ($('#status').val() == null || $('#status').val().trim() == "") {
        toastr.error("status tidak boleh kosong");
        return false;
    }

    if ($('#notes').val() == null || $('#notes').val().trim() == "") {
        toastr.error("Alasan tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete Lembur',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteLembur/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Lembur berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });
}

function getKaryawanList() {
    $.ajax({
        method: 'GET',
        url: 'KaryawanController/getKaryawanList',
        contentType: 'application/json'
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data)
        var options = [],
            _options;
        for (let i = 0; i < data.length; i++) {
            let value = data[i].id;
            let text = data[i].nama_karyawan;

            let option = '<option value="' + value + '">' + text + '</option>';
            options.push(option);

        }
        _options = options.join('');
        $('#karyawan_id')[0].innerHTML = _options;
        $('#karyawan_id').selectpicker('refresh');
    })
}

function hitungJam(jam_awal, jam_akhir) {
    let tempA = jam_awal.split(":");
    let hourA = parseInt(tempA[0]);
    let minutesA = parseInt(tempA[1]);


    let tempB = jam_akhir.split(":");
    let hourB = parseInt(tempB[0]);
    let minutesB = parseInt(tempB[1]);
    let totalJam;
    if (hourB > hourA) {
        totalJam = hourB - hourA;
    } else if (hourB < hourA) {
        let h1 = 24 - hourA;
        totalJam = h1 + hourB;
    }
    let minutes = (minutesB - minutesA) / 60;
    console.log("minutes " + minutes)
    let total = 0;
    if (minutes >= 0.5) {
        minutes = 1
        total = totalJam + minutes;
    } else if (minutes >= 0.1) {
        minutes = 0
        total = totalJam + minutes;
    } else if (minutes >= -0.1) {
        minutes = 0;
        total = totalJam + minutes;
    } else if (minutes < -0.5) {
        minutes = 1;
        total = totalJam - minutes;
    }
    console.log("total Jam " + totalJam)
    console.log("total " + total)

    return total;
}
</script>