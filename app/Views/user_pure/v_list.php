<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <!-- <a type="button" class="btn btn-primary">Tambah Data</a> -->
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
                            Tambah Karyawan
                        </button><br>
                    </div>
                    <div class="col-md-4">
                        <form action="" method="get">
                            <div class="input-group">
                                <input id="btn-input" type="text" class="form-control input-sm" name="keyword"
                                    placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm" id="btn-chat" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Display Name</th>
                                <th>Jabatan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1 + (5 * ($currentPage - 1));
                            foreach ($users as $key => $value) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['username']; ?></td>
                                <td><?= $value['nama_karyawan']; ?></td>
                                <td><?= $value['jabatan']; ?></td>
                                <td><a class="btn btn-info">View</a>&nbsp;&nbsp;&nbsp;
                                    <a href="<?= base_url('edituser/' . $value['id']); ?>"
                                        class="btn btn-primary">Edit</a>&nbsp;&nbsp;&nbsp;
                                    <a href="<?= base_url('deleteuser/' . $value['id']); ?>" class="btn btn-danger"
                                        onclick="return confirm('apakah anda yakin ingin menghapus data  <?= $value['nama_karyawan']; ?> ?')">Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?= $pager->links(); ?>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Karyawan</h4>
                </div>
                <div class="modal-body">
                    <form role="form" action="<?= base_url('adduser'); ?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" id="password" class="form-control" placeholder="Enter Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Karyawan</label>
                                <input type="text" id="nama_karyawan" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Lahir</label>
                                <input type="date" id="tanggal_lahir" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tempat Lahir</label>
                                <input type="text" id="tempat_lahir" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status</label>
                                <select class="form-control" id="status">
                                    <option value="Menikah">Menikah</option>
                                    <option value="Belum Menikah">Belum Menikah</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Jenis Kelamin</label>
                                <select class="form-control" id="jk">
                                    <option value="laki-laki">Laki-laki</option>
                                    <option value="perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Jabatan</label>
                                <select class="form-control" id="jabatan">
                                    <option value="Staff">Staff</option>
                                    <option value="Manager">Manager</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat</label>
                                <textarea type="text" id="alamat" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Gabung</label>
                                <input type="date" id="tanggal_gabung" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-primary" onclick="save()" id="save">Save</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script>
function save() {
    $('#save').attr('disabled', true);
    let value = {
        "username": $('#username').val(),
        "password": $('#password').val(),
        "nama_karyawan": $('#nama_karyawan').val(),
        "tanggal_lahir": $('#tanggal_lahir').val(),
        "tempat_lahir": $('#tempat_lahir').val(),
        "status": $('#status').val(),
        "jk": $('#jk').val(),
        "jabatan": $('#jabatan').val(),
        "alamat": $('#alamat').val(),
        "tanggal_gabung": $('#tanggal_gabung').val()
    }

    $.ajax({
        method: "POST",
        url: "adduser",
        Accept: 'application/json',
        contentType: 'application/json',
        data: JSON.stringify(value)
    }).done(function(response) {
        alert("success")
        $('#save').attr('disabled', false);
        location.reload();
    });
}
</script>