<?php

namespace App\Controllers;

use App\Models\LemburModel;

class LemburController extends BaseController
{
    protected $lembur;
    public function __construct()
    {
        $this->lembur = new LemburModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Lembur',
            'isi' => 'lembur/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->lembur->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->lembur->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->lembur->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function findById($id)
    {
        $result = $this->lembur->findLemburById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jam_awal = str_replace('"', '', json_encode($temp->jam_awal));
            $jam_akhir = str_replace('"', '', json_encode($temp->jam_akhir));
            $total_jam = $temp->total_jam;
            $tanggal = str_replace('"', '', json_encode($temp->tanggal));
            $status = str_replace('"', '', json_encode($temp->status));
            $notes = str_replace('"', '', json_encode($temp->notes));

            $data = [
                "karyawan_id" => $karyawan_id,
                "jam_awal" => $jam_awal,
                "jam_akhir" => $jam_akhir,
                "total_jam" => $total_jam,
                "tanggal" => $tanggal,
                "status" => $status,
                "notes" => $notes
            ];

            $id = $this->lembur->findLemburByTanggalAndKaryawan($tanggal, $karyawan_id);
            if ($id != null) {
                $response = [
                    "code" => 00,
                    "message" => "Gagal, Anda Sudah Mengajukan lembur hari ini"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->lembur->insertLembur($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function update()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jam_datang = str_replace('"', '', json_encode($temp->jam_datang));
            $jam_pulang = str_replace('"', '', json_encode($temp->jam_pulang));
            $tanggal = str_replace('"', '', json_encode($temp->tanggal));
            $status = str_replace('"', '', json_encode($temp->status));

            $data = [
                "karyawan_id" => $karyawan_id,
                "jam_pulang" => $jam_pulang,
                "tanggal" => $tanggal,
                "status" => $status
            ];

            $id = $this->lembur->findLemburByTanggalAndKaryawan($tanggal, $karyawan_id);
            if ($id == null) {
                $response = [
                    "code" => "00",
                    "message" => "Gagal, anda belum absen hari ini"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $leave = $this->lembur->leaveValidation($tanggal, $id);
                if ($leave == "") {
                    $response = [
                        "code" => "00",
                        "message" => "Gagal, anda sudah leave hari ini"
                    ];

                    header('Content-Type: application/json');
                    echo json_encode($response);
                } else {
                    $result = $this->lembur->updateLembur($data, $id);
                    if ($result) {
                        $response = [
                            "code" => 200,
                            "message" => "Success"
                        ];
                    } else {
                        $response = [
                            "code" => "00",
                            "message" => "Failed"
                        ];
                    }
                    header('Content-Type: application/json');
                    echo json_encode($response);
                }
            }
        }
    }

    public function update2($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jam_awal = str_replace('"', '', json_encode($temp->jam_awal));
            $jam_akhir = str_replace('"', '', json_encode($temp->jam_akhir));
            $total_jam = $temp->total_jam;
            // $tanggal = str_replace('"', '', json_encode($temp->tanggal));
            $status = str_replace('"', '', json_encode($temp->status));
            $notes = str_replace('"', '', json_encode($temp->notes));

            $data = [
                "karyawan_id" => $karyawan_id,
                // "jam_awal" => $jam_awal,
                // "jam_akhir" => $jam_akhir,
                // "total_jam" => $total_jam,
                // "tanggal" => $tanggal,
                "status" => $status,
                "notes" => $notes
            ];

            if ($id == null) {
                $response = [
                    "code" => "00",
                    "message" => "Gagal, anda belum mengajukan lembur hari ini"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->lembur->updateLembur($data, $id);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => "00",
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->lembur->deleteLembur($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Tidak dapat menghapus absensi"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }
}