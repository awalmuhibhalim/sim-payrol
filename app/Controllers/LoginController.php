<?php

namespace App\Controllers;

use App\Models\KaryawanModel;

class LoginController extends BaseController
{
    protected $karyawan;
    public function __construct()
    {
        $this->karyawan = new KaryawanModel();
    }
    public function index()
    {
        echo view('login/login');
    }

    public function login()
    {
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        $result = $this->karyawan->proccessLogin($username, $password);
        if ($result) {
            return redirect()->to(base_url('index'));
        } else {
            return redirect()->to(base_url('login'));
            // echo view('login/login');
        }
        // $data = [
        //     'username' => $username,
        //     'password' => $password,
        //     'isi' => 'kombinasi/v_list'
        // ];
        // echo view('/v_wrapper', $data);
    }

    public function logout()
    {
        session_destroy();
        return redirect()->to(base_url('login'));
    }
}