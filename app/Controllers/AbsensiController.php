<?php

namespace App\Controllers;

use App\Models\AbsensiModel;

class AbsensiController extends BaseController
{
    protected $absensi;
    public function __construct()
    {
        $this->absensi = new AbsensiModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Absensi',
            'isi' => 'absensi/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->absensi->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->absensi->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->absensi->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function presentToday()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->absensi->count_absen(""); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->absensi->absen($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->absensi->count_absen($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function unPresentToday()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->absensi->count_leave(""); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->absensi->leave($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->absensi->count_leave($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function persentasiKehadiran()
    {
        $data = [
            "hadir" => $this->absensi->getPersentaseKehadiran("Hadir"),
            "tidak_hadir" => $this->absensi->getPersentaseKehadiran("Tidak Hadir"),
            "terlambat" => $this->absensi->getPersentaseKehadiran("Terlambat"),
            "leave" => $this->absensi->getPersentaseKehadiran("Leave"),
            "izin" => $this->absensi->getPersentaseKehadiran("Izin"),
            "sakit" => $this->absensi->getPersentaseKehadiran("Sakit")
        ];

        echo json_encode($data);
    }

    public function findById($id)
    {
        $result = $this->absensi->findAbsensiById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jam_datang = str_replace('"', '', json_encode($temp->jam_datang));
            $jam_pulang = str_replace('"', '', json_encode($temp->jam_pulang));
            $tanggal = str_replace('"', '', json_encode($temp->tanggal));
            $status = str_replace('"', '', json_encode($temp->status));

            $data = [
                "karyawan_id" => $karyawan_id,
                "jam_datang" => $jam_datang,
                "jam_pulang" => null,
                "tanggal" => $tanggal,
                "status" => $status
            ];

            $id = $this->absensi->findAbsensiByTanggalAndKaryawan($tanggal, $karyawan_id);
            if ($id != null) {
                $response = [
                    "code" => 00,
                    "message" => "Gagal, Anda Sudah absen hari ini"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->absensi->insertAbsensi($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function update()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jam_datang = str_replace('"', '', json_encode($temp->jam_datang));
            $jam_pulang = str_replace('"', '', json_encode($temp->jam_pulang));
            $tanggal = str_replace('"', '', json_encode($temp->tanggal));
            $status = str_replace('"', '', json_encode($temp->status));

            $data = [
                "karyawan_id" => $karyawan_id,
                "jam_pulang" => $jam_pulang,
                "tanggal" => $tanggal,
                "status" => $status
            ];

            $id = $this->absensi->findAbsensiByTanggalAndKaryawan($tanggal, $karyawan_id);
            if ($id == null) {
                $response = [
                    "code" => "00",
                    "message" => "Gagal, anda belum absen hari ini"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $leave = $this->absensi->leaveValidation($tanggal, $id);
                if ($leave == "") {
                    $response = [
                        "code" => "00",
                        "message" => "Gagal, anda sudah leave hari ini"
                    ];

                    header('Content-Type: application/json');
                    echo json_encode($response);
                } else {
                    $result = $this->absensi->updateAbsensi($data, $id);
                    if ($result) {
                        $response = [
                            "code" => 200,
                            "message" => "Success"
                        ];
                    } else {
                        $response = [
                            "code" => "00",
                            "message" => "Failed"
                        ];
                    }
                    header('Content-Type: application/json');
                    echo json_encode($response);
                }
            }
        }
    }

    public function update2($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jam_datang = str_replace('"', '', json_encode($temp->jam_datang));
            $jam_pulang = str_replace('"', '', json_encode($temp->jam_pulang));
            $tanggal = str_replace('"', '', json_encode($temp->tanggal));
            $status = str_replace('"', '', json_encode($temp->status));

            $data = [
                "karyawan_id" => $karyawan_id,
                "status" => $status
            ];

            if ($id == null) {
                $response = [
                    "code" => "00",
                    "message" => "Gagal, anda belum absen hari ini"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->absensi->updateAbsensi($data, $id);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => "00",
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->absensi->deleteAbsensi($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Tidak dapat menghapus absensi"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }
}