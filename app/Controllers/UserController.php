<?php

namespace App\Controllers;

use App\Models\UserModel;

class UserController extends BaseController
{
    protected $UserModel;

    public function __construct()
    {
        $this->UserModel = new UserModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $userModel = $this->UserModel->keyword($keyword);
        } else {
            $userModel = $this->UserModel->getUserList();
        }

        $data = [
            'title' => 'Daftar User',
            // 'users' => $userModel->paginate(5),
            // 'pager' => $this->UserModel->pager,
            // 'currentPage' => $currentPage,
            // 'users' => $this->UserModel->getUserList(),
            'isi' => 'user/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }


    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->UserModel->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->UserModel->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->UserModel->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }



    public function openForm()
    {
        $data = [
            'title' => 'Form User',
            'isi' => 'user/v_add'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function getAllUser()
    {
        $data = [];
        $data = $this->UserModel->getUserList();
        return json_encode($data); // or echo json_encode($data); 
    }



    public function save()
    {
        if ($this->request->isAJAX()) {
            $temp = $this->request->getJSON();
            $username = str_replace('"', '', json_encode($temp->username));
            $password = str_replace('"', '', json_encode($temp->password));
            $display_name = str_replace('"', '', json_encode($temp->display_name));
            $role = str_replace('"', '', json_encode($temp->role));
            $data = [
                'username' => $username,
                'password' => $password,
                'display_name' => $display_name,
                'role' => $role,
                'created_at' => date('d-M-Y h:t:s'),
                'creator' => $_SESSION['username']
            ];

            $validation = $this->UserModel->findByUsername(null, $username);
            if ($validation) {
                $response = [
                    "code" => 00,
                    "message" => "Username sudah terdaftar "
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->UserModel->insertUser($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }

        // session()->setFlashdata('success', 'data berhasil ditambahkan');
        // return redirect()->to(base_url('userlist'));
    }

    public function openFormEdit($id)
    {
        $data = json_encode($this->UserModel->editUser($id));
        return $data;
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $username = str_replace('"', '', json_encode($temp->username));
            $password = str_replace('"', '', json_encode($temp->password));
            $display_name = str_replace('"', '', json_encode($temp->display_name));
            $role = str_replace('"', '', json_encode($temp->role));
            $data = [
                'username' => $username,
                'password' => $password,
                'display_name' => $display_name,
                'role' => $role,
                'updated_at' => date('d-M-Y h:t:s'),
                'creator' => $_SESSION['username']
            ];
            $validation = $this->UserModel->findByUsername($id, $username);
            if (!$validation) {
                $response = [
                    "code" => "00",
                    "message" => "username already exist"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->UserModel->updateUser($data, $id);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => "00",
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
            // var_dump("update user ");

        }
        // $this->UserModel->updateUser($data, $id);
        // session()->setFlashdata('success', 'data berhasil diupdate');
        // return redirect()->to(base_url('userlist'));
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->UserModel->deleteUser($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Manager Tidak dapat menghapus diri sendiri"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */