<?php

namespace App\Controllers;
use App\Models\GolonganModel;

class GolonganController extends BaseController{
    protected $golongan;

    public function __construct(){
        $this->golongan = new GolonganModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Daftar Golongan',
            'isi' => 'golongan/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->golongan->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->golongan->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->golongan->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function findById($id)
    {
        $result = $this->golongan->findGolonganById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function getGolonganList()
    {
        $result = $this->golongan->findAll();
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $kategori = str_replace('"', '', json_encode($temp->kategori));
            $ranges = str_replace('"', '', json_encode($temp->ranges));

            $data = [
                "kategori" => $kategori,
                "ranges" => $ranges
            ];

            
            $result = $this->golongan->insertGolongan($data);
            if ($result) {
                $response = [
                    "code" => 200,
                    "message" => "Success"
                ];
            } else {
                $response = [
                    "code" => 00,
                    "message" => "Failed"
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $kategori = str_replace('"', '', json_encode($temp->kategori));
            $ranges = str_replace('"', '', json_encode($temp->ranges));

            $data = [
                "kategori" => $kategori,
                "ranges" => $ranges
            ];
            
            $result = $this->golongan->updateGolongan($data, $id);
            if ($result) {
                $response = [
                    "code" => 200,
                    "message" => "Success"
                ];
            } else {
                $response = [
                    "code" => "00",
                    "message" => "Failed"
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->golongan->deleteGolongan($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Manager Tidak dapat menghapus diri sendiri"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }
}
