<?php

namespace App\Controllers;

use App\Models\TunjanganModel;

class TunjanganController extends BaseController
{
    protected $tunjangan;

    public function __construct()
    {
        $this->tunjangan = new TunjanganModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Tunjangan',
            'isi' => 'tunjangan/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->tunjangan->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->tunjangan->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->tunjangan->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function findById($id)
    {
        $result = $this->tunjangan->findTunjanganById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jumlah_tunjangan = $temp->jumlah_tunjangan;
            $tanggungan = str_replace('"', '', json_encode($temp->tanggungan));
            $jumlah_tanggungan = $temp->jumlah_tanggungan;
            $pangan = $temp->pangan;
            $transport = $temp->transport;

            $data = [
                "karyawan_id" => $karyawan_id,
                "jumlah_tunjangan" => $jumlah_tunjangan,
                "tanggungan" => $tanggungan,
                "jumlah_tanggungan" => $jumlah_tanggungan,
                "pangan" => $pangan,
                "transport" => $transport
            ];

            $validation = $this->tunjangan->findByKaryawanId($karyawan_id);
            if ($validation) {
                $response = [
                    "code" => 00,
                    "message" => "Gagal, karyawan sudah terdaftar"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->tunjangan->insertTunjangan($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $jumlah_tunjangan = $temp->jumlah_tunjangan;
            $tanggungan = str_replace('"', '', json_encode($temp->tanggungan));
            $jumlah_tanggungan = $temp->jumlah_tanggungan;
            $pangan = $temp->pangan;
            $transport = $temp->transport;

            $data = [
                "karyawan_id" => $karyawan_id,
                "jumlah_tunjangan" => $jumlah_tunjangan,
                "tanggungan" => $tanggungan,
                "jumlah_tanggungan" => $jumlah_tanggungan,
                "pangan" => $pangan,
                "transport" => $transport
            ];

            // $validation = $this->karyawan->findByUsername($id, $username);
            // if (!$validation) {
            //     $response = [
            //         "code" => "00",
            //         "message" => "username already exist"
            //     ];

            //     header('Content-Type: application/json');
            //     echo json_encode($response);
            // } else {
            $result = $this->tunjangan->updateTunjangan($data, $id);
            if ($result) {
                $response = [
                    "code" => 200,
                    "message" => "Success"
                ];
            } else {
                $response = [
                    "code" => "00",
                    "message" => "Failed"
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($response);
            // }
        }
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->tunjangan->deleteTunjangan($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Tidak dapat menghapus tunjangan"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }
}