<?php

namespace App\Controllers;

use App\Models\HutangModel;

class HutangController extends BaseController
{
    protected $hutang;

    public function __construct()
    {
        $this->hutang = new HutangModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Daftar Hutang',
            'isi' => 'hutang/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->hutang->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->hutang->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->hutang->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function findById($id)
    {
        $result = $this->hutang->findHutangById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $tanggal_pinjaman = $temp->tanggal_pinjaman;
            $total_pinjaman = $temp->total_pinjaman;
            $jangka_waktu = $temp->jangka_waktu;
            $cicilan_perbulan = $temp->cicilan_perbulan;

            $data = [
                "karyawan_id" => $karyawan_id,
                "tanggal_pinjaman" => $tanggal_pinjaman,
                "total_pinjaman" => $total_pinjaman,
                "jangka_waktu" => $jangka_waktu,
                "cicilan_perbulan" => $cicilan_perbulan
            ];

            $validation = $this->hutang->findByKaryawanId($karyawan_id);
            if ($validation) {
                $response = [
                    "code" => 00,
                    "message" => "Gagal, karyawan sudah terdaftar"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->hutang->insertHutang($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $tanggal_pinjaman = $temp->tanggal_pinjaman;
            $total_pinjaman = $temp->total_pinjaman;
            $jangka_waktu = $temp->jangka_waktu;
            $cicilan_perbulan = $temp->cicilan_perbulan;
            $sisa_cicilan = $temp->sisa_cicilan;

            $data = [
                "karyawan_id" => $karyawan_id,
                "tanggal_pinjaman" => $tanggal_pinjaman,
                "total_pinjaman" => $total_pinjaman,
                "jangka_waktu" => $jangka_waktu,
                "cicilan_perbulan" => $cicilan_perbulan,
                "sisa_cicilan" => $sisa_cicilan
            ];

            // $validation = $this->karyawan->findByUsername($id, $username);
            // if (!$validation) {
            //     $response = [
            //         "code" => "00",
            //         "message" => "username already exist"
            //     ];

            //     header('Content-Type: application/json');
            //     echo json_encode($response);
            // } else {
            $result = $this->hutang->updateHutang($data, $id);
            if ($result) {
                $response = [
                    "code" => 200,
                    "message" => "Success"
                ];
            } else {
                $response = [
                    "code" => "00",
                    "message" => "Failed"
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($response);
            // }
        }
    }
    
    public function delete($id)
    {
        $response = [];

        $result = $this->hutang->deleteHutang($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Tidak dapat menghapus Hutang"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }

    public function getAllHutang()
    {
        $result = $this->hutang->getAll();
        header('Content-Type: application/json');
        echo json_encode($result);
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */