<?php

namespace App\Controllers;

use App\Models\UserModel;

class UserController extends BaseController
{
    protected $UserModel;

    public function __construct()
    {
        $this->UserModel = new UserModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $userModel = $this->UserModel->keyword($keyword);
        } else {
            $userModel = $this->UserModel->getUserList();
        }

        $data = [
            'title' => 'Daftar Karyawan',
            'users' => $userModel->paginate(5),
            'pager' => $this->UserModel->pager,
            'currentPage' => $currentPage,
            // 'users' => $this->UserModel->getUserList(),
            'isi' => 'user/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function openForm()
    {
        $data = [
            'title' => 'Form User',
            'isi' => 'user/v_add'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function getAllUser()
    {
        $data = [];
        $data = $this->UserModel->getUserList();
        return json_encode($data); // or echo json_encode($data); 
    }



    public function save()
    {
        if ($this->request->isAJAX()) {
            $temp = $this->request->getJSON();
            // $temp = json_decode($temps);
            var_dump($temp);
            $username = json_encode($temp->username);
            $password = json_encode($temp->password);
            $nama_karyawan = json_encode($temp->nama_karyawan);
            $tanggal_lahir = json_encode($temp->tanggal_lahir);
            $tempat_lahir = json_encode($temp->tempat_lahir);
            $status = json_encode($temp->status);
            $jk = json_encode($temp->jk);
            $jabatan = json_encode($temp->jabatan);
            $alamat = json_encode($temp->alamat);
            $tanggal_gabung = json_encode($temp->tanggal_gabung);
            $pattern = "//i";
            $data = [
                'username' => str_replace('"', '', $username),
                'password' => str_replace('"', '', $password),
                'nama_karyawan' => str_replace('"', '', $nama_karyawan),
                'tanggal_lahir' => str_replace('"', '', $tanggal_lahir),
                'tempat_lahir' => str_replace('"', '', $tempat_lahir),
                'status' => str_replace('"', '', $status),
                'jk' => str_replace('"', '', $jk),
                'jabatan' => str_replace('"', '', $jabatan),
                'alamat' => str_replace('"', '', $alamat),
                'tanggal_gabung' => str_replace('"', '', $tanggal_gabung)
            ];

            $this->UserModel->insertUser($data);
            // session()->setFlashdata('success', 'data berhasil ditambahkan');
            // return redirect()->to(base_url('userlist'));
        }
    }

    public function openFormEdit($id)
    {
        $data = [
            'title' => 'Form User',
            'product' => $this->UserModel->editUser($id),
            'isi' => 'user/v_edit'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function update($id)
    {
        $data = [
            'username' => $this->request->getPost('username'),
            'password' => $this->request->getPost('password'),
            'display_name' => $this->request->getPost('display_name')
        ];
        $this->UserModel->updateUser($data, $id);
        session()->setFlashdata('success', 'data berhasil diupdate');
        return redirect()->to(base_url('userlist'));
    }

    public function delete($id)
    {
        $this->UserModel->deleteUser($id);
        session()->setFlashdata('success', 'data berhasil didelete');
        return redirect()->to(base_url('userlist'));
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */