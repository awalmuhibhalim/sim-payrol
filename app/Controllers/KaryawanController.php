<?php

namespace App\Controllers;

use App\Models\KaryawanModel;

class KaryawanController extends BaseController
{
    protected $karyawan;

    public function __construct()
    {
        $this->karyawan = new KaryawanModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar karyawan',
            'isi' => 'karyawan/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->karyawan->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->karyawan->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->karyawan->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function findById($id)
    {
        $result = $this->karyawan->findKaryawanById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $username = str_replace('"', '', json_encode($temp->username));
            $password = str_replace('"', '', json_encode($temp->password));
            $nama_karyawan = str_replace('"', '', json_encode($temp->nama_karyawan));
            $tanggal_lahir = str_replace('"', '', json_encode($temp->tanggal_lahir));
            $tempat_lahir = str_replace('"', '', json_encode($temp->tempat_lahir));
            $status = str_replace('"', '', json_encode($temp->status));
            $jk = str_replace('"', '', json_encode($temp->jk));
            $jabatan = str_replace('"', '', json_encode($temp->jabatan));
            $alamat = str_replace('"', '', json_encode($temp->alamat));
            $tanggal_gabung = str_replace('"', '', json_encode($temp->tanggal_gabung));
            $nik = str_replace('"', '', json_encode($temp->nik));
            $npwp = str_replace('"', '', json_encode($temp->npwp));
            $tgl_keluar = str_replace('"', '', json_encode($temp->tgl_keluar));
            $alasan_keluar = str_replace('"', '', json_encode($temp->alasan_keluar));

            $data = [
                "username" => $username,
                "password" => $password,
                "nama_karyawan" => $nama_karyawan,
                "tanggal_lahir" => $tanggal_lahir,
                "tempat_lahir" => $tempat_lahir,
                "status" => $status,
                "jk" => $jk,
                "jabatan" => $jabatan,
                "alamat" => $alamat,
                "tanggal_gabung" => $tanggal_gabung,
                "nik" => $nik,
                "npwp" => $npwp,
                "tgl_keluar" => $tgl_keluar,
                "alasan_keluar" => $alasan_keluar
            ];

            $validation = $this->karyawan->findByUsername(null, $username);
            if ($validation) {
                $response = [
                    "code" => 00,
                    "message" => "Username sudah terdaftar "
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->karyawan->insertKaryawan($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $username = str_replace('"', '', json_encode($temp->username));
            $password = str_replace('"', '', json_encode($temp->password));
            $nama_karyawan = str_replace('"', '', json_encode($temp->nama_karyawan));
            $tanggal_lahir = str_replace('"', '', json_encode($temp->tanggal_lahir));
            $tempat_lahir = str_replace('"', '', json_encode($temp->tempat_lahir));
            $status = str_replace('"', '', json_encode($temp->status));
            $jk = str_replace('"', '', json_encode($temp->jk));
            $jabatan = str_replace('"', '', json_encode($temp->jabatan));
            $alamat = str_replace('"', '', json_encode($temp->alamat));
            $tanggal_gabung = str_replace('"', '', json_encode($temp->tanggal_gabung));
            $nik = str_replace('"', '', json_encode($temp->nik));
            $npwp = str_replace('"', '', json_encode($temp->npwp));
            $tgl_keluar = str_replace('"', '', json_encode($temp->tgl_keluar));
            $alasan_keluar = str_replace('"', '', json_encode($temp->alasan_keluar));

            $data = [
                "username" => $username,
                "password" => $password,
                "nama_karyawan" => $nama_karyawan,
                "tanggal_lahir" => $tanggal_lahir,
                "tempat_lahir" => $tempat_lahir,
                "status" => $status,
                "jk" => $jk,
                "jabatan" => $jabatan,
                "alamat" => $alamat,
                "tanggal_gabung" => $tanggal_gabung,
                "nik" => $nik,
                "npwp" => $npwp,
                "tgl_keluar" => $tgl_keluar,
                "alasan_keluar" => $alasan_keluar
            ];
            $validation = $this->karyawan->findByUsername($id, $username);
            if (!$validation) {
                $response = [
                    "code" => "00",
                    "message" => "username already exist"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->karyawan->updateKaryawan($data, $id);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => "00",
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->karyawan->deleteKaryawan($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Manager Tidak dapat menghapus diri sendiri"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }

    public function getKaryawanList()
    {
        $result = $this->karyawan->findAll();
        header('Content-Type: application/json');
        echo json_encode($result);
    }
}