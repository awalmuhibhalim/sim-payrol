<?php

namespace App\Controllers;

use App\Models\GajiModel;
use App\Models\DetailGajiModel;

class GajiController extends BaseController
{
    protected $gaji;
    protected $detailGaji;

    public function __construct()
    {
        $this->gaji = new GajiModel();
        $this->detailGaji = new DetailGajiModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Daftar Gaji',
            'isi' => 'gaji/v_list'
        ];

        echo view('new_layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->gaji->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->gaji->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->gaji->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback);
    }

    public function findById($id)
    {
        $result = $this->gaji->findGajiById($id);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    public function insert()
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $gaji_pokok = $temp->gaji_pokok;
            $golongan_id = $temp->golongan_id;

            $data = [
                "karyawan_id" => $karyawan_id,
                "gaji_pokok" => $gaji_pokok,
                "id_golongan" => $golongan_id
            ];

            $validation = $this->gaji->findByKaryawanId($karyawan_id);
            if ($validation) {
                $response = [
                    "code" => 00,
                    "message" => "Gagal, karyawan sudah terdaftar"
                ];

                header('Content-Type: application/json');
                echo json_encode($response);
            } else {
                $result = $this->gaji->insertGaji($data);
                if ($result) {
                    $response = [
                        "code" => 200,
                        "message" => "Success"
                    ];
                } else {
                    $response = [
                        "code" => 00,
                        "message" => "Failed"
                    ];
                }
                header('Content-Type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $response = [];
            $temp = $this->request->getJSON();
            $karyawan_id = $temp->karyawan_id;
            $gaji_pokok = $temp->gaji_pokok;
            $golongan_id = $temp->golongan_id;

            $data = [
                "karyawan_id" => $karyawan_id,
                "gaji_pokok" => $gaji_pokok,
                "id_golongan" => $golongan_id
            ];

            // $validation = $this->karyawan->findByUsername($id, $username);
            // if (!$validation) {
            //     $response = [
            //         "code" => "00",
            //         "message" => "username already exist"
            //     ];

            //     header('Content-Type: application/json');
            //     echo json_encode($response);
            // } else {
            $result = $this->gaji->updateGaji($data, $id);
            if ($result) {
                $response = [
                    "code" => 200,
                    "message" => "Success"
                ];
            } else {
                $response = [
                    "code" => "00",
                    "message" => "Failed"
                ];
            }
            header('Content-Type: application/json');
            echo json_encode($response);
            // }
        }
    }

    public function delete($id)
    {
        $response = [];

        $result = $this->gaji->deleteGaji($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Tidak dapat menghapus Gaji"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }

    public function generateGaji()
    {
        if ($this->request->isAJAX()) {
            $temp = $this->request->getJSON();
            // $karyawan_id = $temp->karyawan_id;
            $start = str_replace('"', '', json_encode($temp->start));
            $end = str_replace('"', '', json_encode($temp->end));
            $response = $this->detailGaji->gajiKaryawan($start, $end);
            // $response  = $this->detailGaji->findByKaryawanId($karyawan_id, $start, $end);
            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function simpanGaji()
    {
        if ($this->request->isAJAX()) {
            $temp = $this->request->getJSON();
            // $karyawan_id = $temp->karyawan_id;
            $start = str_replace('"', '', json_encode($temp->start));
            $end = str_replace('"', '', json_encode($temp->end));
            $response = $this->detailGaji->simpanGajiKaryawan($start, $end);
            // $response  = $this->detailGaji->findByKaryawanId($karyawan_id, $start, $end);
            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */