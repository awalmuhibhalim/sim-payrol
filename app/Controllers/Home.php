<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'Home',
			'isi' => 'v_home'
		];
		echo view('new_layout/v_wrapper', $data);
	}

	public function new_layout()
	{
		$data = [
			'title' => 'NEW PAGE',
			'isi' => 'page/v_list'
		];
		echo view('new_layout/v_wrapper', $data);
	}

	public function halaman2()
	{
		$data = [
			'title' => 'Halaman 2',
			'isi' => 'v_halaman_2'
		];
		echo view('layout/v_wrapper', $data);
	}

	//--------------------------------------------------------------------

}