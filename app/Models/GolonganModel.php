<?php
namespace App\Models;
use CodeIgniter\Model;

date_default_timezone_set('Asia/Jakarta');
class GolonganModel extends Model
{
    protected $table = 'golongan';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->db->table('golongan')->select("golongan.*")
            ->like('kategori', $search)
            ->orLike('ranges', $search)
            ->orderBy('id', 'ASC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
    }

    public function count_filter($search)
    {
        return $this->db->table('golongan')->select("golongan.*")
            ->like('kategori', $search)
            ->orLike('ranges', $search)
            ->countAllResults();
    }

    public function findGolonganById($id)
    {
        return $this->db->table('golongan')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function insertGolongan($data)
    {
        $result = $this->db->table('golongan')->insert($data);

        return $result;
    }

    public function updateGolongan($data, $id)
    {
        $result = $this->db->table('golongan')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteGolongan($id)
    {
        $row = $this->db->query("select * from golongan where id=$id")->getRow();
        if (isset($row)) {
            $this->db->table('golongan')->delete(array('id' => $id));

            return true;
        }
    }

}