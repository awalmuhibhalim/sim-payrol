<?php

namespace App\Models;

use CodeIgniter\Model;

class TunjanganModel extends Model
{
    protected $table = 'tunjangan';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->db->table('tunjangan')->select("tunjangan.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'tunjangan.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
    }
    public function count_filter($search)
    {
        return $this->db->table('tunjangan')->select("tunjangan.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'tunjangan.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->countAllResults();
    }

    public function insertTunjangan($data)
    {
        $result = $this->db->table('tunjangan')->insert($data);

        return $result;
    }

    public function findTunjanganById($id)
    {
        return $this->db->table('tunjangan')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateTunjangan($data, $id)
    {
        $result = $this->db->table('tunjangan')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteTunjangan($id)
    {
        $row = $this->db->query("select * from tunjangan where id=$id")->getRow();
        if (isset($row)) {
            // if ($row->username == $_SESSION['username'] && $row->role == "Manager") {
            //     return false;
            // } else {
            $this->db->table('tunjangan')->delete(array('id' => $id));

            return true;
            // }
        }
    }

    public function findByKaryawanId($karyawan_id)
    {
        $row = $this->db->query("select * from tunjangan where karyawan_id = $karyawan_id")->getRow();
        if (isset($row)) {
            return true;
        } else {
            return false;
        }
    }
}