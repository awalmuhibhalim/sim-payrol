<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Models\GajiModel;
use App\Models\KaryawanModel;
use App\Models\TunjanganModel;
use App\Models\AbsensiModel;
use App\Models\LemburModel;
use App\Models\HutangModel;

class DetailGajiModel extends Model
{
    protected $table = 'detail_gaji';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok as gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, detail_gaji.total_gaji as total_gaji, detail_gaji.keterangan as keterangan, detail_gaji.tanggal as tanggal")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('detail_gaji', 'gaji.id = detail_gaji.gaji_id')
            ->where('karyawan.id', $_SESSION['id'])
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }else{
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok as gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, detail_gaji.total_gaji as total_gaji, detail_gaji.keterangan as keterangan, detail_gaji.tanggal as tanggal")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('detail_gaji', 'gaji.id = detail_gaji.gaji_id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->orLike('gaji.gaji_pokok', $search)
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }
        
    }
    public function count_filter($search)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, detail_gaji.total_gaji as total_gaji, detail_gaji.keterangan as keterangan, detail_gaji.tanggal as tanggal")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('detail_gaji', 'gaji.id = detail_gaji.gaji_id')
            ->where('karyawan.id', $_SESSION['id'])
            ->countAllResults();
        }else{
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, detail_gaji.total_gaji as total_gaji, detail_gaji.keterangan as keterangan, detail_gaji.tanggal as tanggal")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('detail_gaji', 'gaji.id = detail_gaji.gaji_id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->orLike('gaji.gaji_pokok', $search)
            ->countAllResults();
        }
    }

    public function findByMonth($month)
    {
        return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, detail_gaji.total_gaji as total_gaji, detail_gaji.keterangan as keterangan, detail_gaji.tanggal as tanggal")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('detail_gaji', 'gaji.id = detail_gaji.gaji_id')
            ->where('detail_gaji.tanggal', $month)
            ->get()->getResultArray();
    }

    public function insertGaji($data)
    {
        $result = $this->db->table('gaji')->insert($data);

        return $result;
    }

    public function findGajiById($id)
    {
        return $this->db->table('gaji')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateGaji($data, $id)
    {
        $result = $this->db->table('gaji')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteGaji($id)
    {
        $row = $this->db->query("select * from gaji where id=$id")->getRow();
        if (isset($row)) {
            $this->db->table('gaji')->delete(array('id' => $id));

            return true;
        }
    }

    public function gajiKaryawan($start, $end)
    {
        $result = [];
        $karyawan = $this->db->query("select * from karyawan")->getResultArray();
        for ($i = 0; $i < count($karyawan); $i++) {
            $data = $this->findByKaryawanId($karyawan[$i]['id'], $start, $end);
            array_push($result, $data);
        }

        return $result;
    }

    public function findByKaryawanId($karyawan_id, $start, $end)
    {
        $nama_karyawan = "";
        $jabatan = "";
        $gaji_pokok = 0;
        $uang_makan = 0;
        $uang_transport = 0;
        $totalJamKerja = 21 * 9;
        $gaji_perjam = 0;

        $response = [];

        $row = $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung, tunjangan.pangan as pangan , tunjangan.transport as transport")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('tunjangan', 'tunjangan.karyawan_id = karyawan.id')
            ->where('karyawan.id', $karyawan_id)->get()->getRow();
        if (isset($row)) {
            $nama_karyawan = $row->nama_karyawan;
            $jabatan = $row->jabatan;
            $gaji_pokok = $row->gaji_pokok;
            $gaji_perjam = $gaji_pokok / $totalJamKerja;
            $uang_makan = $row->pangan;
            $uang_transport = $row->transport;
        }

        $absens = $this->db->query("select * from absensi where karyawan_id = $karyawan_id AND tanggal between '$start' and '$end'")->getResultArray();
        $countAbsen = 0;
        for ($i = 0; $i < count($absens); $i++) {
            if ($absens[$i]['status'] == "Hadir") {
                $countAbsen = $countAbsen + 1;
            }
        }

        $lembur = $this->db->query("select * from lembur where karyawan_id = $karyawan_id AND tanggal between '$start' and '$end'")->getResultArray();
        $totalLembur = 0;
        for ($i = 0; $i < count($lembur); $i++) {
            if ($lembur[$i]['status'] == "Disetujui") {
                $totalLembur = $totalLembur + $lembur[$i]['total_jam'];
            }
        }

        $totalTunjangan = ($uang_makan + $uang_transport) * $countAbsen;
        $lemburan = $gaji_perjam * $totalLembur;
        $totalGaji = $gaji_pokok + $totalTunjangan + $lemburan;
        $pph = $gaji_pokok * 0.1;
        $gajiKotor = $totalGaji;
        $totalGaji = $totalGaji - $pph;
        $sisa_hutang = $this->sumHutang($karyawan_id);
        $response = [
            "tanggal" => $start . " - " . $end,
            "nama_karyawan" => $nama_karyawan,
            "jabatan" => $jabatan,
            "gaji_pokok" => number_format($gaji_pokok),
            "total_absen" => $countAbsen,
            "total_tunjangan" => number_format($totalTunjangan),
            "lemburan" => number_format($lemburan),
            "total_jam" => $totalLembur,
            "gaji_perjam" => number_format($gaji_perjam),
            "total_gaji" => number_format($totalGaji),
            "gaji_kotor" => number_format($gajiKotor),
            "sisa_hutang" => number_format($sisa_hutang),
            "pph" => number_format($pph),
            "total_pengeluaran" => number_format($pph),
        ];

        return $response;
    }


    public function simpanGajiKaryawan($start, $end)
    {
        $result = [];
        $karyawan = $this->db->query("select * from karyawan")->getResultArray();
        for ($i = 0; $i < count($karyawan); $i++) {
            $data = $this->hitungGajiKaryawan($karyawan[$i]['id'], $start, $end);
            array_push($result, $data);
        }

        return $result;
    }

    public function hitungGajiKaryawan($karyawan_id, $start, $end)
    {
        $id_gaji = null;
        $nama_karyawan = "";
        $jabatan = "";
        $gaji_pokok = 0;
        $uang_makan = 0;
        $uang_transport = 0;
        $totalJamKerja = 21 * 9;
        $gaji_perjam = 0;

        $response = [];

        $row = $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung, tunjangan.pangan as pangan , tunjangan.transport as transport")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('tunjangan', 'tunjangan.karyawan_id = karyawan.id')
            ->where('karyawan.id', $karyawan_id)->get()->getRow();
        if (isset($row)) {
            $id_gaji = $row->id;
            $nama_karyawan = $row->nama_karyawan;
            $jabatan = $row->jabatan;
            $gaji_pokok = $row->gaji_pokok;
            $gaji_perjam = $gaji_pokok / $totalJamKerja;
            $uang_makan = $row->pangan;
            $uang_transport = $row->transport;
        }

        $absens = $this->db->query("select * from absensi where karyawan_id = $karyawan_id AND tanggal between '$start' and '$end'")->getResultArray();
        $countAbsen = 0;
        for ($i = 0; $i < count($absens); $i++) {
            if ($absens[$i]['status'] == "Hadir") {
                $countAbsen = $countAbsen + 1;
            }
        }

        $lembur = $this->db->query("select * from lembur where karyawan_id = $karyawan_id AND tanggal between '$start' and '$end'")->getResultArray();
        $totalLembur = 0;
        for ($i = 0; $i < count($lembur); $i++) {
            if ($lembur[$i]['status'] == "Disetujui") {
                $totalLembur = $totalLembur + $lembur[$i]['total_jam'];
            }
        }

        $totalTunjangan = ($uang_makan + $uang_transport) * $countAbsen;
        $lemburan = $gaji_perjam * $totalLembur;
        $totalGaji = $gaji_pokok + $totalTunjangan + $lemburan;
        $pph = $gaji_pokok * 0.1;
        $totalGaji = $totalGaji - $pph;


        $keterangan = 'Gaji Pokok : ' . number_format($gaji_pokok) . PHP_EOL .
            '=Total Tunjangan : ' . number_format($totalTunjangan) . PHP_EOL .
            '=PPH 10% : ' . number_format($pph) . PHP_EOL .
            '=Lembur : ' . number_format($lemburan) . PHP_EOL;

        $tanggalGaji = $end;
        $stringParts = explode("-", $tanggalGaji);

        $tahun  = $stringParts[0];
        $bulan = $stringParts[1];
        if ($id_gaji != null || $id_gaji != "") {
            $validation = $this->detailGajiValidation($id_gaji, $bulan . "-" . $tahun);
            if (!$validation) {
                $data = [
                    "gaji_id" => $id_gaji,
                    "total_gaji" => number_format($totalGaji),
                    "keterangan" => $keterangan,
                    "tanggal" => $bulan . "-" . $tahun
                ];
                $result = $this->db->table('detail_gaji')->insert($data);
            }
        }

        return $response;
    }

    public function detailGajiValidation($id_gaji, $bulan)
    {
        $row = $this->db->query("select * from detail_gaji where gaji_id = $id_gaji AND tanggal ='$bulan'")->getRow();
        if (isset($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function sumHutang($karyawan_id)
    {
        $row = $this->db->query("select sum(sisa_cicilan) as sisa_cicilan from hutang where karyawan_id=$karyawan_id")->getRow();
        if (isset($row)) {
            $sisa_cicilan = $row->sisa_cicilan;
            return $sisa_cicilan;
        }else{
            return 0;
        }
    }
}