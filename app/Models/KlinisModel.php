<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

date_default_timezone_set('Asia/Jakarta');

class KlinisModel extends Model
{
    protected $table = 'klinis';
    protected $useTimesTamps = true;
    protected $allowedFields = [
        "nama_obat",
        "bentuk_sediaan",
        "kekuatan_sediaan",
        "satuan_sediaan",
        "jumlah_obat",
        "aturan_pakai",
        "cara_pakai",
        "stabilitas_obat"
    ];

    protected $resepModel;

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->db->table('klinis')->select('klinis.*, administrasi.nama_pasien as nama_pasien, administrasi.no_rekam_medis as no_rekam_medis, administrasi.no_telepon_pasien as no_telepon_pasien')
            ->join('administrasi', 'klinis.administrasi_id=administrasi.id')
            ->like('administrasi.nama_pasien', $search)
            ->orLike('administrasi.no_rekam_medis', $search)
            ->orLike('administrasi.no_telepon_pasien', $search)
            ->limit($limit, $start)
            ->get()->getResultArray();
    }
    public function count_filter($search)
    {
        return $this->db->table('klinis')->select('klinis.*, administrasi.nama_pasien as nama_pasien, administrasi.no_rekam_medis as no_rekam_medis')
            ->join('administrasi', 'klinis.administrasi_id=administrasi.id')
            ->like('administrasi.nama_pasien', $search)
            ->orLike('administrasi.no_rekam_medis', $search)
            ->orLike('administrasi.no_telepon_pasien', $search)
            ->countAllResults();
    }

    public function getKlinisList()
    {
        return $this->table('klinis');
    }

    public function findById($id)
    {
        try {
            return $this->db->table('klinis')->select('klinis.*, administrasi.nama_pasien as nama_pasien, administrasi.no_rekam_medis as no_rekam_medis, administrasi.no_telepon_pasien as no_telepon_pasien, administrasi.nama_dokter as nama_dokter, administrasi.jenis_penjamin as jenis_penjamin')
                ->join('administrasi', 'klinis.administrasi_id=administrasi.id')
                ->where('klinis.id', $id)
                ->get()
                ->getRowArray();
        } catch (Exception $ex) {
        }
    }

    public function insertKlinis($data)
    {
        try {
            $shift = str_replace('"', '', json_encode($data->shift));
            $param = [
                "administrasi_id" => json_encode($data->administrasi_id),
                "shift" => $shift,
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $this->db->table('klinis')->insert($param);
            $klinis_id = $this->getLastId();
            $resepTemp = json_encode($data->resep_obat);
            $resepList = json_decode($resepTemp, true);
            for ($i = 0; $i < count($resepList); $i++) {
                $resep = [
                    "klinis_id" => $klinis_id,
                    "farmasetis_id" => $resepList[$i]['farmasetis_id'],
                    "bentuk_sediaan" => $resepList[$i]['bentuk_sediaan'],
                    "jumlah_obat" => $resepList[$i]['jumlah_obat'],
                    "aturan_pakai" => $resepList[$i]['aturan_pakai']
                ];
            }
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    public function updateKlinis($data, $id)
    {
        try {

            $param = [
                "administrasi_id" => json_encode($data->administrasi_id),
                "updated_at" => date('d-m-Y H:i:s'),
                "creator" => $_SESSION['username']
            ];

            $this->db->table('klinis')->update($param, array('id' => $id));
            $resepTemp = json_encode($data->resep_obat);
            $resepList = json_decode($resepTemp, true);
            for ($i = 0; $i < count($resepList); $i++) {
                $resep = [
                    "klinis_id" => $id,
                    "farmasetis_id" => $resepList[$i]['farmasetis_id'],
                    "bentuk_sediaan" => $resepList[$i]['bentuk_sediaan'],
                    "jumlah_obat" => $resepList[$i]['jumlah_obat'],
                    "aturan_pakai" => $resepList[$i]['aturan_pakai']
                ];
                var_dump($resep);
                $resepModel->insertResepObat($resep);
            }
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    public function deleteKlinis($id)
    {
        try {
            $row = $this->db->query("select * from resep_obat where klinis_id=$id")->getRow();
            if (isset($row)) {
                $result = $this->db->table('klinis')->delete(array('id' => $id));

                return true;
            } else {
                $row2 = $this->db->query("select * from klinis where id=$id")->getRow();
                if (isset($row2)) {
                    $result = $this->db->table('klinis')->delete(array('id' => $id));

                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception $ex) {
        }
    }

    public function search($keyword)
    {
        try {
            return $this->table('klinis')->like('indikasi_obat', $keyword)->orLike('rute_pemberian_obat', $keyword);
        } catch (Exception $ex) {
        }
    }

    public function getLastId()
    {
        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `klinis`')->getRow();
        if (isset($row)) {
            return (int) $row->maxid;
        }
    }

    public function filterReport($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->db->table('klinis')->select('klinis.*, administrasi.nama_pasien as nama_pasien, administrasi.no_rekam_medis as no_rekam_medis, administrasi.no_telepon_pasien as no_telepon_pasien, administrasi.nama_dokter as nama_dokter, administrasi.tanggal_resep as tanggal_resep')
            ->join('administrasi', 'klinis.administrasi_id=administrasi.id')
            ->like('administrasi.nama_pasien', $search)
            ->orLike('administrasi.no_rekam_medis', $search)
            ->orLike('administrasi.no_telepon_pasien', $search)
            ->orLike('klinis.created_at', $search)
            ->orLike('klinis.creator', $search)
            ->orLike('klinis.shift', $search)
            ->limit($limit, $start)
            ->get()->getResultArray();
    }

    public function count_filterReport($search)
    {
        return $this->db->table('klinis')->select('klinis.*, administrasi.nama_pasien as nama_pasien, administrasi.no_rekam_medis as no_rekam_medis, administrasi.no_telepon_pasien as no_telepon_pasien, administrasi.nama_dokter as nama_dokter, administrasi.tanggal_resep as tanggal_resep')
            ->join('administrasi', 'klinis.administrasi_id=administrasi.id')
            ->like('administrasi.nama_pasien', $search)
            ->orLike('administrasi.no_rekam_medis', $search)
            ->orLike('administrasi.no_telepon_pasien', $search)
            ->orLike('klinis.created_at', $search)
            ->orLike('klinis.creator', $search)
            ->orLike('klinis.shift', $search)
            ->countAllResults();
    }
}