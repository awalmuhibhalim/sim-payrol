<?php

namespace App\Models;

use CodeIgniter\Model;

class GajiModel extends Model
{
    protected $table = 'gaji';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung, golongan.id as id_golongan, golongan.kategori as kategori")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('golongan', 'gaji.id_golongan = golongan.id')
            ->where('karyawan.id', $_SESSION['id'])
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }else{
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung, golongan.id as id_golongan, golongan.kategori as kategori")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('golongan', 'gaji.id_golongan = golongan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->orLike('gaji.gaji_pokok', $search)
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }
        
    }
    public function count_filter($search)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung, golongan.id as id_golongan, golongan.kategori as kategori")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('golongan', 'gaji.id_golongan = golongan.id')
            ->where('karyawan.id', $_SESSION['id'])
            ->countAllResults();
        }else{
            return $this->db->table('gaji')->select("gaji.id as id, gaji.gaji_pokok, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung, golongan.id as id_golongan, golongan.kategori as kategori")
            ->join('karyawan', 'gaji.karyawan_id = karyawan.id')
            ->join('golongan', 'gaji.id_golongan = golongan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->orLike('gaji.gaji_pokok', $search)
            ->countAllResults();
        }
        
    }

    public function insertGaji($data)
    {
        $result = $this->db->table('gaji')->insert($data);

        return $result;
    }

    public function findGajiById($id)
    {
        return $this->db->table('gaji')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateGaji($data, $id)
    {
        $result = $this->db->table('gaji')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteGaji($id)
    {
        $row = $this->db->query("select * from gaji where id=$id")->getRow();
        if (isset($row)) {
            // if ($row->username == $_SESSION['username'] && $row->role == "Manager") {
            //     return false;
            // } else {
            $this->db->table('gaji')->delete(array('id' => $id));

            return true;
            // }
        }
    }

    public function findByKaryawanId($karyawan_id)
    {
        $row = $this->db->query("select * from gaji where karyawan_id = $karyawan_id")->getRow();
        if (isset($row)) {
            return true;
        } else {
            return false;
        }
    }
}