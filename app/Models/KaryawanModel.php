<?php

namespace App\Models;

use CodeIgniter\Model;

class KaryawanModel extends Model
{
    protected $table = 'karyawan';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->table('karyawan')->where('id', $_SESSION['id'])
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }else{
            return $this->table('karyawan')
            ->like('username', $search)
            ->orLike('nama_karyawan', $search)
            ->orLike('status', $search)
            ->orLike('jabatan', $search)
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }
        
    }
    public function count_filter($search)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->table('karyawan')->where('id', $_SESSION['id'])
            ->countAllResults();
        }else{
            return $this->table('karyawan')->like('username', $search)
            ->orLike('nama_karyawan', $search)
            ->orLike('status', $search)
            ->orLike('jabatan', $search)
            ->countAllResults();
        }
        
    }

    public function insertKaryawan($data)
    {
        $result = $this->db->table('karyawan')->insert($data);

        return $result;
    }

    public function findKaryawanById($id)
    {
        return $this->db->table('karyawan')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateKaryawan($data, $id)
    {
        $result = $this->db->table('karyawan')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteKaryawan($id)
    {
        $row = $this->db->query("select * from karyawan where id=$id")->getRow();
        if (isset($row)) {
            // if ($row->username == $_SESSION['username'] && $row->role == "Manager") {
            //     return false;
            // } else {
            $this->db->table('karyawan')->delete(array('id' => $id));

            return true;
            // }
        }
    }

    public function keyword($keyword)
    {
        return $this->table('karyawab')->like('username', $keyword)->orLike('display_name', $keyword);
    }

    public function proccessLogin($username, $password)
    {
        $row = $this->db->query("SELECT * FROM karyawan WHERE username='$username' AND password='$password'")->getRow();
        if (isset($row)) {
            $_SESSION['id'] = $row->id;
            $_SESSION['username'] = $row->username;
            $_SESSION['password'] = $row->password;
            $_SESSION['nama_karyawan'] = $row->nama_karyawan;
            $_SESSION['jabatan'] = $row->jabatan;
            if (isset($_SESSION['username'])) {
                // var_dump("Login Success ini display name " . $_SESSION['display_name']);
            }

            return true;
            // var_dump("Login Success");
            // var_dump($row->display_name);
        } else {
            return false;
            // var_dump("Login Failed");
        }
    }

    public function findByUsername($id = null, $username)
    {
        $result = false;
        if (isset($id)) {
            $row = $this->db->query("SELECT * FROM karyawan WHERE id=$id")->getRow();
            if (isset($row)) {
                $rows = $this->db->query("SELECT * FROM karyawan WHERE username='$username'")->getRow();
                if (isset($rows)) {
                    if ($rows->username == $username) {
                        $result = true;
                    } else {
                        $result = false;
                    }
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }
        } else {
            $row = $this->db->query("SELECT * FROM karyawan WHERE username='$username'")->getRow();
            if (isset($row)) {
                $result = true;
            } else {
                $result = false;
            }
        }

        return $result;
    }

    public function selectCount()
    {
        $result = $this->db->query("select * from karyawan where jabatan='manager'")->countAllResults();
        return $result;
    }
}