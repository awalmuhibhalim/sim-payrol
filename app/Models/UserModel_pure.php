<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class UserModel extends Model
{
    protected $table = 'karyawan';
    protected $useTimestamps = true;

    public function getUserList()
    {
        try {
            return $this->table('karyawan');
        } catch (Exception $ex) {
        }
    }

    public function insertUser($data)
    {
        return $this->db->table('karyawan')->insert($data);
    }

    public function editUser($id)
    {
        return $this->db->table('karyawan')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateUser($data, $id)
    {
        return $this->db->table('karyawan')->update($data, array('id' => $id));
    }

    public function deleteUser($id)
    {
        return $this->db->table('karyawan')->delete(array('id' => $id));
    }

    public function keyword($keyword)
    {
        return $this->table('karyawan')->like('username', $keyword)->orLike('nama_karyawan', $keyword);
    }
}