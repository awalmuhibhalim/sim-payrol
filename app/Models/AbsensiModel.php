<?php

namespace App\Models;

use CodeIgniter\Model;

date_default_timezone_set('Asia/Jakarta');
// date('d-m-Y H:i:s')
class AbsensiModel extends Model
{
    protected $table = 'absensi';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->where('karyawan.id', $_SESSION['id'])
            ->orderBy('id', 'ASC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }else{
            return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('absensi.jam_datang', $search)
            ->orLike('absensi.jam_pulang', $search)
            ->orLike('absensi.status', $search)
            ->orLike('absensi.tanggal', $search)
            ->orderBy('id', 'ASC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }
    }
    public function count_filter($search)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->where('karyawan.id', $_SESSION['jabatan'])
            ->countAllResults();
        }else{
            return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('absensi.jam_datang', $search)
            ->orLike('absensi.jam_pulang', $search)
            ->orLike('absensi.status', $search)
            ->orLike('absensi.tanggal', $search)
            ->countAllResults();
        }
    }

    public function absen($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->where('absensi.tanggal', date('Y-m-d'))
            ->where('absensi.status', 'Hadir')
            ->like('karyawan.nama_karyawan', $search)
            ->orderBy('id', 'ASC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
    }
    public function count_absen($search)
    {
        return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->where('absensi.tanggal', date('Y-m-d'))
            ->where('absensi.status', 'Hadir')
            ->like('karyawan.nama_karyawan', $search)
            ->countAllResults();
    }

    public function leave($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->where('absensi.tanggal', date('Y-m-d'))
            ->whereIn('absensi.status', ['Leave', 'Terlambat', 'Tidak Hadir', 'Izin', 'Sakit'])
            ->like('karyawan.nama_karyawan', $search)
            ->orderBy('id', 'ASC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
    }
    public function count_leave($search)
    {
        return $this->db->table('absensi')->select("absensi.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'absensi.karyawan_id = karyawan.id')
            ->where('absensi.tanggal', date('Y-m-d'))
            ->whereIn('absensi.status', ['Leave', 'Terlambat', 'Tidak Hadir', 'Izin', 'Sakit'])
            ->like('karyawan.nama_karyawan', $search)
            ->countAllResults();
    }

    public function getPersentaseKehadiran($reason)
    {
        $today =  date('Y-m-d');
        $total = $this->db->query("select count(id) as jumlah from absensi where status='$reason' and tanggal='$today'")->getRow();
        if (isset($total)) {
            return $total->jumlah;
        } else {
            return $total->jumlah;
        }
    }



    public function insertAbsensi($data)
    {
        $result = $this->db->table('absensi')->insert($data);

        return $result;
    }

    public function findAbsensiByTanggalAndKaryawan($tanggal, $id)
    {
        $row = $this->db->query("select * from absensi where karyawan_id=$id and tanggal='$tanggal' order by id desc limit 1")->getRow();
        if (isset($row)) {

            return $row->id;
        } else {
            return null;
        }
    }

    public function findAbsensiById($id)
    {
        $row = $this->db->query("select * from absensi where id=$id")->getRow();
        if (isset($row)) {
            return $row;
        } else {
            return null;
        }
    }

    public function leaveValidation($tanggal, $id)
    {
        $row = $this->db->query("select * from absensi where karyawan_id=$id and tanggal='$tanggal' order by id desc limit 1")->getRow();
        if (isset($row)) {
            if ($row->jam_pulang != null) {
                return "";
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function updateAbsensi($data, $id)
    {
        $result = $this->db->table('absensi')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteAbsensi($id)
    {
        $row = $this->db->query("select * from absensi where id=$id")->getRow();
        if (isset($row)) {
            $this->db->table('absensi')->delete(array('id' => $id));

            return true;
        }
    }
}