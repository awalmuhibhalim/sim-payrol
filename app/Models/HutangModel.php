<?php

namespace App\Models;

use CodeIgniter\Model;

class HutangModel extends Model
{
    protected $table = 'hutang';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('hutang')->select("hutang.id as id, hutang.tanggal_pinjaman as tanggal_pinjaman, hutang.total_pinjaman as total_pinjaman, hutang.jangka_waktu as jangka_waktu ,hutang.cicilan_perbulan as cicilan_perbulan, hutang.sisa_cicilan as sisa_cicilan, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'hutang.karyawan_id = karyawan.id')
            ->where('karyawan.id', $_SESSION['id'])
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }else{
            return $this->db->table('hutang')->select("hutang.id as id, hutang.tanggal_pinjaman as tanggal_pinjaman, hutang.total_pinjaman as total_pinjaman, hutang.jangka_waktu as jangka_waktu ,hutang.cicilan_perbulan as cicilan_perbulan, hutang.sisa_cicilan as sisa_cicilan, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'hutang.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray(); 
        }
    }
    public function count_filter($search)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('hutang')->select("hutang.id as id, hutang.tanggal_pinjaman as tanggal_pinjaman, hutang.total_pinjaman as total_pinjaman, hutang.jangka_waktu as jangka_waktu ,hutang.cicilan_perbulan as cicilan_perbulan, hutang.sisa_cicilan as sisa_cicilan, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'hutang.karyawan_id = karyawan.id')
            ->like('karyawan.id', $_SESSION['jabatan'])
            ->countAllResults();
        }else{
            return $this->db->table('hutang')->select("hutang.id as id, hutang.tanggal_pinjaman as tanggal_pinjaman, hutang.total_pinjaman as total_pinjaman, hutang.jangka_waktu as jangka_waktu ,hutang.cicilan_perbulan as cicilan_perbulan, hutang.sisa_cicilan as sisa_cicilan, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'hutang.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('karyawan.status', $search)
            ->orLike('karyawan.tanggal_gabung', $search)
            ->countAllResults();
        }
    }

    public function insertHutang($data)
    {
        $result = $this->db->table('hutang')->insert($data);

        return $result;
    }

    public function findHutangById($id)
    {
        return $this->db->table('hutang')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateHutang($data, $id)
    {
        $result = $this->db->table('hutang')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteHutang($id)
    {
        $row = $this->db->query("select * from hutang where id=$id")->getRow();
        if (isset($row)) {
            // if ($row->username == $_SESSION['username'] && $row->role == "Manager") {
            //     return false;
            // } else {
            $this->db->table('hutang')->delete(array('id' => $id));

            return true;
            // }
        }
    }

    public function findByKaryawanId($karyawan_id)
    {
        $row = $this->db->query("select * from hutang where karyawan_id = $karyawan_id")->getRow();
        if (isset($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        return $this->db->table('hutang')->select("hutang.id as id, hutang.tanggal_pinjaman as tanggal_pinjaman, hutang.total_pinjaman as total_pinjaman, hutang.jangka_waktu as jangka_waktu ,hutang.cicilan_perbulan as cicilan_perbulan, hutang.sisa_cicilan as sisa_cicilan, karyawan.id as karyawan_id , karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'hutang.karyawan_id = karyawan.id')
            ->get()->getResultArray();
    }
}