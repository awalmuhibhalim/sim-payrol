<?php

namespace App\Models;

use CodeIgniter\Model;

class LemburModel extends Model
{
    protected $table = 'lembur';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('lembur')->select("lembur.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'lembur.karyawan_id = karyawan.id')
            ->where('karyawan.id', $_SESSION['id'])
            ->orderBy('id', 'DESC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }else{
            return $this->db->table('lembur')->select("lembur.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'lembur.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('lembur.jam_awal', $search)
            ->orLike('lembur.jam_akhir', $search)
            ->orLike('lembur.status', $search)
            ->orLike('lembur.tanggal', $search)
            ->orderBy('id', 'DESC')
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
        }
    }
    public function count_filter($search)
    {
        if($_SESSION['jabatan'] == 'Karyawan'){
            return $this->db->table('lembur')->select("lembur.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'lembur.karyawan_id = karyawan.id')
            ->where('karyawan.id', $_SESSION['id'])
            ->orderBy('id', 'DESC')
            ->countAllResults();
        }else{
            return $this->db->table('lembur')->select("lembur.*, karyawan.nama_karyawan as nama_karyawan , karyawan.jabatan as jabatan, karyawan.tanggal_gabung as tanggal_gabung")
            ->join('karyawan', 'lembur.karyawan_id = karyawan.id')
            ->like('karyawan.nama_karyawan', $search)
            ->orLike('karyawan.jabatan', $search)
            ->orLike('lembur.jam_awal', $search)
            ->orLike('lembur.jam_akhir', $search)
            ->orLike('lembur.status', $search)
            ->orLike('lembur.tanggal', $search)
            ->orderBy('id', 'DESC')
            ->countAllResults();
        }
        
    }

    public function insertLembur($data)
    {
        $result = $this->db->table('lembur')->insert($data);

        return $result;
    }

    public function findLemburByTanggalAndKaryawan($tanggal, $id)
    {
        $row = $this->db->query("select * from lembur where karyawan_id=$id and tanggal='$tanggal' order by id desc limit 1")->getRow();
        if (isset($row)) {

            return $row->id;
        } else {
            return null;
        }
    }

    public function findLemburById($id)
    {
        $row = $this->db->query("select * from lembur where id=$id")->getRow();
        if (isset($row)) {
            return $row;
        } else {
            return null;
        }
    }

    public function leaveValidation($tanggal, $id)
    {
        $row = $this->db->query("select * from lembur where karyawan_id=$id and tanggal='$tanggal' order by id desc limit 1")->getRow();
        if (isset($row)) {
            if ($row->jam_pulang != null) {
                return "";
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function updateLembur($data, $id)
    {
        $result = $this->db->table('lembur')->update($data, array('id' => $id));

        return $result;
    }

    public function deleteLembur($id)
    {
        $row = $this->db->query("select * from lembur where id=$id")->getRow();
        if (isset($row)) {
            $this->db->table('lembur')->delete(array('id' => $id));

            return true;
        }
    }
}