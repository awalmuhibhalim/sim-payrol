<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/index', 'Home::index');
$routes->get('/halaman2', 'Home::halaman2');

$routes->get('/new_layout', 'Home::new_layout');

$routes->get('/menu_karyawan', 'KaryawanController::index');
$routes->post('/ls_daftar_karyawan', 'KaryawanController::view');
$routes->get('/karyawanFindById/(:num)', 'KaryawanController::findById/$1');
$routes->post('/insertKaryawan', 'KaryawanController::insert');
$routes->post('/updateKaryawan/(:num)', 'KaryawanController::update/$1');
$routes->get('/deleteKaryawan/(:num)', 'KaryawanController::delete/$1');

$routes->get('/menu_gaji', 'GajiController::index');
$routes->post('/ls_daftar_gaji', 'GajiController::view');
$routes->get('/GajiFindById/(:num)', 'GajiController::findById/$1');
$routes->post('/insertGaji', 'GajiController::insert');
$routes->post('/updateGaji/(:num)', 'GajiController::update/$1');
$routes->get('/deleteGaji/(:num)', 'GajiController::delete/$1');

$routes->get('/menu_detail_gaji', 'DetailGajiController::index');
$routes->post('/ls_daftar_detail_gaji', 'DetailGajiController::view');
$routes->get('/detailGajiFindById/(:num)', 'DetailGajiController::findById/$1');
$routes->post('/updateDetailGaji/(:num)', 'DetailGajiController::update/$1');

$routes->get('/menu_tunjangan', 'TunjanganController::index');
$routes->post('/ls_daftar_tunjangan', 'TunjanganController::view');
$routes->get('/TunjanganFindById/(:num)', 'TunjanganController::findById/$1');
$routes->post('/insertTunjangan', 'TunjanganController::insert');
$routes->post('/updateTunjangan/(:num)', 'TunjanganController::update/$1');
$routes->get('/deleteTunjangan/(:num)', 'TunjanganController::delete/$1');

$routes->get('/menu_absensi', 'AbsensiController::index');
$routes->post('/ls_daftar_absensi', 'AbsensiController::view');
$routes->get('/AbsensiFindById/(:num)', 'AbsensiController::findById/$1');
$routes->post('/insertAbsensi', 'AbsensiController::insert');
$routes->post('/updateAbsensi', 'AbsensiController::update');
$routes->post('/updateAbsensi2/(:num)', 'AbsensiController::update2/$1');
$routes->get('/deleteAbsensi/(:num)', 'AbsensiController::delete/$1');

$routes->get('/menu_lembur', 'LemburController::index');
$routes->post('/ls_daftar_lembur', 'LemburController::view');
$routes->get('/lemburFindById/(:num)', 'LemburController::findById/$1');
$routes->post('/insertLembur', 'LemburController::insert');
// $routes->post('/updateLembur', 'LemburController::update');
$routes->post('/updateLembur/(:num)', 'LemburController::update2/$1');
$routes->get('/deleteLembur/(:num)', 'LemburController::delete/$1');


$routes->get('/userlist', 'UserController::index');
$routes->get('/form_add_user', 'UserController::openForm');
$routes->post('/adduser', 'UserController::save');
$routes->get('/edituser/(:num)', 'UserController::openFormEdit/$1');
$routes->post('/updateuser/(:num)', 'UserController::update/$1');
$routes->get('/deleteuser/(:num)', 'UserController::delete/$1');

$routes->get('/klinis_list', 'KlinisController::index');
$routes->get('/form_add_klinis', 'KlinisController::openForm');
$routes->post('/add_klinis', 'KlinisController::save');
$routes->get('/edit_klinis/(:num)', 'KlinisController::openFormEdit/$1');
$routes->post('/update_klinis/(:num)', 'KlinisController::update/$1');
$routes->get('/delete_klinis/(:num)', 'KlinisController::delete/$1');
$routes->post('/insertKlinis', 'KlinisController::insertKlinis');

$routes->get('/menu_golongan', 'GolonganController::index');
$routes->post('/ls_golongan', 'GolonganController::view');
$routes->get('/GolonganFindById/(:num)', 'GolonganController::findById/$1');
$routes->post('/insertGolongan', 'GolonganController::insert');
$routes->post('/updateGolongan/(:num)', 'GolonganController::update/$1');
$routes->get('/deleteGolongan/(:num)', 'GolonganController::delete/$1');

$routes->get('/menu_hutang', 'HutangController::index');
$routes->post('/ls_daftar_hutang', 'HutangController::view');
$routes->get('/HutangFindById/(:num)', 'HutangController::findById/$1');
$routes->post('/insertHutang', 'HutangController::insert');
$routes->post('/updateHutang/(:num)', 'HutangController::update/$1');
$routes->get('/deleteHutang/(:num)', 'HutangController::delete/$1');

$routes->get('/gaji_list', 'GajiController::index');

$routes->get('/login', 'LoginController::index');
$routes->post('/process_login', 'LoginController::login');
$routes->get('/logout', 'LoginController::logout');


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}