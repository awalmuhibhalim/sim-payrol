function accessManagement($jabatan) {
  if ($jabatan == "HRD") {
    // $(".menu_absensi").remove();
    // $(".menu_lembur").remove();
    // $(".menu_karyawan").remove();
    // $(".menu_gaji").remove();
    $(".menu_tunjangan").remove();
    $(".menu_golongan").remove();
    $(".menu_hutang").remove();
    // $(".menu_absensi").remove();
    $(".menu_detail_gaji").remove();

    // menu keseluruhan
    // $(".menu_karyawan").remove();
    // $(".menu_gaji").remove();
    // $(".menu_tunjangan").remove();
    // $(".menu_golongan").remove();
    // $(".menu_hutang").remove();
    // $(".menu_absensi").remove();
    // $(".menu_lembur").remove();
    // $(".menu_detail_gaji").remove();

    // $(".absensi_btn_izin").remove();
    // $(".absensi_btn_delete").remove();
    // $(".absensi_btn_view").remove();

    // $(".karyawan_btn_add").remove();
    // $(".karyawan_btn_save").remove();
    // $(".karyawan_btn_delete").remove();

    $(".gaji_btn_add").remove();
    $(".gaji_btn_hitung").remove();
    $(".gaji_btn_save").remove();
    $(".gaji_btn_delete").remove();

    $(".tunjangan_btn_add").remove();
    $(".tunjangan_btn_save").remove();
    $(".tunjangan_btn_delete").remove();

    // $(".lembur_btn_add").remove();
    // $(".lembur_btn_delete").remove();
    // $(".lembur_btn_view").remove();

    $(".detail_gaji_btn_save").remove();
    $(".detail_gaji_btn_delete").remove();
    $(".detail_gaji_btn_view").remove();
  } else if ($jabatan == "Keuangan") {
    // $(".menu_absensi").remove();
    // $(".menu_gaji").remove();
    // $(".menu_tunjangan").remove();
    // $(".menu_hutang").remove();
    // $(".menu_detail_gaji").remove();
    // $(".menu_golongan").remove();

    $(".menu_karyawan").remove();
    $(".menu_lembur").remove();
    

    $(".absensi_btn_izin").remove();
    $(".absensi_btn_delete").remove();
    $(".absensi_btn_view").remove();

    $(".karyawan_btn_add").remove();
    $(".karyawan_btn_save").remove();
    $(".karyawan_btn_delete").remove();

    // $(".gaji_btn_add").remove();
    // $(".gaji_btn_hitung").remove();
    // $(".gaji_btn_save").remove();
    // $(".gaji_btn_delete").remove();

    // $(".tunjangan_btn_add").remove();
    // $(".tunjangan_btn_save").remove();
    // $(".tunjangan_btn_delete").remove();

    $(".lembur_btn_add").remove();
    $(".lembur_btn_delete").remove();
    $(".lembur_btn_view").remove();

    // $(".detail_gaji_btn_save").remove();
    // $(".detail_gaji_btn_delete").remove();
    // $(".detail_gaji_btn_view").remove();
  } else if ($jabatan == "Menejer") {
    // $(".menu_absensi").remove();
    // $(".menu_gaji").remove();

    $(".menu_karyawan").remove();
    $(".menu_tunjangan").remove();
    $(".menu_golongan").remove();
    $(".menu_hutang").remove();
    $(".menu_lembur").remove();
    $(".menu_detail_gaji").remove();

    $(".absensi_btn_izin").remove();
    $(".absensi_btn_delete").remove();
    $(".absensi_btn_view").remove();

    $(".karyawan_btn_add").remove();
    $(".karyawan_btn_save").remove();
    $(".karyawan_btn_delete").remove();

    $(".gaji_btn_add").remove();
    $(".gaji_btn_hitung").remove();
    $(".gaji_btn_save").remove();
    $(".gaji_btn_delete").remove();

    $(".tunjangan_btn_add").remove();
    $(".tunjangan_btn_save").remove();
    $(".tunjangan_btn_delete").remove();

    $(".lembur_btn_add").remove();
    $(".lembur_btn_delete").remove();
    $(".lembur_btn_view").remove();

    // $(".detail_gaji_btn_save").remove();
    // $(".detail_gaji_btn_delete").remove();
    // $(".detail_gaji_btn_view").remove();
  } else if ($jabatan == "Karyawan") {
    $(".menu_tunjangan").remove();

    // menu keseluruhan
    // $(".menu_karyawan").remove();
    // $(".menu_gaji").remove();
    // $(".menu_tunjangan").remove();
    // $(".menu_golongan").remove();
    // $(".menu_hutang").remove();
    // $(".menu_absensi").remove();
    // $(".menu_lembur").remove();
    // $(".menu_detail_gaji").remove();

    $(".absensi_btn_izin").remove();
    $(".absensi_btn_delete").remove();
    $(".absensi_btn_view").remove();

    $(".karyawan_btn_add").remove();
    $(".karyawan_btn_save").remove();
    $(".karyawan_btn_delete").remove();

    $(".gaji_btn_add").remove();
    $(".gaji_btn_hitung").remove();
    $(".gaji_btn_save").remove();
    $(".gaji_btn_delete").remove();

    $(".tunjangan_btn_add").remove();
    $(".tunjangan_btn_save").remove();
    $(".tunjangan_btn_delete").remove();

    $(".lembur_btn_add").remove();
    $(".lembur_btn_delete").remove();
    $(".lembur_btn_view").remove();

    $(".detail_gaji_btn_save").remove();
    $(".detail_gaji_btn_delete").remove();
    $(".detail_gaji_btn_view").remove();
  }else if($jabatan == "Superadmin"){

  } else {
    $(".hak_akses").remove();
  }
  //
  // $(".menu_karyawan").remove();
  // $(".menu_gaji").remove();
  // $(".menu_tunjangan").remove();
  // $(".menu_absensi").remove();
  // $(".menu_lembur").remove();
  // $(".menu_detail_gaji").remove();
  //
  // $(".absensi_btn_izin").remove();
  // $(".absensi_btn_save").remove();
  // $(".absensi_btn_delete").remove();
  // $(".absensi_btn_view").remove();
  //
  // $(".karyawan_btn_add").remove();
  // $(".karyawan_btn_save").remove();
  // $(".karyawan_btn_delete").remove();
  // $(".karyawan_btn_view").remove();
  //
  // $(".gaji_btn_add").remove();
  // $(".gaji_btn_hitung").remove();
  // $(".gaji_btn_save").remove();
  // $(".gaji_btn_delete").remove();
  // $(".gaji_btn_view").remove();
  //
  // $(".tunjangan_btn_add").remove();
  // $(".tunjangan_btn_save").remove();
  // $(".tunjangan_btn_delete").remove();
  // $(".tunjangan_btn_view").remove();
  //
  // $(".lembur_btn_add").remove();
  // $(".lembur_btn_save").remove();
  // $(".lembur_btn_delete").remove();
  // $(".lembur_btn_view").remove();
  //
  // $(".detail_gaji_btn_save").remove();
  // $(".detail_gaji_btn_delete").remove();
  // $(".detail_gaji_btn_view").remove();
}
