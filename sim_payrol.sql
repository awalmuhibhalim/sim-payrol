-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Sep 2021 pada 10.07
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sim_payrol`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `jam_datang` datetime NOT NULL,
  `jam_pulang` datetime DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `tanggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id`, `karyawan_id`, `jam_datang`, `jam_pulang`, `status`, `tanggal`) VALUES
(1, 2, '2020-09-28 08:20:21', '2020-09-28 17:30:21', 'Hadir', '27-09-2020'),
(4, 12, '2020-09-28 08:20:21', '2020-09-28 17:20:21', 'Hadir', '2020-09-28 '),
(5, 1, '2020-08-00 21:21:40', '0000-00-00 00:00:00', 'Hadir', '2020-08-00'),
(6, 1, '2020-08-00 21:23:46', '0000-00-00 00:00:00', 'Hadir', '2020-08-00'),
(7, 1, '2020-08-27 21:25:12', '0000-00-00 00:00:00', 'Hadir', '2020-08-27'),
(9, 1, '2020-09-27 21:33:18', '0000-00-00 00:00:00', 'Hadir', '2020-09-27'),
(10, 2, '2020-09-27 21:51:38', '0000-00-00 00:00:00', 'Tidak Hadir', '2020-09-27'),
(11, 2, '2020-09-27 21:52:38', '0000-00-00 00:00:00', 'Hadir', '2020-09-27'),
(12, 11, '2020-09-27 22:16:36', '2020-09-27 22:47:51', 'Leave', '2020-09-27'),
(14, 11, '2020-09-28 10:23:43', NULL, 'Terlambat', '2020-09-28'),
(16, 1, '2020-09-28 11:02:46', NULL, 'Terlambat', '2020-09-28'),
(17, 2, '0000-00-00 00:00:00', NULL, 'Tidak Hadir', '2020-09-28'),
(18, 1, '0000-00-00 00:00:00', NULL, 'Hadir', '2020-10-05'),
(19, 1, '2020-10-07 21:30:36', NULL, 'Terlambat', '2020-10-07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_gaji`
--

CREATE TABLE `detail_gaji` (
  `id` bigint(20) NOT NULL,
  `gaji_id` int(11) NOT NULL,
  `total_gaji` varchar(50) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `tanggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detail_gaji`
--

INSERT INTO `detail_gaji` (`id`, `gaji_id`, `total_gaji`, `keterangan`, `tanggal`) VALUES
(7, 7, '10,314,550', 'Gaji Pokok : 10,000,000\nTotal Tunjangan : 10,000,000\nLembur : 10,000,000\n', '09-2020'),
(8, 2, '10,050,000', 'Gaji Pokok : 10,000,000\nTotal Tunjangan : 10,000,000\nLembur : 10,000,000\n', '09-2020'),
(9, 8, '15,158,730', 'Gaji Pokok : 15,000,000\nTotal Tunjangan : 15,000,000\nLembur : 15,000,000\n', '09-2020'),
(10, 3, '10,050,000', 'Gaji Pokok : 10,000,000\nTotal Tunjangan : 10,000,000\nLembur : 10,000,000\n', '09-2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gaji`
--

CREATE TABLE `gaji` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `gaji_pokok` bigint(20) DEFAULT NULL,
  `pph` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gaji`
--

INSERT INTO `gaji` (`id`, `karyawan_id`, `gaji_pokok`, `pph`) VALUES
(2, 2, 10000000, NULL),
(3, 12, 10000000, NULL),
(7, 1, 10000000, NULL),
(8, 11, 15000000, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan`
--

CREATE TABLE `golongan` (
  `id` int(11) NOT NULL,
  `kategori` varchar(250) DEFAULT NULL,
  `ranges` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `golongan`
--

INSERT INTO `golongan` (`id`, `kategori`, `ranges`) VALUES
(1, 'Golongan A Edit', '5.000.000 - 7.500.000'),
(2, 'Golongan B', '7.500.000 - 9.000.000'),
(3, 'Golongan C', '9.000.000 - 12.000.000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hutang`
--

CREATE TABLE `hutang` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) DEFAULT NULL,
  `tanggal_pinjaman` varchar(50) DEFAULT NULL,
  `total_pinjaman` bigint(20) DEFAULT NULL,
  `jangka_waktu` int(11) DEFAULT NULL,
  `cicilan_perbulan` bigint(20) DEFAULT NULL,
  `sisa_cicilan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_karyawan` varchar(50) DEFAULT NULL,
  `tanggal_lahir` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(150) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `tanggal_gabung` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `username`, `password`, `nama_karyawan`, `tanggal_lahir`, `tempat_lahir`, `status`, `jk`, `jabatan`, `alamat`, `tanggal_gabung`) VALUES
(1, 'adehafidudin@gmail.com', 'password', 'Ade Hafidudin', '2020-09-23', 'test', 'Menikah', 'laki-laki', 'Staff', 'test', '2020-09-23'),
(2, 'pamayung@gmail.com', 'password', 'Pamayung', '2020-09-27', 'Bogor', 'Belum Menikah', 'laki-laki', 'HRD', 'Ciomas', '2020-09-28'),
(11, 'awalmuhibhalim@gmail.com', 'password', 'Awal Muhib Halim', '2020-09-10', 'Bogor', 'Menikah', 'laki-laki', 'Menejer', 'Meruya Utara', '2020-09-10'),
(12, 'superadmin@gmail.com', 'password', 'Cantik', '2020-09-11', 'Meruya', 'Menikah', 'laki-laki', 'Bendahara', 'Meruya utara', '2020-09-10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lembur`
--

CREATE TABLE `lembur` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `jam_awal` varchar(100) DEFAULT NULL,
  `jam_akhir` varchar(100) DEFAULT NULL,
  `total_jam` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `tanggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `lembur`
--

INSERT INTO `lembur` (`id`, `karyawan_id`, `jam_awal`, `jam_akhir`, `total_jam`, `status`, `notes`, `tanggal`) VALUES
(5, 11, '09:35', '11:30', 2, 'Disetujui', 'oke saya setujui', '2020-09-29'),
(6, 1, '09:30', '12:35', 2, 'Disetujui', 'tidak jelas', '2020-09-29'),
(8, 11, '09:30', '11:30', 2, 'Ditolak', 'saya mau lembur', '2020-09-30'),
(15, 11, '21:30', '23:30', 2, 'Menunggu', 'lembur 2 jam', '2020-10-01'),
(16, 1, '08:30', '12:35', 3, 'Disetujui', 'tidak jelas', '2020-09-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tunjangan`
--

CREATE TABLE `tunjangan` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `jumlah_tunjangan` bigint(20) DEFAULT NULL,
  `tanggungan` varchar(250) DEFAULT NULL,
  `jumlah_tanggungan` bigint(20) DEFAULT NULL,
  `pangan` bigint(20) DEFAULT NULL,
  `transport` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tunjangan`
--

INSERT INTO `tunjangan` (`id`, `karyawan_id`, `jumlah_tunjangan`, `tanggungan`, `jumlah_tanggungan`, `pangan`, `transport`) VALUES
(1, 2, 2000, 'istri', 2, 25000, 25000),
(2, 1, 1000000, 'istri dan anak', 2, 25000, 25000),
(3, 11, 2000, '1', 1, 25000, 25000),
(4, 12, 1, '1', 1, 25000, 25000);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `detail_gaji`
--
ALTER TABLE `detail_gaji`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gaji_id` (`gaji_id`);

--
-- Indeks untuk tabel `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `tunjangan`
--
ALTER TABLE `tunjangan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kaywan_id` (`karyawan_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `detail_gaji`
--
ALTER TABLE `detail_gaji`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `gaji`
--
ALTER TABLE `gaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `lembur`
--
ALTER TABLE `lembur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tunjangan`
--
ALTER TABLE `tunjangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `absensi_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `detail_gaji`
--
ALTER TABLE `detail_gaji`
  ADD CONSTRAINT `detail_gaji_ibfk_1` FOREIGN KEY (`gaji_id`) REFERENCES `gaji` (`id`);

--
-- Ketidakleluasaan untuk tabel `gaji`
--
ALTER TABLE `gaji`
  ADD CONSTRAINT `gaji_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `hutang`
--
ALTER TABLE `hutang`
  ADD CONSTRAINT `hutang_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `lembur`
--
ALTER TABLE `lembur`
  ADD CONSTRAINT `lembur_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `tunjangan`
--
ALTER TABLE `tunjangan`
  ADD CONSTRAINT `tunjangan_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
