-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 01 Nov 2021 pada 14.25
-- Versi server: 5.7.36
-- Versi PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rifqysit_sim-payrol`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `jam_datang` datetime NOT NULL,
  `jam_pulang` datetime DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `tanggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id`, `karyawan_id`, `jam_datang`, `jam_pulang`, `status`, `tanggal`) VALUES
(4, 12, '2020-09-28 08:20:21', '2020-09-28 17:20:21', 'Hadir', '2020-09-28 '),
(5, 1, '2020-08-00 21:21:40', '0000-00-00 00:00:00', 'Hadir', '2020-08-00'),
(6, 1, '2020-08-00 21:23:46', '0000-00-00 00:00:00', 'Hadir', '2020-08-00'),
(7, 1, '2020-08-27 21:25:12', '0000-00-00 00:00:00', 'Hadir', '2020-08-27'),
(9, 1, '2020-09-27 21:33:18', '0000-00-00 00:00:00', 'Hadir', '2020-09-27'),
(10, 2, '2020-09-27 21:51:38', '0000-00-00 00:00:00', 'Tidak Hadir', '2020-09-27'),
(11, 2, '2020-09-27 21:52:38', '0000-00-00 00:00:00', 'Hadir', '2020-09-27'),
(12, 11, '2020-09-27 22:16:36', '2020-09-27 22:47:51', 'Leave', '2020-09-27'),
(14, 11, '2020-09-28 10:23:43', NULL, 'Terlambat', '2020-09-28'),
(16, 1, '2020-09-28 11:02:46', NULL, 'Terlambat', '2020-09-28'),
(17, 2, '0000-00-00 00:00:00', NULL, 'Tidak Hadir', '2020-09-28'),
(18, 1, '0000-00-00 00:00:00', NULL, 'Hadir', '2020-10-05'),
(19, 1, '2020-10-07 21:30:36', NULL, 'Terlambat', '2020-10-07'),
(20, 1, '2020-10-13 13:20:08', NULL, 'Terlambat', '2020-10-13'),
(21, 1, '2020-10-15 22:39:19', NULL, 'Terlambat', '2020-10-15'),
(22, 1, '2020-10-20 21:01:32', NULL, 'Terlambat', '2020-10-20'),
(23, 2, '2020-12-04 14:50:45', NULL, 'Terlambat', '2020-12-04'),
(24, 1, '2020-12-21 19:03:15', NULL, 'Terlambat', '2020-12-21'),
(25, 11, '2020-12-22 14:51:15', NULL, 'Terlambat', '2020-12-22'),
(26, 2, '0000-00-00 00:00:00', NULL, 'Sakit', '2020-12-22'),
(27, 1, '0000-00-00 00:00:00', NULL, 'Izin', '2020-12-22'),
(28, 2, '2020-12-23 07:04:38', NULL, 'Hadir', '2020-12-23'),
(29, 1, '2020-12-23 07:07:00', NULL, 'Hadir', '2020-12-23'),
(30, 12, '2020-12-23 08:47:19', NULL, 'Terlambat', '2020-12-23'),
(31, 11, '2020-12-23 08:48:57', NULL, 'Terlambat', '2020-12-23'),
(32, 2, '2021-01-09 12:56:09', NULL, 'Terlambat', '2021-01-09'),
(33, 1, '2021-03-17 21:00:08', NULL, 'Terlambat', '2021-03-17'),
(34, 2, '2021-10-11 18:10:29', NULL, 'Terlambat', '2021-10-11'),
(35, 1, '2021-09-01 08:20:21', '2021-09-01 17:20:21', 'Hadir', '2021-09-01'),
(36, 1, '2021-09-02 08:20:21', '2021-09-02 17:20:21', 'Hadir', '2021-09-02'),
(37, 1, '2021-09-03 08:20:21', '2021-09-03 17:20:21', 'Hadir', '2021-09-03'),
(38, 1, '2021-09-04 08:20:21', '2021-09-04 17:20:21', 'Hadir', '2021-09-04'),
(39, 1, '2021-09-05 08:20:21', '2021-09-05 17:20:21', 'Hadir', '2021-09-05'),
(40, 1, '2021-09-06 08:20:21', '2021-09-06 17:20:21', 'Hadir', '2021-09-06'),
(41, 1, '2021-09-07 08:20:21', '2021-09-07 17:20:21', 'Hadir', '2021-09-07'),
(42, 1, '2021-09-08 08:20:21', '2021-09-08 17:20:21', 'Hadir', '2021-09-08'),
(43, 1, '2021-09-09 08:20:21', '2021-09-09 17:20:21', 'Hadir', '2021-09-09'),
(44, 1, '2021-09-10 08:20:21', '2021-09-10 17:20:21', 'Hadir', '2021-09-10'),
(45, 1, '2021-09-11 08:20:21', '2021-09-11 17:20:21', 'Hadir', '2021-09-11'),
(46, 1, '2021-09-12 08:20:21', '2021-09-12 17:20:21', 'Hadir', '2021-09-12'),
(47, 1, '2021-09-13 08:20:21', '2021-09-13 17:20:21', 'Hadir', '2021-09-13'),
(48, 1, '2021-09-14 08:20:21', '2021-09-14 17:20:21', 'Hadir', '2021-09-14'),
(49, 1, '2021-09-15 08:20:21', '2021-09-15 17:20:21', 'Hadir', '2021-09-15'),
(50, 1, '2021-09-16 08:20:21', '2021-09-16 17:20:21', 'Hadir', '2021-09-16'),
(51, 1, '2021-09-17 08:20:21', '2021-09-17 17:20:21', 'Hadir', '2021-09-17'),
(52, 1, '2021-09-18 08:20:21', '2021-09-18 17:20:21', 'Hadir', '2021-09-18'),
(53, 1, '2021-09-19 08:20:21', '2021-09-19 17:20:21', 'Hadir', '2021-09-19'),
(54, 1, '2021-09-20 08:20:21', '2021-09-20 17:20:21', 'Hadir', '2021-09-20'),
(55, 1, '2021-09-21 08:20:21', '2021-09-21 17:20:21', 'Hadir', '2021-09-21'),
(56, 1, '2021-09-22 08:20:21', '2021-09-22 17:20:21', 'Hadir', '2021-09-22'),
(57, 1, '2021-09-23 08:20:21', '2021-09-23 17:20:21', 'Hadir', '2021-09-23'),
(58, 1, '2021-09-24 08:20:21', '2021-09-24 17:20:21', 'Hadir', '2021-09-24'),
(59, 1, '2021-09-25 08:20:21', '2021-09-25 17:20:21', 'Hadir', '2021-09-25'),
(60, 1, '2021-09-26 08:20:21', '2021-09-26 17:20:21', 'Hadir', '2021-09-26'),
(61, 1, '2021-09-27 08:20:21', '2021-09-27 17:20:21', 'Hadir', '2021-09-27'),
(62, 1, '2021-09-28 08:20:21', '2021-09-28 17:20:21', 'Hadir', '2021-09-28'),
(63, 1, '2021-09-29 08:20:21', '2021-09-29 17:20:21', 'Hadir', '2021-09-29'),
(64, 1, '2021-09-30 08:20:21', '2021-09-30 17:20:21', 'Hadir', '2021-09-30'),
(65, 2, '2021-09-01 08:20:21', '2021-09-01 17:20:21', 'Hadir', '2021-09-01'),
(66, 2, '2021-09-02 08:20:21', '2021-09-02 17:20:21', 'Hadir', '2021-09-02'),
(67, 2, '2021-09-03 08:20:21', '2021-09-03 17:20:21', 'Hadir', '2021-09-03'),
(68, 2, '2021-09-04 08:20:21', '2021-09-04 17:20:21', 'Hadir', '2021-09-04'),
(69, 2, '2021-09-05 08:20:21', '2021-09-05 17:20:21', 'Hadir', '2021-09-05'),
(70, 2, '2021-09-06 08:20:21', '2021-09-06 17:20:21', 'Hadir', '2021-09-06'),
(71, 2, '2021-09-07 08:20:21', '2021-09-07 17:20:21', 'Hadir', '2021-09-07'),
(72, 2, '2021-09-08 08:20:21', '2021-09-08 17:20:21', 'Hadir', '2021-09-08'),
(73, 2, '2021-09-09 08:20:21', '2021-09-09 17:20:21', 'Hadir', '2021-09-09'),
(74, 2, '2021-09-10 08:20:21', '2021-09-10 17:20:21', 'Hadir', '2021-09-10'),
(75, 2, '2021-09-11 08:20:21', '2021-09-11 17:20:21', 'Hadir', '2021-09-11'),
(76, 2, '2021-09-12 08:20:21', '2021-09-12 17:20:21', 'Hadir', '2021-09-12'),
(77, 2, '2021-09-13 08:20:21', '2021-09-13 17:20:21', 'Hadir', '2021-09-13'),
(78, 2, '2021-09-14 08:20:21', '2021-09-14 17:20:21', 'Hadir', '2021-09-14'),
(79, 2, '2021-09-15 08:20:21', '2021-09-15 17:20:21', 'Hadir', '2021-09-15'),
(80, 2, '2021-09-16 08:20:21', '2021-09-16 17:20:21', 'Hadir', '2021-09-16'),
(81, 2, '2021-09-17 08:20:21', '2021-09-17 17:20:21', 'Hadir', '2021-09-17'),
(82, 2, '2021-09-18 08:20:21', '2021-09-18 17:20:21', 'Hadir', '2021-09-18'),
(83, 2, '2021-09-19 08:20:21', '2021-09-19 17:20:21', 'Hadir', '2021-09-19'),
(84, 2, '2021-09-20 08:20:21', '2021-09-20 17:20:21', 'Hadir', '2021-09-20'),
(85, 2, '2021-09-21 08:20:21', '2021-09-21 17:20:21', 'Hadir', '2021-09-21'),
(86, 2, '2021-09-22 08:20:21', '2021-09-22 17:20:21', 'Hadir', '2021-09-22'),
(87, 2, '2021-09-23 08:20:21', '2021-09-23 17:20:21', 'Hadir', '2021-09-23'),
(88, 2, '2021-09-24 08:20:21', '2021-09-24 17:20:21', 'Hadir', '2021-09-24'),
(89, 2, '2021-09-25 08:20:21', '2021-09-25 17:20:21', 'Hadir', '2021-09-25'),
(90, 2, '2021-09-26 08:20:21', '2021-09-26 17:20:21', 'Hadir', '2021-09-26'),
(91, 2, '2021-09-27 08:20:21', '2021-09-27 17:20:21', 'Hadir', '2021-09-27'),
(92, 2, '2021-09-28 08:20:21', '2021-09-28 17:20:21', 'Hadir', '2021-09-28'),
(93, 2, '2021-09-29 08:20:21', '2021-09-29 17:20:21', 'Hadir', '2021-09-29'),
(94, 2, '2021-09-30 08:20:21', '2021-09-30 17:20:21', 'Hadir', '2021-09-30'),
(95, 11, '2021-09-01 08:20:21', '2021-09-01 17:20:21', 'Hadir', '2021-09-01'),
(96, 11, '2021-09-02 08:20:21', '2021-09-02 17:20:21', 'Hadir', '2021-09-02'),
(97, 11, '2021-09-03 08:20:21', '2021-09-03 17:20:21', 'Hadir', '2021-09-03'),
(98, 11, '2021-09-04 08:20:21', '2021-09-04 17:20:21', 'Hadir', '2021-09-04'),
(99, 11, '2021-09-05 08:20:21', '2021-09-05 17:20:21', 'Hadir', '2021-09-05'),
(100, 11, '2021-09-06 08:20:21', '2021-09-06 17:20:21', 'Hadir', '2021-09-06'),
(101, 11, '2021-09-07 08:20:21', '2021-09-07 17:20:21', 'Hadir', '2021-09-07'),
(102, 11, '2021-09-08 08:20:21', '2021-09-08 17:20:21', 'Hadir', '2021-09-08'),
(103, 11, '2021-09-09 08:20:21', '2021-09-09 17:20:21', 'Hadir', '2021-09-09'),
(104, 11, '2021-09-10 08:20:21', '2021-09-10 17:20:21', 'Hadir', '2021-09-10'),
(105, 11, '2021-09-11 08:20:21', '2021-09-11 17:20:21', 'Hadir', '2021-09-11'),
(106, 11, '2021-09-12 08:20:21', '2021-09-12 17:20:21', 'Hadir', '2021-09-12'),
(107, 11, '2021-09-13 08:20:21', '2021-09-13 17:20:21', 'Hadir', '2021-09-13'),
(108, 11, '2021-09-14 08:20:21', '2021-09-14 17:20:21', 'Hadir', '2021-09-14'),
(109, 11, '2021-09-15 08:20:21', '2021-09-15 17:20:21', 'Hadir', '2021-09-15'),
(110, 11, '2021-09-16 08:20:21', '2021-09-16 17:20:21', 'Hadir', '2021-09-16'),
(111, 11, '2021-09-17 08:20:21', '2021-09-17 17:20:21', 'Hadir', '2021-09-17'),
(112, 11, '2021-09-18 08:20:21', '2021-09-18 17:20:21', 'Hadir', '2021-09-18'),
(113, 11, '2021-09-19 08:20:21', '2021-09-19 17:20:21', 'Hadir', '2021-09-19'),
(114, 11, '2021-09-20 08:20:21', '2021-09-20 17:20:21', 'Hadir', '2021-09-20'),
(115, 11, '2021-09-21 08:20:21', '2021-09-21 17:20:21', 'Hadir', '2021-09-21'),
(116, 11, '2021-09-22 08:20:21', '2021-09-22 17:20:21', 'Hadir', '2021-09-22'),
(117, 11, '2021-09-23 08:20:21', '2021-09-23 17:20:21', 'Hadir', '2021-09-23'),
(118, 11, '2021-09-24 08:20:21', '2021-09-24 17:20:21', 'Hadir', '2021-09-24'),
(119, 11, '2021-09-25 08:20:21', '2021-09-25 17:20:21', 'Hadir', '2021-09-25'),
(120, 11, '2021-09-26 08:20:21', '2021-09-26 17:20:21', 'Hadir', '2021-09-26'),
(121, 11, '2021-09-27 08:20:21', '2021-09-27 17:20:21', 'Hadir', '2021-09-27'),
(122, 11, '2021-09-28 08:20:21', '2021-09-28 17:20:21', 'Hadir', '2021-09-28'),
(123, 11, '2021-09-29 08:20:21', '2021-09-29 17:20:21', 'Hadir', '2021-09-29'),
(124, 11, '2021-09-30 08:20:21', '2021-09-30 17:20:21', 'Hadir', '2021-09-30'),
(125, 12, '2021-09-01 08:20:21', '2021-09-01 17:20:21', 'Hadir', '2021-09-01'),
(126, 12, '2021-09-02 08:20:21', '2021-09-02 17:20:21', 'Hadir', '2021-09-02'),
(127, 12, '2021-09-03 08:20:21', '2021-09-03 17:20:21', 'Hadir', '2021-09-03'),
(128, 12, '2021-09-04 08:20:21', '2021-09-04 17:20:21', 'Hadir', '2021-09-04'),
(129, 12, '2021-09-05 08:20:21', '2021-09-05 17:20:21', 'Hadir', '2021-09-05'),
(130, 12, '2021-09-06 08:20:21', '2021-09-06 17:20:21', 'Hadir', '2021-09-06'),
(131, 12, '2021-09-07 08:20:21', '2021-09-07 17:20:21', 'Hadir', '2021-09-07'),
(132, 12, '2021-09-08 08:20:21', '2021-09-08 17:20:21', 'Hadir', '2021-09-08'),
(133, 12, '2021-09-09 08:20:21', '2021-09-09 17:20:21', 'Hadir', '2021-09-09'),
(134, 12, '2021-09-10 08:20:21', '2021-09-10 17:20:21', 'Hadir', '2021-09-10'),
(135, 12, '2021-09-11 08:20:21', '2021-09-11 17:20:21', 'Hadir', '2021-09-11'),
(136, 12, '2021-09-12 08:20:21', '2021-09-12 17:20:21', 'Hadir', '2021-09-12'),
(137, 12, '2021-09-13 08:20:21', '2021-09-13 17:20:21', 'Hadir', '2021-09-13'),
(138, 12, '2021-09-14 08:20:21', '2021-09-14 17:20:21', 'Hadir', '2021-09-14'),
(139, 12, '2021-09-15 08:20:21', '2021-09-15 17:20:21', 'Hadir', '2021-09-15'),
(140, 12, '2021-09-16 08:20:21', '2021-09-16 17:20:21', 'Hadir', '2021-09-16'),
(141, 12, '2021-09-17 08:20:21', '2021-09-17 17:20:21', 'Hadir', '2021-09-17'),
(142, 12, '2021-09-18 08:20:21', '2021-09-18 17:20:21', 'Hadir', '2021-09-18'),
(143, 12, '2021-09-19 08:20:21', '2021-09-19 17:20:21', 'Hadir', '2021-09-19'),
(144, 12, '2021-09-20 08:20:21', '2021-09-20 17:20:21', 'Hadir', '2021-09-20'),
(145, 12, '2021-09-21 08:20:21', '2021-09-21 17:20:21', 'Hadir', '2021-09-21'),
(146, 12, '2021-09-22 08:20:21', '2021-09-22 17:20:21', 'Hadir', '2021-09-22'),
(147, 12, '2021-09-23 08:20:21', '2021-09-23 17:20:21', 'Hadir', '2021-09-23'),
(148, 12, '2021-09-24 08:20:21', '2021-09-24 17:20:21', 'Hadir', '2021-09-24'),
(149, 12, '2021-09-25 08:20:21', '2021-09-25 17:20:21', 'Hadir', '2021-09-25'),
(150, 12, '2021-09-26 08:20:21', '2021-09-26 17:20:21', 'Hadir', '2021-09-26'),
(151, 12, '2021-09-27 08:20:21', '2021-09-27 17:20:21', 'Hadir', '2021-09-27'),
(152, 12, '2021-09-28 08:20:21', '2021-09-28 17:20:21', 'Hadir', '2021-09-28'),
(153, 12, '2021-09-29 08:20:21', '2021-09-29 17:20:21', 'Hadir', '2021-09-29'),
(154, 12, '2021-09-30 08:20:21', '2021-09-30 17:20:21', 'Hadir', '2021-09-30'),
(155, 13, '2021-09-01 08:20:21', '2021-09-01 17:20:21', 'Hadir', '2021-09-01'),
(156, 13, '2021-09-02 08:20:21', '2021-09-02 17:20:21', 'Hadir', '2021-09-02'),
(157, 13, '2021-09-03 08:20:21', '2021-09-03 17:20:21', 'Hadir', '2021-09-03'),
(158, 13, '2021-09-04 08:20:21', '2021-09-04 17:20:21', 'Hadir', '2021-09-04'),
(159, 13, '2021-09-05 08:20:21', '2021-09-05 17:20:21', 'Hadir', '2021-09-05'),
(160, 13, '2021-09-06 08:20:21', '2021-09-06 17:20:21', 'Hadir', '2021-09-06'),
(161, 13, '2021-09-07 08:20:21', '2021-09-07 17:20:21', 'Hadir', '2021-09-07'),
(162, 13, '2021-09-08 08:20:21', '2021-09-08 17:20:21', 'Hadir', '2021-09-08'),
(163, 13, '2021-09-09 08:20:21', '2021-09-09 17:20:21', 'Hadir', '2021-09-09'),
(164, 13, '2021-09-10 08:20:21', '2021-09-10 17:20:21', 'Hadir', '2021-09-10'),
(165, 13, '2021-09-11 08:20:21', '2021-09-11 17:20:21', 'Hadir', '2021-09-11'),
(166, 13, '2021-09-12 08:20:21', '2021-09-12 17:20:21', 'Hadir', '2021-09-12'),
(167, 13, '2021-09-13 08:20:21', '2021-09-13 17:20:21', 'Hadir', '2021-09-13'),
(168, 13, '2021-09-14 08:20:21', '2021-09-14 17:20:21', 'Hadir', '2021-09-14'),
(169, 13, '2021-09-15 08:20:21', '2021-09-15 17:20:21', 'Hadir', '2021-09-15'),
(170, 13, '2021-09-16 08:20:21', '2021-09-16 17:20:21', 'Hadir', '2021-09-16'),
(171, 13, '2021-09-17 08:20:21', '2021-09-17 17:20:21', 'Hadir', '2021-09-17'),
(172, 13, '2021-09-18 08:20:21', '2021-09-18 17:20:21', 'Hadir', '2021-09-18'),
(173, 13, '2021-09-19 08:20:21', '2021-09-19 17:20:21', 'Hadir', '2021-09-19'),
(174, 13, '2021-09-20 08:20:21', '2021-09-20 17:20:21', 'Hadir', '2021-09-20'),
(175, 13, '2021-09-21 08:20:21', '2021-09-21 17:20:21', 'Hadir', '2021-09-21'),
(176, 13, '2021-09-22 08:20:21', '2021-09-22 17:20:21', 'Hadir', '2021-09-22'),
(177, 13, '2021-09-23 08:20:21', '2021-09-23 17:20:21', 'Hadir', '2021-09-23'),
(178, 13, '2021-09-24 08:20:21', '2021-09-24 17:20:21', 'Hadir', '2021-09-24'),
(179, 13, '2021-09-25 08:20:21', '2021-09-25 17:20:21', 'Hadir', '2021-09-25'),
(180, 13, '2021-09-26 08:20:21', '2021-09-26 17:20:21', 'Hadir', '2021-09-26'),
(181, 13, '2021-09-27 08:20:21', '2021-09-27 17:20:21', 'Hadir', '2021-09-27'),
(182, 13, '2021-09-28 08:20:21', '2021-09-28 17:20:21', 'Hadir', '2021-09-28'),
(183, 13, '2021-09-29 08:20:21', '2021-09-29 17:20:21', 'Hadir', '2021-09-29'),
(184, 13, '2021-09-30 08:20:21', '2021-09-30 17:20:21', 'Hadir', '2021-09-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_gaji`
--

CREATE TABLE `detail_gaji` (
  `id` bigint(20) NOT NULL,
  `gaji_id` int(11) NOT NULL,
  `total_gaji` varchar(50) NOT NULL,
  `keterangan` text,
  `tanggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `detail_gaji`
--

INSERT INTO `detail_gaji` (`id`, `gaji_id`, `total_gaji`, `keterangan`, `tanggal`) VALUES
(27, 7, '10,314,550', 'Gaji Pokok : 10,000,000\n=Total Tunjangan : 50,000\n=Lembur : 264,550\n', '09-2020'),
(28, 2, '10,050,000', 'Gaji Pokok : 10,000,000\n=Total Tunjangan : 50,000\n=Lembur : 0\n', '09-2020'),
(29, 8, '15,158,730', 'Gaji Pokok : 15,000,000\n=Total Tunjangan : 0\n=Lembur : 158,730\n', '09-2020'),
(30, 3, '10,050,000', 'Gaji Pokok : 10,000,000\n=Total Tunjangan : 50,000\n=Lembur : 0\n', '09-2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gaji`
--

CREATE TABLE `gaji` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `gaji_pokok` bigint(20) DEFAULT NULL,
  `pph` bigint(20) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gaji`
--

INSERT INTO `gaji` (`id`, `karyawan_id`, `gaji_pokok`, `pph`, `id_golongan`) VALUES
(2, 2, 10000000, NULL, 2),
(3, 12, 10000000, NULL, 2),
(7, 1, 10000000, NULL, 2),
(8, 11, 15000000, NULL, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan`
--

CREATE TABLE `golongan` (
  `id` int(11) NOT NULL,
  `kategori` varchar(250) DEFAULT NULL,
  `ranges` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `golongan`
--

INSERT INTO `golongan` (`id`, `kategori`, `ranges`) VALUES
(1, 'Golongan A ', '5.000.000 - 7.500.000'),
(2, 'Golongan B', '7.500.000 - 10.000.000'),
(3, 'Golongan C', '10.000.000 - 15.000.000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hutang`
--

CREATE TABLE `hutang` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) DEFAULT NULL,
  `tanggal_pinjaman` varchar(50) DEFAULT NULL,
  `total_pinjaman` bigint(20) DEFAULT NULL,
  `jangka_waktu` int(11) DEFAULT NULL,
  `cicilan_perbulan` bigint(20) DEFAULT NULL,
  `sisa_cicilan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_karyawan` varchar(50) DEFAULT NULL,
  `tanggal_lahir` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(150) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `tanggal_gabung` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `username`, `password`, `nama_karyawan`, `tanggal_lahir`, `tempat_lahir`, `status`, `jk`, `jabatan`, `alamat`, `tanggal_gabung`) VALUES
(1, 'karyawan@gmail.com', 'password', 'Ade Hafidudin', '2020-09-23', 'test', 'Menikah', 'laki-laki', 'Karyawan', 'test', '2020-09-23'),
(2, 'hrd@gmail.com', 'password', 'Pamayung', '2020-09-27', 'Bogor', 'Belum Menikah', 'laki-laki', 'HRD', 'Ciomas', '2020-09-28'),
(11, 'manager@gmail.com', 'password', 'Awal Muhib Halim', '2020-09-10', 'Bogor', 'Menikah', 'laki-laki', 'Menejer', 'Meruya Utara', '2020-09-10'),
(12, 'keuangan@gmail.com', 'password', 'Cantik', '2020-09-11', 'Meruya', 'Menikah', 'laki-laki', 'Keuangan', 'Meruya utara', '2020-09-10'),
(13, 'superadmin@gmail.com', 'password', 'Super Admin', '2021-10-11', 'WhereEver', 'Belum Menikah', 'laki-laki', 'Superadmin', 'Superadmin', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lembur`
--

CREATE TABLE `lembur` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `jam_awal` varchar(100) DEFAULT NULL,
  `jam_akhir` varchar(100) DEFAULT NULL,
  `total_jam` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `tanggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `lembur`
--

INSERT INTO `lembur` (`id`, `karyawan_id`, `jam_awal`, `jam_akhir`, `total_jam`, `status`, `notes`, `tanggal`) VALUES
(5, 11, '09:35', '11:30', 2, 'Disetujui', 'oke saya setujui', '2020-09-29'),
(6, 1, '09:30', '12:35', 2, 'Disetujui', 'tidak jelas', '2020-09-29'),
(8, 11, '09:30', '11:30', 2, 'Ditolak', 'saya mau lembur', '2020-09-30'),
(15, 11, '21:30', '23:30', 2, 'Menunggu', 'lembur 2 jam', '2020-10-01'),
(16, 1, '08:30', '12:35', 3, 'Disetujui', 'tidak jelas', '2020-09-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tunjangan`
--

CREATE TABLE `tunjangan` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `jumlah_tunjangan` bigint(20) DEFAULT NULL,
  `tanggungan` varchar(250) DEFAULT NULL,
  `jumlah_tanggungan` bigint(20) DEFAULT NULL,
  `pangan` bigint(20) DEFAULT NULL,
  `transport` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tunjangan`
--

INSERT INTO `tunjangan` (`id`, `karyawan_id`, `jumlah_tunjangan`, `tanggungan`, `jumlah_tanggungan`, `pangan`, `transport`) VALUES
(1, 2, 2000, 'istri', 2, 25000, 25000),
(2, 1, 1000000, 'istri dan anak', 2, 25000, 25000),
(3, 11, 2000, '1', 1, 25000, 25000),
(4, 12, 1, '1', 1, 25000, 25000);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `detail_gaji`
--
ALTER TABLE `detail_gaji`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gaji_id` (`gaji_id`);

--
-- Indeks untuk tabel `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indeks untuk tabel `tunjangan`
--
ALTER TABLE `tunjangan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kaywan_id` (`karyawan_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT untuk tabel `detail_gaji`
--
ALTER TABLE `detail_gaji`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `gaji`
--
ALTER TABLE `gaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `lembur`
--
ALTER TABLE `lembur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tunjangan`
--
ALTER TABLE `tunjangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `absensi_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `detail_gaji`
--
ALTER TABLE `detail_gaji`
  ADD CONSTRAINT `detail_gaji_ibfk_1` FOREIGN KEY (`gaji_id`) REFERENCES `gaji` (`id`);

--
-- Ketidakleluasaan untuk tabel `gaji`
--
ALTER TABLE `gaji`
  ADD CONSTRAINT `gaji_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `hutang`
--
ALTER TABLE `hutang`
  ADD CONSTRAINT `hutang_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `lembur`
--
ALTER TABLE `lembur`
  ADD CONSTRAINT `lembur_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `tunjangan`
--
ALTER TABLE `tunjangan`
  ADD CONSTRAINT `tunjangan_ibfk_1` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`);
COMMIT;


alter table karyawan
add column nik varchar(50);

alter table karyawan
add column npwp varchar(50);

alter table karyawan
add column tgl_keluar varchar(50);

alter table karyawan
add column alasan_keluar varchar(50);



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
